package com.fourhead.carnet2;

import android.app.Application;
import android.content.Context;

import com.facebook.react.ReactApplication;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.reactnative.photoview.PhotoViewPackage;
import com.github.wumke.RNImmediatePhoneCall.RNImmediatePhoneCallPackage;
import com.imagepicker.ImagePickerPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.dylanvann.fastimage.FastImageViewPackage;
import org.devio.rn.splashscreen.SplashScreenReactPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.actionsheet.ActionSheetPackage;
import com.mapbox.rctmgl.RCTMGLPackage;
import com.devfd.RNGeocoder.RNGeocoderPackage;
import com.fourhead.carnet2.helpers.LocaleHelper;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import com.swmansion.reanimated.ReanimatedPackage;
import com.reactnativecommunity.webview.RNCWebViewPackage;
import io.invertase.firebase.RNFirebasePackage;
import io.invertase.firebase.messaging.RNFirebaseMessagingPackage;
import io.invertase.firebase.notifications.RNFirebaseNotificationsPackage;
import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return com.fourhead.carnet2.BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new RNDeviceInfo(),
            new PhotoViewPackage(),
            new RNImmediatePhoneCallPackage(),
            new ImagePickerPackage(),
            new VectorIconsPackage(),
            new FastImageViewPackage(),
            new SplashScreenReactPackage(),
            new ActionSheetPackage(),
            new RNGeocoderPackage(),
            new RNGestureHandlerPackage(),
            new ReanimatedPackage(),
            new RCTMGLPackage(),
            new RNCWebViewPackage(),
            new RNFirebasePackage(),
            new RNFirebaseMessagingPackage(),
            new RNFirebaseNotificationsPackage()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }

  protected void attachBaseContext(Context base) {
    super.attachBaseContext(LocaleHelper.onAttach(base, "ru"));
  }
}
