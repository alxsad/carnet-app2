import { Platform } from 'react-native';

const iosValues = {
  Cancel: 'Отменить'
};

const androidValues = {
  Cancel: 'ОТМЕНА'
};

export const platformText = Platform.OS === 'ios' ? iosValues : androidValues;
