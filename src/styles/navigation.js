/**
 * Created by aliaksei on 5/31/17.
 */
import { StyleSheet, Platform } from 'react-native';
import {DEFAULT_COLOR} from './app'

export const NavStyles = StyleSheet.create({
  headerNavStyle: {
    backgroundColor: DEFAULT_COLOR,
    borderBottomWidth: 0,
    shadowOpacity: 0,
    shadowOffset: {
      height: 0,
    },
    shadowRadius: 0,
    elevation: 0,
  },
  navIcon: {
    padding: 10,
    // position: 'relative',
    // left: -(Dimensions.get('window').width * 0.02)
  },
  navAvatarRadius:{
    margin: 10,
    borderRadius: 28,
    width: 36,
    height: 36,
    overflow: 'hidden',
  },
  navAvatar: {
      flex: 1,
  }
});

export const tabBarStyles = StyleSheet.create({
  //tabBarOptions.iconStyle not working for ios, so define style for icon directly
  iconStyle: {
    flexGrow: 1,
    width: 30,
    height: 30
  },
  badgeIcon: {
    position: 'absolute',
    right: 0,
    top: Platform.OS === 'android' ? 20:6,
    // bottom: 0,
    // zIndex: 2
  }
});

