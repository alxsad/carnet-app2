const CarBrandCollection = [
  'Acura',
  'Alfa Romeo',
  'Aston Martin',
  'Audi',
  'Bentley',
  'BMW',
  'Bugatti',
  'Buick',
  'Cadillac',
  'Chrysler',
  'Citroen',
  'Dodge',
  'Ferrari',
  'Fiat',
  'Ford',
  'Geely',
  'General Motors',
  'GMC',
  'Honda',
  'Hyundai',
  'Infiniti',
  'Jaguar',
  'Jeep',
  'Kia',
  'Koenigsegg',
  'Lamborghini',
  'Land Rover',
  'Lexus',
  'Maserati',
  'Mazda',
  'McLaren',
  'Mercedes-Benz',
  'Mini',
  'Mitsubishi',
  'Nissan',
  'Opel',
  'Pagani',
  'Peugeot',
  'Porsche',
  'Ram',
  'Renault',
  'Rolls Royce',
  'Saab',
  'Subaru',
  'Suzuki',
  'Tesla',
  'Toyota',
  'Volkswagen',
  'Volvo',
];
export default CarBrandCollection;



