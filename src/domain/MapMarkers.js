export const MarkerTypes = {
    camera: 'camera',
    gai: 'gai',
    traffic: 'traffic',
    accident: 'accident',
    other: 'other'
};

export const Markers = ['gai', 'camera', 'traffic','accident', 'other'];

export const MarkerImages = {
    gai: require('../views/map/screens/assets/gai.png'),
    camera: require('../views/map/screens/assets/camera.png'),
    traffic: require('../views/map/screens/assets/traffic.png'),
    accident: require('../views/map/screens/assets/accident.png'),
    other: require('../views/map/screens/assets/other.png'),
    gai_gray: require('../views/map/screens/assets/gai_gray.png'),
    camera_gray: require('../views/map/screens/assets/camera_gray.png'),
    traffic_gray: require('../views/map/screens/assets/traffic_gray.png'),
    accident_gray: require('../views/map/screens/assets/accident_gray.png'),
    other_gray: require('../views/map/screens/assets/other_gray.png'),
};

export const TypesDescription = {
    gai: `Патруль ГАИ`,
    camera: `Радар`,
    traffic: `Дорожные работы`,
    accident: 'Авария',
    other: `Внимание`,
};

export const Optional = {
    lines: {
        left: 'левый ряд',
        center: 'центральный ряд',
        right: 'правый ряд',
    }
};
