export const OffenseStatus = {
  pending:    'pending',
  processing: 'processing',
  rejected:   'rejected',
  resolved:   'resolved',
};

export const OffenseStatusLabel = {
  pending:    'в ожидании',
  processing: 'в обработке',
  rejected:   'отклонено',
  resolved:   'рассмотрено',
};

export const OffenseType = {
  accident: 'accident',
  parking:  'parking',
  traffic:  'traffic',
};

export const OffenseTypeLabel = {
  accident: 'Происшествие',
  parking:  'Парковка',
  traffic:  'Дорожные объекты',
};
export const OffenseStatusColor = {
    pending:    '#dcdcdc',
    processing: '#DCD86D',
    rejected:   '#DCABAB',
    resolved:   '#A6DC94',
};
