export const DEFAULT_ERROR_MESSAGE = 'Oppsss. Что-то пошло не так, повторите позже';
export const NO_CONNECTION = 'Нет соеденения с Интернетом';
export const INCORRECT_PHONE_MESSAGE = 'Похоже вы ввели некорректный номер';
export const INCORRECT_CODE_MESSAGE = 'Похоже вы ввели некорректный код';
export const RESEND_CODE_MESSAGE = 'СМС с кодом отправлено повторно';
export const MARKER_VOTED_MESSAGE = 'Принято. Спасибо за информацию';
export const MARKER_HAS_BEEN_REMOVED = 'Oppsss. Похоже этот маркер был удален';
