export const RegionEnum = {
    minsk: 'minsk',
    grodno: 'grodno',
    vitebsk: 'vitebsk',
    brest: 'brest',
    mogilev: 'mogilev',
    gomel: 'gomel',
    default: 'belarus',
};

export default  Regions = {
    MINSK_EN: 'Minsk',
    MINSK_RU: 'Минск',
    VITSEBSK_EN: 'Vitsyebsk Province',
    VITSEBSK_RU: 'Витебская область',
    MOGILEV_EN: 'Mogilev',
    MOGILEV_RU: 'Могилевская область',
    GOMEL_EN: 'Gomel',
    GOMEL_RU: 'Гомельская область',
    BREST_EN: 'Brest',
    BREST_RU: 'Брестская область',
    GRODNO_EN: 'Grodno Province',
    GRODNO_RU: 'Гродненская область',
};