import {
  BackAndroid as DeprecatedBackAndroid,
  BackHandler as ModernBackHandler,
} from 'react-native';
import { showMessage } from 'react-native-messages';
import { NO_CONNECTION, DEFAULT_ERROR_MESSAGE } from '../domain/MessagesList';

const Helpers = {
  getNumeric(str){
    return str && str.replace(/[^0-9\.]+/g, "")
  },
  capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  },
  showNoConnectionMessage() {
    showMessage({ type: 'warning', text: NO_CONNECTION });
  },
  showGenericError() {
    showMessage({ type: 'error', text: DEFAULT_ERROR_MESSAGE });
  },
  showError(text) {
    showMessage({ type: 'error', text });
  },

  renderLineDivider(index, linesObj) {
    const lines = Object.keys(linesObj);
    const DOT = '.';
    const COMMA = ', ';
    switch (index) {
      case 0:
        if (lines.length === 1) return DOT;
        return COMMA;
      case lines.length - 1 :
        return DOT;
      default:
        return COMMA;
    }
  }
};

export default Helpers;

const BackHandler = ModernBackHandler || DeprecatedBackAndroid;

export { BackHandler };
