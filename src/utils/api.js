//TODO: rework to component from static
export default class Api {

  static headers(accessToken) {
    const headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      // 'dataType': 'json',
    };
    return accessToken ? {...headers, 'Authorization': accessToken} : headers;
  }

  static get(route, accessToken) {
    return this.xhr(route, null, 'GET', accessToken);
  }

  static put(route, params, accessToken) {
    return this.xhr(route, params, 'PUT', accessToken)
  }

  static post(route, params, accessToken) {
    return this.xhr(route, params, 'POST', accessToken)
  }

  static patch(route, params, accessToken) {
    return this.xhr(route, params, 'PATCH', accessToken)
  }

  static delete(route, accessToken) {
    return this.xhr(route, null, 'DELETE', accessToken)
  }

  //TODO: add possibility to pass additional headers
  static xhr(route, params, verb, accessToken, isJson = true) {
    const host = 'https://carnet.davidovi.ch/api/v1';
    // const host = 'http://localhost:8581/api/v1';
    const url = `${host}${route}`;
    let fetchParams = null;
    if(params){
      if(isJson){
        fetchParams = { body: JSON.stringify(params) }
      }
      else{
        fetchParams = { body: params }
      }
    }

    let options = Object.assign({ method: verb }, fetchParams );
    options.headers = Api.headers(accessToken);
    return fetch(url, options).then(resp => {
      return resp;
      //let json = resp.json();
      // if (resp.ok) {
      //   return json;//TODO: return response not result(promise)
      // }
      // return json.then(err => {throw err});
    });
  }
}
