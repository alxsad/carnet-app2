import { PermissionsAndroid, Platform } from 'react-native';
const isAndroid = Platform.OS === 'android';

const PermissionsHelper = {

  async requestLocationPermission() {
    if(!isAndroid){
      return true;
    }
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          'message': 'Разрешите использование локации для приложения'
        }
      );
      if (granted === true || granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("You can use the location");
        return true;
      } else {
        console.log("location permission denied");
        return false;
      }
    } catch (err) {
      console.warn(err)
    }
  },

  async requestCallPermission() {
    if(!isAndroid){
      return true;
    }
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CALL_PHONE,
        {
          'title': 'Call permissions request',
          'message': 'GIVE IT TO MEE !!!1'
        }
      );
      if (granted === true || granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("You can use the calls");
        return true;
      } else {
        console.log("Calls permission denied");
        return false;
      }
    } catch (err) {
      console.warn(err)
    }
  },


};

export default PermissionsHelper;
