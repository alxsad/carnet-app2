const timeout = function(ms, promise) {
  const timer = new Promise(() => {
    const id = setTimeout(() => {
      clearTimeout(id);
    }, ms)
  });
  return Promise.race([promise, timer]);
};

export default timeout;
