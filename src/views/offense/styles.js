import { StyleSheet } from 'react-native';
//import {DEFAULT_COLOR} from './app'

export default StyleSheet.create({
  lineContent: {
    //flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingBottom: 20,
    paddingTop: 20,
    // height: 50,// change hardcoded value to flex
  },
  separator: {
    borderBottomWidth: 1,
    borderColor: '#dcdcdc',
  },
});
