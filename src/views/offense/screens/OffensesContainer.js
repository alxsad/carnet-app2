import React, {Component} from 'react';
import {StyleSheet, View, TouchableOpacity} from 'react-native';
import {Button} from 'react-native-elements';
import {DEFAULT_COLOR} from '../../../styles';
import {TopTapBar} from '../../../components/tapBar/index'; //last working version 0.0.72
import OffensesList from './OffensesList';
import {OffenseStatus} from '../../../domain';
import {connect} from 'react-redux';
import {getOffenses} from '../../../app/actions/offense';
import Helpers from '../../../utils/helpers';

class OffensesContainer extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      loading: false,
    }
  };

  componentDidMount() {
    this.loadOffenses();
  };

  loadOffenses() {
    if (!this.props.isConnected) {
      return Helpers.showNoConnectionMessage();
    }
    this.showLoader();
    this.props.getOffenses(this.hideLoader.bind(this), this.onError.bind(this));
  }

  showLoader() {
    this.setState({ loading: true });
  }

  hideLoader() {
    this.setState({ loading: false });
  }

  onError() {
    this.hideLoader();
    Helpers.showGenericError();
  }

  renderScene(route, active, archive) {
    const { getOffenses, isConnected } = this.props;
    switch (route.key) {
      case '1':
        return <OffensesList
          fetchCallback={getOffenses}
          offenses={active}
          loading={this.state.loading}
          isConnected={isConnected}
          totalCount={this.props.offensesList.length}
        />;
      case '2':
        return <OffensesList
          fetchCallback={getOffenses}
          offenses={archive}
          loading={this.state.loading}
          isConnected={isConnected}
          totalCount={this.props.offensesList.length}
        />;
      default:
        return null;
    }
  };

  goToNewOffense() {
    this.props.navigation.navigate('NewOffense');
  };

  getTapBarRoutes(active, archive) {
    return [
      { key: '1', title: 'Активные', hint: active.length  ? active.length.toString()  : null },
      { key: '2', title: 'Архив',    hint: archive.length ? archive.length.toString() : null },
    ];
  };

  render() {
    const active = this.props.offensesList.filter(_ => _.status === OffenseStatus.pending || _.status === OffenseStatus.processing);
    const archive = this.props.offensesList.filter(_ => _.status === OffenseStatus.rejected || _.status === OffenseStatus.resolved);

    return (
      <View style={styles.container}>
        <View style={styles.offensesListContainer}>
          <TopTapBar
            tapBarRoutes={this.getTapBarRoutes(active, archive)}
            renderAction={route => this.renderScene(route, active, archive)}
            lazy={false}
          />
        </View>
        <View style={styles.buttonContainer}>
          <View>
            <Button
              Component={TouchableOpacity}
              buttonStyle={styles.sendButton}
              title='Подать заметку'
              backgroundColor={DEFAULT_COLOR}
              onPress={this.goToNewOffense.bind(this)}
            />
          </View>
        </View>
      </View>
    );
  }
}

const propsMapping = state => {
  return {
    offensesList: state.offense.offensesList,
    isConnected: state.network.isConnected,
  };
};

const actionMapping = dispatch => {
  return {
    getOffenses: (success, fail) => dispatch(getOffenses(success, fail)),
  }
};

export default connect(propsMapping, actionMapping)(OffensesContainer);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderTopWidth: 0,
  },
  offensesListContainer: {
    flex: 1,
  },
  buttonContainer: {
    bottom: 0,
    backgroundColor: DEFAULT_COLOR,
    flexDirection:'column',
    justifyContent:'center',
    alignItems:'stretch',
  },
  sendButton: {
    height: 60, //TODO: make responsive
  },
});
