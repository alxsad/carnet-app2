import React, {Component} from 'react';
import {StyleSheet, View, TouchableOpacity, Platform, Text} from 'react-native';
import {Form, LabelField} from '../../../components/form/index';
import {Icon} from '../../../components/index';
import {ModalPicker, DateTimePicker, ImagePicker, ImageSwiper } from '../../../components/index';
import {Button} from 'react-native-elements';
import {DEFAULT_COLOR, NavStyles} from '../../../styles';
import offenseStyles from '../styles';
import {KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {AutoGrowingTextInput} from 'react-native-autogrow-textinput';
import moment from 'moment';
import {normalize, platformText} from '../../../conf';
import {createOffense} from '../../../app/actions/offense';
import {OffenseTypeLabel, OffenseType, RegionEnum} from '../../../domain';
import {connect} from 'react-redux';
import {Helpers, PermissionsHelper} from '../../../utils';

class NewOffense extends Component {

  static navigationOptions = ({ navigation }) => {
    const { params = {isConnected: false} } = navigation.state;
      return {
    headerLeft:
      <TouchableOpacity onPress={() => navigation.dispatch({ type: 'Navigation/BACK' })} >
        <Icon name="icon_back" style={NavStyles.navIcon} size={20} color={'white'}/>
      </TouchableOpacity>,
    // headerRight:
    //   <TouchableOpacity onPress={() => {
    //       if (!params.isConnected) {
    //           return Helpers.showNoConnectionMessage();
    //       }
    //     navigation.navigate('OffenseDisclaimerModal')
    //   }}>
    //       <Text style={[NavStyles.navIcon, {color:'#ffff', fontSize: 25}]}>🤔</Text>
    //     {/*<Icon name="icon_add_white" style={NavStyles.navIcon} size={25} color={'white'}/>*/}
    //   </TouchableOpacity>
  }};

  imagePickerOptions = {
    title: Platform.OS === 'ios' ? 'Выберите фото': null,
    takePhotoButtonTitle: 'Сфотографировать',
    chooseFromLibraryButtonTitle: 'Выбрать фотографию',
    cancelButtonTitle: platformText.Cancel,
    mediaType: 'photo',
    maxWidth: 700,
    maxHeight: 700,
  };

  constructor(props, context) {
    super(props, context);
    this.state = {
      offense: {
        offense_date: null,
        type: null,
        photos: [],
        location: null,
        region: RegionEnum.default,
        text: '',
      },
      offenseType: { value: null, label: null },
      localImages: [],
      isLoading: false,
      buttonDisabled: false,
    };
  };

  componentDidMount() {
      this.props.navigation.setParams({
          isConnected: this.props.isConnected
      })
  }

  onSelectDate(date) {
    const utc = moment(date).format();
    this.setState({ offense: { ...this.state.offense, offense_date: utc }});
  };

  onSelectType(type) {
    this.setState({ offenseType: type, offense: { ...this.state.offense, type: type.value } });
  };

  onSelectImage(image) {
    this.setState({ localImages: [ ...this.state.localImages, image ] });
  };

  onRemoveImage(index) {
    this.setState({ localImages: this.state.localImages.filter((_, i) => i !== index) });
  };

  onSelectLocation(data) {
    this.setState({ offense: { ...this.state.offense, location: data.location, region: data.region } });
  };

  onDescriptionChange(event) {
    this.setState({ offense: { ...this.state.offense, text: event.nativeEvent.text || '' } });
  };

  onSubmit() {
    if (!this.props.isConnected) {
      return Helpers.showNoConnectionMessage();
    }
    if (this.state.offense.text.length === 0) {
      return Helpers.showError('Опишите что произошло');
    }
    this.setState({ isLoading: true, buttonDisabled: true });

    let offense = {...{}, ...this.state.offense};
    if (!offense.type) {
      offense.type = OffenseType.accident;
    }
    if (!offense.offense_date) {
      offense.offense_date = moment().format();
    }
    this.props.createOffense(
      offense,
      this.state.localImages,
      this.onSubmitSuccess.bind(this),
      this.onSubmitFail.bind(this)
    );
  };

  onSubmitSuccess() {
    this.setState({ isLoading: false, buttonDisabled: false });
    this.props.navigation.dispatch({ type: 'Navigation/BACK' });
  };

  onSubmitFail() {
    this.setState({ isLoading: false, buttonDisabled: false });
    Helpers.showGenericError();
  }

  async navigateToSelectLocation() {
    const permissionAllowed = await PermissionsHelper.requestLocationPermission();
    if(permissionAllowed){
      this.props.navigation.navigate('MarkLocationMapModal', {
        cb: this.onSelectLocation.bind(this),
        currLocation: this.state.offense.location
      });
    }
  }

  renderDatePicker() {
    return (
      <View style={[offenseStyles.lineContent, offenseStyles.separator]}>
        <DateTimePicker
          ref={picker => {this.dateTimePicker = picker}}
          maximumDate={moment().toDate()}
          cb={this.onSelectDate.bind(this)}
        />
        <Icon name="icon_calendar" size={15} color={'#ccc'} style={styles.icon} />
        <LabelField
          title="Когда"
          placeholder="Выберите дату"
          valueStyle={{fontSize: normalize(17)}}
          value={this.state.offense.offense_date && moment(this.state.offense.offense_date).format('llll')}
          onPress={() => this.dateTimePicker.show()}
        />
      </View>
    );
  }

  renderTypeSelector() {
    const items = [
      { value: OffenseType.accident, label: OffenseTypeLabel.accident },
      { value: OffenseType.parking,  label: OffenseTypeLabel.parking },
      { value: OffenseType.traffic,  label: OffenseTypeLabel.traffic },
    ];
    return (
      <View style={[offenseStyles.lineContent, offenseStyles.separator]}>
        <ModalPicker
          ref={picker => {this.typePicker = picker}}
          items={items}
          cb={this.onSelectType.bind(this)}
        />
        <Icon name="icon_type" size={15} color={'#ccc'} style={styles.icon}/>
        <LabelField
          title="Тип"
          placeholder="Укажите тип события"
          valueStyle={{fontSize: normalize(17)}}
          value={this.state.offenseType.label}
          onPress={() => this.typePicker.show()}
        />
      </View>
    );
  }

  renderImagePicker() {
    const imagePickerDisabled = this.state.localImages.length >= 3;
    return (
      <View style={[offenseStyles.lineContent, this.state.localImages.length === 0 && offenseStyles.separator]}>
        <ImagePicker
          ref={picker => {this.imagePicker = picker}}
          cb={this.onSelectImage.bind(this)}
        />
        <Icon name="icon_camera" size={15} color={'#ccc'} style={styles.icon}/>
        <LabelField
          disabled={imagePickerDisabled}
          title="Фото правонарушения"
          valueStyle={{fontSize: normalize(17)}}
          onPress={() => this.imagePicker.show(this.imagePickerOptions)}
        />
      </View>
    );
  }

  renderImageSwiper() {
    if (this.state.localImages.length) {
      return(
        <ImageSwiper
          images={this.state.localImages}
          deleteCb={this.onRemoveImage.bind(this)}
        />
      );
    }
    return null;
  }

  renderLocationPicker() {
    return (
      <View style={[offenseStyles.lineContent, offenseStyles.separator]}>
        <Icon name="icon_pin" size={15} color={'#ccc'} style={styles.icon}/>
        <LabelField
          title="Где"
          valueStyle={{fontSize: normalize(17)}}
          placeholder="Укажите точное место"
          value={this.state.offense.location && this.state.offense.location.address}
          onPress={this.navigateToSelectLocation.bind(this)}
        />
      </View>
    );
  }

  renderDescriptionInput() {
    return (
      <AutoGrowingTextInput
        style={styles.textInput}
        placeholder={'Что произошло ?'}
        placeholderTextColor={'#dcdcdc'}
        underlineColorAndroid="transparent"
        onChange={this.onDescriptionChange.bind(this)}
      />
    );
  }

  render() {
    return (
      <View style={styles.container} >
        <KeyboardAwareScrollView
          keyboardDismissMode="interactive"
          automaticallyAdjustContentInsets={false}
          keyboardShouldPersistTaps='never'
        >
          <View style={styles.formContainer}>
            <Form>
              {this.renderDatePicker()}
              {this.renderTypeSelector()}
              {this.renderImagePicker()}
              {this.renderImageSwiper()}
              {this.renderLocationPicker()}
              {this.renderDescriptionInput()}
            </Form>
          </View>
        </KeyboardAwareScrollView>
        <View style={styles.buttonContainer}>
          <View style={styles.sendButton}>
            <Button
              loading={this.state.isLoading}
              disabled={this.state.buttonDisabled}
              disabledStyle={{backgroundColor: DEFAULT_COLOR}}
              Component={TouchableOpacity}
              title="Подать заметку"
              backgroundColor={DEFAULT_COLOR}
              onPress={this.onSubmit.bind(this)}
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  formContainer: {
    flex: 5,
    borderWidth: 1,
    backgroundColor: '#fff',
    borderColor: 'rgba(0,0,0,0.1)',
    margin: 8,
    padding: 18,
    paddingTop: 8,
    shadowColor: '#ccc',
    shadowOffset: { width: 2, height: 2, },
    shadowOpacity: 0.5,
    shadowRadius: 3,
  },
  textInput: {
    paddingLeft: 10,
    fontSize: normalize(17),
    paddingBottom: 0,
    lineHeight: 20,
  },
  buttonContainer: {
    //flex: 1,
    bottom: 0,
    backgroundColor: DEFAULT_COLOR,
    },
  sendButton: {
    //flex: 1,
    flexDirection:'column',
    justifyContent:'center',
    alignItems:'stretch',
    height: 60,
  },
  icon: {
    marginRight: 3,
  }
});

const propsMapping = state => {
  return {
    isConnected: state.network.isConnected,
  };
};

const actionMapping = dispatch => {
  return {
    createOffense: (offense, images, success, fail) => dispatch(createOffense(offense, images, success, fail)),
  }
};

export default connect(propsMapping, actionMapping)(NewOffense);
