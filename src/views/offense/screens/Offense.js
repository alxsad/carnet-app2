import React, {Component} from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { OffenseTypeLabel, OffenseStatusLabel, OffenseStatusColor } from '../../../domain';
import moment from 'moment';

class Offense extends Component {

  renderPhotos(photos) {
    return (
      <View style={styles.thumbWrap}>
        {photos.map((item, i) =>
          <Image
            style={[styles.thumb, i === 0 && { marginLeft: 0 }]} key={i}
            resizeMode="cover"
            source={{ uri: item.url }}
          />
        )}
      </View>
    );
  }

  render() {
    const {offense, selfStyle} = this.props;
    const type = OffenseTypeLabel[offense.type];
    const status = OffenseStatusLabel[offense.status];
    return (
      <View style={[styles.container, selfStyle]}>
        <Text style={styles.boldText}>№ {offense.human_readable_id}</Text>
        <Text style={styles.text}>{moment(offense.offense_date).format('ll')}</Text>
        <Text style={styles.boldText}>{type}</Text>
        {offense.location && <Text style={styles.text}>{offense.location.address}</Text>}
        {offense.photos.length !== 0 && this.renderPhotos(offense.photos)}
        <Text>{offense.text}</Text>

        {offense.gai_comment &&
            <View>
                <View style={styles.separator}/>
                <Text style={styles.boldText}>Результат обработки:</Text>
                <Text style={styles.verdictText}>{offense.gai_comment}</Text>
            </View>
        }
        <View style={styles.separator}/>
        <View style={styles.statusContainer}>
          <Text style={styles.boldText}>Статус:</Text>
          <Text style={[styles.statusText, {color: OffenseStatusColor[offense.status]}]}>{status}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  separator: {
    height: 1,
    backgroundColor: '#dcdcdc',
    marginTop: 15,
    marginBottom: 10,
  },
  text: {
    fontSize: 15,
    lineHeight: 20,
  },
  container: {
    borderWidth: 1,
    backgroundColor: '#fff',
    borderColor: 'rgba(0,0,0,0.1)',
    paddingTop: 28,
    padding: 18,
    marginTop: 4,
    marginBottom: 4,
    marginRight: 8,
    marginLeft: 8,
    shadowColor: '#ccc',
    shadowOffset: { width: 2, height: 2, },
    shadowOpacity: 0.5,
    shadowRadius: 3,
  },
  boldText: {
    fontSize: 15,
    lineHeight: 20,
    fontWeight: 'bold',
  },
  statusContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  statusText: {
    fontSize: 18,
    color: '#dcdcdc',
  },
  verdictText: {
      fontSize: 15,
      // color: '#dcdcdc',
  },
  thumbWrap: {
    flexDirection: 'row',
    height: 158, //TODO: make responsive
  },
  thumb: {
    flex: 1,
    marginLeft: 7,
    marginTop: 15,
    marginBottom: 15,
  },
});

export default Offense;
