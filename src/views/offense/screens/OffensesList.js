import React, {Component} from 'react';
import {StyleSheet, View, FlatList, ActivityIndicator, RefreshControl, Text} from 'react-native';
import Offense from './Offense';
import Helpers from '../../../utils/helpers';
import {DEFAULT_COLOR} from '../../../styles/app';
import {normalize} from '../../../conf';

export default class OffensesList extends Component {

  constructor (props) {
    super(props);
    this.state = {
      refreshing: false,
    }
  }

  showLoader() {
      this.setState({ refreshing: true });
  }

  hideLoader() {
    this.setState({ refreshing: false });
  }

  onError() {
    this.hideLoader();
    Helpers.showGenericError();
  }

  makeRequest() {
    const { fetchCallback, isConnected } = this.props;
    if (!isConnected) {
      return Helpers.showNoConnectionMessage();
    }
    this.showLoader();
    fetchCallback(this.hideLoader.bind(this), this.onError.bind(this));
  };

  renderFooter() {
    if (!this.props.loading) return null;
    return (
      <View style={styles.indicator}>
        <ActivityIndicator color={DEFAULT_COLOR} animating size="large"/>
      </View>
    )
  };

  renderItem = ({item, index}) => {
    const { offenses } = this.props;
    let margin = null;
    if(index === 0){
      margin = {marginTop: 8}
    }
    else if(index === offenses.length - 1){
      margin = {marginBottom: 8}
    }
    return (
      <Offense
        offense={item}
        selfStyle={margin}
      />
  )};

  render() {
    const { offenses, totalCount } = this.props;
    if(totalCount <= 0 && !this.props.loading){
      return <View style={styles.noOffense}>
        <Text style={styles.noOffenseText}>Подача заметок в ГАИ работает в тестовом режиме и на данный момент
          только в городе Минск
        </Text>
      </View>;
    }
    return (
      <View style={styles.container}>
        <FlatList
          scrollEnabled={!this.props.loading}
          data={offenses}
          renderItem={this.renderItem}
          keyExtractor={(item, index) => item.id}
          ListFooterComponent={this.renderFooter.bind(this)}
          enableEmptySections={true}
          removeClippedSubviews={false}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this.makeRequest.bind(this)}
              tintColor={DEFAULT_COLOR}
              colors={[DEFAULT_COLOR]}
            />
          }
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  indicator: {
    paddingVertical: 20,
    borderColor: "#CED0CE",
  },
  noOffense: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  noOffenseText: {
    textAlign: 'center',
    fontSize: normalize(16),
    color: '#9B9B9B',
  },
});
