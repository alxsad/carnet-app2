'use strict';
import React from 'react';

import { StackNavigator } from 'react-navigation'
import {NavStyles} from '../../../styles';
// screens
import OffensesContainer from './OffensesContainer'
import NewOffense from './NewOffense'

const routeConfiguration = {
  OffensesContainer: {
    screen: OffensesContainer,
  },
  NewOffense: {
    screen: NewOffense,
  },
};

const stackNavigatorConfiguration = {
  navigationOptions: ({ screenProps }) => ({
    headerStyle: NavStyles.headerNavStyle,
    title: 'На заметку ГАИ',
    headerTintColor: '#ffffff',
    headerTitleStyle: {alignSelf: 'center'},
  }),
};

export const OffensesNavigation = StackNavigator(routeConfiguration,
  {...stackNavigatorConfiguration, ...{initialRouteName: 'OffensesContainer'}});

