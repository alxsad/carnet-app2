import React, {Component} from 'react';
import {StyleSheet, View, SafeAreaView, TouchableOpacity} from 'react-native';
import {BackHandler} from '../../utils/helpers';
import {Icon} from "../../components";
import {NavStyles} from '../../styles';

export default class MapModal extends Component {

  static navigationOptions = ({navigation}) => {
    const icon = navigation.state.params.icon ?? 'icon_close';
    return {
      gesturesEnabled: navigation.state.params.gesturesEnabled ?? true,
      title: navigation.state.params.title ?? '',
      headerLeft: <View/>,
      headerRight:
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Icon name={icon} style={NavStyles.navIcon} size={20} color={'white'}/>
        </TouchableOpacity>
    }
  };

  handleBackPress = () => {
    this.props.navigation.goBack();
    return true;
  };

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }

  render() {
    const {content} = this.props.navigation.state.params;
    return (
      <SafeAreaView style={styles.safeContainer}>
        <View style={styles.container}>
          {content}
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  safeContainer: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
});
