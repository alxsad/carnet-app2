import React, {Component} from 'react';
import { BackHandler } from '../../utils/helpers';

import {GoogleMarkerMap} from '../../components/maps/index';

class MarkLocationMap extends Component {

  static navigationOptions = ({ navigation }) => ({
    gesturesEnabled: false,
  });

  handleBackPress = () => {
    this.props.navigation.goBack();
    return true;
  };
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress',this.handleBackPress);
  }

  render() {
    const {currLocation, cb} = this.props.navigation.state.params;
    return (
      <GoogleMarkerMap
        currLocation = {currLocation}
        locationCb = {cb}
        navigation = {this.props.navigation}
      />
    );
  }
}

export default MarkLocationMap;


