import React, {Component} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Alert, ActivityIndicator} from 'react-native';
import {Icon} from '../../components/index';
import {CarNumberInput} from '../../components/index';
import {Profile} from '../../services';
import {createChat} from '../../app/actions/messenger';
import {connect} from 'react-redux';
import {v4 as uuid} from 'uuid';
import Helpers, {BackHandler} from '../../utils/helpers';
import { normalize} from '../../conf';
import {NavStyles} from '../../styles';

class FindCar extends Component {

  static navigationOptions = ({ navigation }) => {
    const { params = {showActivity: false} } = navigation.state;
    return {
      title: 'Общение',
      headerLeft:
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Icon name="icon_close_grey" style={NavStyles.navIcon} size={20} color={'white'}/>
        </TouchableOpacity>,
      headerRight: params.showActivity &&
        <ActivityIndicator style={NavStyles.navIcon} animating color="white" size="small"/> ||
        <TouchableOpacity onPress={() => params.findCar && params.findCar()}>
          <Icon name="icon_next" style={NavStyles.navIcon} size={20} color={'white'}/>
        </TouchableOpacity>
    }
  };

  constructor(props, context){
    super(props, context);
    this.state = {
      text: '',
    };
  };

  handleBackPress = () => {
    this.props.navigation.goBack();
    return true;
  };

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  componentWillMount() {
    this.props.navigation.setParams({findCar: this.onFindCar.bind(this)});
    BackHandler.addEventListener('hardwareBackPress',this.handleBackPress);
  }

  onChangeText = text => {
    this.setState({text: text.toUpperCase()});
  };

  isChatExists(carNumber) {
    let found = false;
    Object.keys(this.props.chats).forEach(k => {
      let chat = this.props.chats[k];
      if (chat.car_number === carNumber) {
        found = true;
      }
    });
    return found;
  }

  showAlert(text) {
    Alert.alert(null, text, [{
      text: 'OK',
      onPress: () => {
        this.numberInput.getElement().focus();
      }
    }]);
  }

  async onFindCar() {
    if (!this.props.isConnected) {
      return Helpers.showNoConnectionMessage();
    }
    this.props.navigation.setParams({ showActivity: true});
    const profile = await Profile.getPublicProfile(this.state.text, this.props.accessToken);
    this.props.navigation.setParams({ showActivity: false});
    if (!profile) {
      this.showAlert("Авто не найдено");
    } else if (this.props.userId === profile.user_id) {
      this.showAlert("Нельзя создать чат с самим собой");
    } else if (this.isChatExists(this.state.text)) {
      this.showAlert("Такой чат уже существует");
    } else {
      const chatId = uuid();
      this.props.createChat(this.state.text, profile, chatId);
      this.props.navigation.navigate('Chat', {
        title: (profile.name || 'Водитель') + ' (' + this.state.text + ')',
        chatId: chatId,
        companion: {
          userId: profile.user_id,
          userAvatar: profile.avatar,
          carNumber: profile.car.number
        }
      });
      this.props.navigation.goBack();
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <Text style={styles.headerTitle}>Поиск Авто</Text>
          <Text style={styles.headerText}>Чтобы написать владельцу, введите номер автомобиля используя латинские буквы</Text>
        </View>
        <View style={styles.inputContainer}>
          <CarNumberInput
            refer={ref => {this.numberInput = ref}}
            value={this.state.text}
            onChangeText={(text) => this.onChangeText(text)}
            inputStyle={styles.numberText}
            autoFocus={true}
          />
        </View>
        <View style={styles.footerContainer}>
        </View>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },

  noOffense: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  noOffenseText: {
    textAlign: 'center',
    fontSize: 16,
    color: '#9B9B9B',
  },



  headerContainer: {
    flex: 2,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.05)',
  },
  headerTitle: {
    fontSize: normalize(30),
    color: '#9B9B9B',
    marginBottom: 25,
  },
  headerText: {
    paddingRight: 50,
    paddingLeft: 50,
    textAlign: 'center',
    color: '#9B9B9B',
    fontSize: normalize(16),
  },
  inputContainer: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    borderBottomWidth: 0.5,
    borderColor: '#93939c',
    borderTopWidth: 0.5,
  },
  numberText: {
    flex: 1,
    textAlign: 'center',
    fontSize: normalize(43)
  },
  footerContainer: {
    flex: 3.5,
    alignItems: 'center',
    justifyContent: 'space-around',
    backgroundColor: 'rgba(0,0,0,0.05)',
  },

});

const isConnected = state => state.network.isConnected;

const propsMapping = state => {
  return {
    accessToken: state.auth.accessToken,
    userId: state.auth.userId,
    chats: state.messenger,
    isConnected: isConnected(state),
  };
};

const actionMapping = dispatch => {
  return {
    createChat: (carNumber, profile, chatId) => dispatch(createChat(carNumber, profile, chatId)),
  }
};

export default connect(propsMapping, actionMapping)(FindCar);
