import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  lineContent: {
    //flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingBottom: 20,
    paddingTop: 20,
  },
  separator: {
    borderBottomWidth: 1,
    borderColor: '#dcdcdc',
  },
  hintText: {
    // fontSize: 12,
    //color: DEFAULT_GRAY_COLOR
  }
});
