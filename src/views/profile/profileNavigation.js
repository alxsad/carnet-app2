'use strict';

// React
import React, {Component} from 'react'

// Navigation
import { addNavigationHelpers } from 'react-navigation'
import { ProfileNavigation } from './screens/navigationConfiguration'

// Redux
import { connect } from 'react-redux'

const mapStateToProps = (state) => {
  return {
    navigationState: state.profileTab
  }
};
class TabProfileNavigation extends Component {

  render(){
    const { navigationState, dispatch } = this.props;
    return (
      <ProfileNavigation
        navigation={
          addNavigationHelpers({
            dispatch: dispatch,
            state: navigationState
          })
        }
      />
    )
  }
}
export default connect(mapStateToProps)(TabProfileNavigation)
