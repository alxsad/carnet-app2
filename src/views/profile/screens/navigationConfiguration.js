'use strict';
import React from 'react';

import { StackNavigator } from 'react-navigation'
import {NavStyles} from '../../../styles';
// screens
import ViewProfile from './ProfileContainer'
import EditProfile from './EditProfile'
import AddNewCar from './AddNewCar'
import CarVendors from './CarVendors'
import InterestsList from './InterestsList'
import ViewCar from './ViewCar'


const routeConfiguration = {
  ViewProfile: {
    screen: ViewProfile,
  },
  EditProfile: {
    screen: EditProfile,
  },
  AddNewCar: {
    screen: AddNewCar,
    navigationOptions: ({navigation}) => ({
      tabBarVisible: false,
    })
  },
  CarVendors: {
    screen: CarVendors,
    navigationOptions: ({navigation}) => ({
      tabBarVisible: false,
    })
  },
  Interests: {
    screen: InterestsList,
    navigationOptions: ({navigation}) => ({
      tabBarVisible: false,
    })
  },
  ViewCar: {
    screen: ViewCar,
    navigationOptions: ({navigation}) => ({
      tabBarVisible: false,
    })
  }
};

const stackNavigatorConfiguration = {
  navigationOptions: ({ screenProps }) => ({
    headerStyle: NavStyles.headerNavStyle,
    title: 'Профиль',
    headerTintColor: '#ffffff',
    headerTitleStyle: {alignSelf: 'center'},
  }),
  initialRouteName: 'ViewProfile',
};

export const ProfileNavigation = StackNavigator(routeConfiguration,stackNavigatorConfiguration);
