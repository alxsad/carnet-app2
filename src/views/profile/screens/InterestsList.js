import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {Checkbox} from '../../../components/index';
import {Icon} from '../../../components/index';
import {NavStyles} from '../../../styles';
import {getInterestsList} from '../../../app/actions/domain';
import {connect} from 'react-redux';
import {showMessage} from 'react-native-messages';
import Helpers from '../../../utils/helpers';
import {DEFAULT_ERROR_MESSAGE} from '../../../domain/MessagesList';
import {DEFAULT_COLOR} from "../../../styles/app";

class InterestsList extends Component {

  static navigationOptions = ({ navigation }) => {
    const {params = {}} = navigation.state;
    return {
      title: 'Список интересов',
      headerLeft:
        <TouchableOpacity onPress={() => {
          navigation.dispatch({ type: 'Navigation/BACK' });
        }} >
          <Icon name="icon_back" style={NavStyles.navIcon} size={20} color={'white'}/>
        </TouchableOpacity>,
      headerRight: <View/>,
    }
  };

  constructor(props, context){
    super(props, context);
    this.state = {
      interests: [ ...this.props.navigation.state.params.interests ],
      loading: false
    };
  };

  componentDidMount(){
    if (!this.props.isConnected) {
      return Helpers.showNoConnectionMessage();
    }
    if(!this.props.interestsList.length > 0){
      this.setState({loading: true});
      this.makeRequest();
    }
  }

  async makeRequest() {
    await this.props.getInterestsList(this.onError.bind(this));
    this.setState({loading: false});
  };

  onError() {
    showMessage({ type: 'error', text: DEFAULT_ERROR_MESSAGE });
  }

  toggleInterest = row => {
    const { onSelect } = this.props.navigation.state.params;

    let newInterests = [];
    if (this.state.interests.includes(row)) {
      newInterests = this.state.interests.filter(value => value !== row);
    } else {
      newInterests = [ ...this.state.interests, row ];
    }
    this.setState({ interests: newInterests },
      //TODO: check performance (rerender)
      onSelect(newInterests));
  };

  renderFooter() {
    if (!this.state.loading) {
      return null;
    }
    return (
      <View style={styles.footer}>
        <ActivityIndicator color={DEFAULT_COLOR} animating size="large"/>
      </View>
    );
  };

  renderRow(row) {
    return (
      <View style={styles.rowContainer}>
        <TouchableOpacity
          style={styles.row}
          onPress={() => this.toggleInterest(row)}
        >
            <Text style={styles.rowText}>{row}</Text>
            <Checkbox checked={this.state.interests.includes(row)}/>
        </TouchableOpacity>
      </View>
    )
  }
  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.props.interestsList}
          renderItem={({item}) => this.renderRow(item)}
          keyExtractor={(item, index)  => index.toString()}
          extraData={this.state.interests}// watch props so flatList will re-render
          enableEmptySections={true}
          removeClippedSubviews={false}// fixed disappearing content if ListView used
          ListFooterComponent={this.renderFooter.bind(this)}
        />
      </View>
    )
  }
}

const propsMapping = state => {
  return {
    interestsList: state.domain.interestsList,
    isConnected: state.network.isConnected,
  }
};
const actionMapping = dispatch => {
  return {
    getInterestsList: ( fail) => dispatch(getInterestsList(fail)),
  }
};

export default connect(propsMapping, actionMapping)(InterestsList);

const styles = StyleSheet.create({
  container: {
    paddingTop: 0,
    padding: 15,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    backgroundColor: '#fff',
  },
  rowContainer: {
    borderColor: '#dcdcdc',
    borderBottomWidth: 1,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: 12,
    paddingBottom: 12,
  },
  rowText: {
    fontSize: 15,
  },
  footer: {
    paddingVertical: 20,
  },
});
