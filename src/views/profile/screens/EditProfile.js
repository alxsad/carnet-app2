import React, {Component} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Dimensions, ActivityIndicator} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import profileStyles from '../styles';
import {saveProfile, uploadAvatar} from '../../../app/actions/profile';
import {normalize, platformText} from '../../../conf';
import {NavStyles} from '../../../styles';
import {ImagePicker} from '../../../components/index';
import {connect} from 'react-redux';
import {Form, TextField} from '../../../components/form/index';
import {Icon, CarNumberPlate, Avatar, Content, Tag, Link} from '../../../components';
import {DEFAULT_ERROR_MESSAGE, NO_CONNECTION} from '../../../domain/MessagesList';
import {showMessage} from 'react-native-messages';

class EditProfile extends Component {

  static navigationOptions = ({ navigation }) => {
    const { params = {showHeaderLoader: false}} = navigation.state;
    return {
      tabBarVisible: false,
      headerTitleStyle: {marginHorizontal: 0, alignSelf: 'center'},
      title: <Text style={styles.headerTitle}>Редактирование профиля</Text>,
      headerLeft:
        <TouchableOpacity onPress={() => navigation.dispatch({ type: 'Navigation/BACK' })} >
          <Icon name="icon_back" style={NavStyles.navIcon} size={20} color={'white'}/>
        </TouchableOpacity>,
      headerRight: params.showHeaderLoader &&
        <ActivityIndicator style={NavStyles.navIcon} animating color="white" size="small"/> ||
        <TouchableOpacity onPress={() => params.saveProfile && params.saveProfile()}>
          <Icon name="icon_checkmark" style={NavStyles.navIcon} size={20} color={'white'}/>
        </TouchableOpacity>
    }
  };

  constructor(props, context){
    super(props, context);
    this.state = {
      profile: {...this.props.profile},
      showHeaderLoader: false,
    };
  };

  componentDidMount() {
    this.props.navigation.setParams({saveProfile: () => this.onSaveProfile()});
  }

  onSaveProfile() {
    if (!this.props.isConnected) {
      showMessage({ type: 'warning', text: NO_CONNECTION });
      return;
    }
    this.showActivityIndicator();
    this.props.saveProfile(this.state.profile, this.onSaveProfileSuccess.bind(this), this.onSaveProfileError.bind(this));
  }

  onSaveProfileSuccess() {
    this.hideActivityIndicator();
    this.props.navigation.dispatch({ type: 'Navigation/BACK' });
  }

  onSaveProfileError() {
    this.hideActivityIndicator();
    showMessage({ type: 'error', text: DEFAULT_ERROR_MESSAGE });
  }

  showActivityIndicator() {
    this.props.navigation.setParams({ showHeaderLoader: true });
    this.setState({ showHeaderLoader: true });
  }

  hideActivityIndicator() {
    this.props.navigation.setParams({ showHeaderLoader: false });
    this.setState({ showHeaderLoader: false });
  }

  onChangeAvatar = image => {
    if (!this.props.isConnected){
      showMessage({type: 'warning', text: NO_CONNECTION});
      return;
    }
    this.showActivityIndicator();
    this.setState({ profile: {...this.state.profile, avatarLocal: image.uri }});
    this.props.uploadAvatar(image.data, this.onChangeAvatarSuccess.bind(this), this.onChangeAvatarError.bind(this));
  };

  onChangeAvatarSuccess(url){
    this.hideActivityIndicator();
    this.setState({ profile: {...this.state.profile, avatar: url }});
  }

  onChangeAvatarError() {
    this.hideActivityIndicator();
    showMessage({ type: 'error', text: DEFAULT_ERROR_MESSAGE });
  }

  onChangeName = name => {
    this.setState({ profile: {...this.state.profile, name }})
  };

  onEditCar = (car, ndx) => {
    let cars = this.state.profile.cars.filter((_, index) => index !== ndx);
    cars.push(car);
    this.setState({ profile: {...this.state.profile, cars }})
  };

  onRemoveCar = ndx => {
    let cars = this.state.profile.cars.filter((_, index) => index !== ndx);
    this.setState({ profile: {...this.state.profile, cars }})
  };

  onCreateCar = car => {
    const cars = [...this.state.profile.cars];
    cars.push(car);
    this.setState({profile: {...this.state.profile, cars }})
  };

  onSelectInterests = interests => {
    this.setState({profile: {...this.state.profile, interests }})
  };

  imagePickerOptions = {
    title: 'Выберите фото',
    takePhotoButtonTitle: 'Сделать фотографию',
    chooseFromLibraryButtonTitle: 'Выбрать фотографию',
    cancelButtonTitle: platformText.Cancel,
    mediaType: 'photo',
    allowsEditing: true,
    maxWidth: 300,
    maxHeight: 300,
    storageOptions: {
      skipBackup: true,
      path: 'images',
    }
  };

  renderInterests() {
    const interests = this.state.profile.interests;
    return interests.map((interest, i) =>
      <Tag key={i} styles={{marginRight: 5, marginBottom: 5}}>
        <Text style={{fontSize: normalize(16)}}>{interest}</Text>
      </Tag>
    );
  }

  renderCars() {
    const cars = this.state.profile.cars;
    return cars.length > 0 && cars.map((car, i) => (
      <View key={i} style={[profileStyles.lineContent, profileStyles.separator]}>
        <Text style={{flex: 1, fontSize: normalize(18)}}>{car.brand} {car.model}</Text>
        <TouchableOpacity
          style={styles.carNumberContainer}
          onPress={() => this.props.navigation.navigate('ViewCar', {
            car,
            onEdit: this.onEditCar,
            onRemove: this.onRemoveCar,
            ndx: i,
          })}
        >
          <CarNumberPlate carNumber={car.number}/>
          <Icon name="icon_disclosure" style={{marginLeft: 7}} size={15} color={'#a6a6a6'}/>
        </TouchableOpacity>
      </View>
    ));
  }

  render() {
    const avatarSource = this.state.profile.avatarLocal || this.state.profile.avatar;
    return (
      <View style={styles.container}>
        <KeyboardAwareScrollView
          keyboardDismissMode="interactive"
          automaticallyAdjustContentInsets={false}
          keyboardShouldPersistTaps='never'
        >
          <View style={styles.avatarContainer}>
            <ImagePicker
              ref={picker => {this.avatarPicker = picker}}
              cb={this.onChangeAvatar}
            />
              <Avatar disabled={this.state.showHeaderLoader}
                      avatarStyle={styles.avatar}
                      onPress={() => this.avatarPicker && this.avatarPicker.show(this.imagePickerOptions)}
                      url={avatarSource} />
          </View>
          <View style={styles.profileContainer}>
            <Form>
              <Content title="Контактые данные">
                <View style={[profileStyles.lineContent, profileStyles.separator]}>
                  <TextField
                    title="Имя"
                    titleStyle={styles.title}
                    placeholder="Укажите ваше имя"
                    value={this.state.profile.name}
                    valueStyle={styles.value}
                    onChangeText={this.onChangeName}
                    maxLength={25}
                  />
                </View>
              </Content>
              <Content title="В гараже">
                {this.renderCars()}
              </Content>
              <View style={profileStyles.lineContent}>
                <Link
                  disabled={this.state.profile.cars.length === 5}
                  titleStyle={styles.link}
                  icon="icon_add"
                  title="Добавить ТС"
                  onPress={() => this.props.navigation.navigate('AddNewCar', {
                    onCreate: this.onCreateCar,
                    cars: this.state.profile.cars,
                  })}
                />
              </View>
              {/*<Content title="Мои интересы">*/}
              {/*  <View style={[styles.tags, profileStyles.separator]}>*/}
              {/*    {this.renderInterests()}*/}
              {/*  </View>*/}
              {/*</Content>*/}
              {/*<View style={profileStyles.lineContent}>*/}
              {/*  <Link*/}
              {/*    titleStyle={styles.link}*/}
              {/*    icon="icon_edit"*/}
              {/*    title="Изменить список интересов"*/}
              {/*    onPress={() =>  this.props.navigation.navigate('Interests', {*/}
              {/*      onSelect: this.onSelectInterests,*/}
              {/*      interests: this.state.profile.interests,*/}
              {/*    })}*/}
              {/*  />*/}
              {/*</View>*/}
            </Form>
          </View>
        </KeyboardAwareScrollView>
      </View>
    )
  }
}

const { height } = Dimensions.get('window');
const avatarContainerHeight = height * 0.3;
const avatarSize = avatarContainerHeight * 0.9;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f2f2',
  },
  headerTitle: {
    fontSize: normalize(14.5),
  },
  avatarContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: avatarContainerHeight,
  },
  avatar: {
    width: avatarSize,
    height: avatarSize,
    borderRadius: avatarSize / 2,
    borderColor: 'white',
    borderWidth: 2,
  },
  profileContainer: {
    flex: 1,
    backgroundColor: '#fff',
    paddingLeft: 19,
    paddingRight: 19,
  },
  tags: {
    alignItems: 'flex-start',
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingTop: 20,
    paddingBottom: 10,
  },
  carNumberContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  title: {
    fontSize: normalize(18),
  },
  value: {
    fontSize:  normalize(18),
    color:'#cacaca',
  },
  link: {
    fontSize:  normalize(18),
  }
});

const propsMapping = state => {
  return {
    profile: state.profile,
    isConnected: state.network.isConnected,
  }
};
const actionMapping = dispatch => {
  return {
    saveProfile: (profile, success, fail) => dispatch(saveProfile(profile, success, fail)),
    uploadAvatar: (base64, success, fail) => dispatch(uploadAvatar(base64, success, fail)),
  }
};

export default connect(propsMapping, actionMapping)(EditProfile);
