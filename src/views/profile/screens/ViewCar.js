import React, {Component} from 'react';
import {StyleSheet, View, TouchableOpacity, Alert, Text, ActivityIndicator} from 'react-native';
import {Form, TextField, LabelField} from '../../../components/form/index';
import {CarNumberInput} from '../../../components/index';
import {Button} from 'react-native-elements';
import {Icon} from '../../../components/index';
import {CarBrands} from '../../../domain';
import profileStyles from '../styles';
import {Profile} from '../../../services';
import {connect} from 'react-redux';
import {NavStyles} from '../../../styles';
import Helpers from '../../../utils/helpers';
import {normalize} from '../../../conf';

class ViewCar extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {showHeaderLoader: false} } = navigation.state;
    const {car} = params;
    return {
      title:<Text style={styles.headerTitle}>{`${car.number.toUpperCase()} ${car.brand.toUpperCase()} ${car.model.toUpperCase()}`}</Text>,
      headerLeft:
        <TouchableOpacity onPress={() => navigation.dispatch({ type: 'Navigation/BACK' })} >
          <Icon name="icon_back" style={NavStyles.navIcon} size={20} color={'white'}/>
        </TouchableOpacity>,
      headerRight:
      params.showHeaderLoader &&
      <ActivityIndicator style={NavStyles.navIcon} animating color="white" size="small"/> ||
        <TouchableOpacity onPress={() => params.editCar && params.editCar()}>
          <Icon name="icon_checkmark" style={NavStyles.navIcon} size={20} color={'white'}/>
      </TouchableOpacity>
    }
  };

  constructor(props, context) {
    super(props, context);
    const {car} = this.props.navigation.state.params;
    this.state = {
      car: car,
      isMarkEditable: false,
    }
  };

  componentDidMount() {
    this.props.navigation.setParams({ editCar: () => this.onEditCar()});
  }

  onSelectBrand() {
    this.props.navigation.navigate('CarVendors', {
      value: this.state.car.brand,
      collection: CarBrands,
      onSelect: value => {
        this.onChangeMark('brand', value);
        this.props.navigation.dispatch({ type: 'Navigation/BACK' });
      },
    });
  }

  async onEditCar() {
    if(this.state.car.number.length <= 0  || this.state.car.brand.length <= 0 || this.state.car.model.length <= 0){
      Alert.alert(null, 'Введите марку, модель и номер автомобиля');
      return;
    }
    if(this.state.car.number.length < 9){
      Alert.alert(null, 'Номер автомобиля введен неверно');
      return;
    }
    if (!this.props.isConnected) {
      return Helpers.showNoConnectionMessage();
    }
    this.props.navigation.setParams({ showHeaderLoader: true });
    const { onEdit, ndx } = this.props.navigation.state.params;
    const publicProfile = await Profile.getPublicProfile(this.state.car.number, this.props.accessToken);
    if (!publicProfile || (publicProfile && publicProfile.user_id === this.props.userId)) {
      onEdit(this.state.car, ndx);
      this.props.navigation.dispatch({ type: 'Navigation/BACK' });
    } else {
      Alert.alert(null, 'Автомобиль с таким номером уже зарегистрирован');
    }
    this.props.navigation.setParams({ showHeaderLoader: false });
  }

  onRemoveCar() {
    const { onRemove, ndx } = this.props.navigation.state.params;
    onRemove(ndx);
    this.props.navigation.dispatch({ type: 'Navigation/BACK' });
  }

  onChangeMark(field, text) {
    if (text === 'other') {
      this.setState({
        isMarkEditable: true,
        car: {...this.state.car, [field]: ''},
      });
    } else {
      this.setState({
        car: {...this.state.car, [field]: text},
      })
    }
  }

  render() {
    return (
        <View style={styles.container}>
          <View style={[styles.carContent, profileStyles.separator]}>
              <Form>
                <View style={[styles.lineContent, profileStyles.separator]}>
                  {
                    this.state.isMarkEditable &&
                    <TextField
                      autoFocus={true}
                      title="Марка"
                      titleStyle={styles.title}
                      value={this.state.car.brand}
                      valueStyle={styles.value}
                      placeholder="Введите марку"
                      placeholderTextColor={placeholderTextColor}
                      onChangeText={text => this.setState({car: {...this.state.car, brand: text}})}
                    />
                   ||
                    <LabelField
                      title="Марка"
                      titleStyle={styles.title}
                      editable={false}
                      value={this.state.car.brand}
                      valueStyle={styles.value}
                      onPress={() => this.onSelectBrand()}
                    />
                  }
                </View>
                <View style={[styles.lineContent, profileStyles.separator]}>
                  <TextField
                    title="Модель"
                    placeholder="Введите модель"
                    placeholderTextColor={placeholderTextColor}
                    titleStyle={styles.title}
                    value={this.state.car.model}
                    valueStyle={styles.value}
                    onChangeText={text => this.setState({car: {...this.state.car, model: text}})}
                  />
                </View>
                <View style={styles.lineContent}>
                  <CarNumberInput
                    title="Гос.номер"
                    placeholderTextColor={placeholderTextColor}
                    titleStyle={styles.title}
                    value={this.state.car.number}
                    caretHidden={false}
                    onChangeText={text => this.setState({car: {...this.state.car, number: text.toUpperCase()}})}
                    inputStyle={[styles.value, {flex: 1, textAlign: 'right'}]}
                  />
                </View>
              </Form>
          </View>
            <View style={styles.buttonContainer}>
              <Button
                icon={<Icon name="icon_bin" size={23} color={'red'}/>}
                type="clear"
                titleStyle={styles.buttonTitle}
                title="Удалить"
                onPress={this.onRemoveCar.bind(this)}/>
            </View>
        </View>
    );
  }
}
const placeholderTextColor = '#cacaca';
const styles = StyleSheet.create({
  headerTitle: {
    fontSize: normalize(14.5),
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'stretch',
  },
  lineContent: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingBottom: 20,
    paddingTop: 20,
  },
  carContent: {
  backgroundColor: '#fff',
    paddingLeft: 15,
    paddingRight: 15,
  },
  buttonContainer: {
    flexDirection:'column',
    justifyContent: 'center',
    alignItems: 'stretch',
    marginTop: 25,
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderColor: '#dcdcdc',
  },
  buttonTitle: {
    color:'red',
    fontSize: normalize(19),
    marginVertical: 10,
    paddingHorizontal: 10,
  },
  title: {
    fontSize: normalize(19),
  },
  value: {
    fontSize: normalize(19),
    color:'#cacaca'
  },
});

const isConnected = state => state.network.isConnected;
const getAccessToken = state => state.auth.accessToken;
const getUserId = state => state.auth.userId;

const propsMapping = state => {
  return {
    userId: getUserId(state),
    accessToken: getAccessToken(state),
    isConnected: isConnected(state),
  };
};

export default connect(propsMapping, null)(ViewCar);
