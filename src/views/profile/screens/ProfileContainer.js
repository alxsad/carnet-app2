import React, {Component} from 'react';
import {StyleSheet, Dimensions, Text, View, Image, ScrollView, TouchableOpacity, Alert} from 'react-native';
import {Icon} from '../../../components/index';
import profileStyles from '../styles';
import {connect} from 'react-redux';
import {Form} from '../../../components/form';
import {Content, Tag, CarNumberPlate, Avatar} from '../../../components';
import {normalize} from '../../../conf';
import {NavStyles} from '../../../styles';
import {setInitialState} from '../../../app/actions/profile';

class ProfileContainer extends Component {

  static navigationOptions = ({ navigation }) => ({
    headerLeft: <View/>,
    headerRight:
      <TouchableOpacity onPress={() => navigation.navigate('EditProfile')} >
        <Icon name="icon_edit" style={NavStyles.navIcon} size={24} color={'white'}/>
      </TouchableOpacity>,
  });

  logout() {
    Alert.alert(
      'Вы действительно хотите выйти?',
      'Это приведет к тому что некоторые данные, такие как история переписок, будут утеряны.',
      [
        { text: 'Выйти', onPress: () => this.props.logOut() },
        { text: 'Закрыть' }
      ]
    );
  }

  renderCars() {
    const { cars } = this.props.profile;
    if (cars.length <= 0) {
      return <View style={[styles.emptySection, profileStyles.separator]}>
        <Text>Гараж пуст</Text>
      </View>
    }
    return cars.map((car, i) => (
      <View key={i} style={[profileStyles.lineContent, {justifyContent: 'space-between'}, (i !== cars.length - 1 && profileStyles.separator)]}>
        <Text style={{flex: 1, fontSize: normalize(18)}}>{car.brand} {car.model}</Text>
        <CarNumberPlate carNumber={car.number}/>
      </View>
    ));
  }

  renderInterests() {
    const {interests} = this.props.profile;
    if (interests.length <= 0) {
      return <View style={styles.emptySection}>
        <Text style={profileStyles.hintText}>Интересы позволяют получать интересующие Вас новости</Text>
      </View>
    }
    return (
      <View style={styles.tags}>
      {interests.map((interest, i) =>
        <Tag
          styles={{marginRight: 5,marginBottom: 5}}
          key={i}
        >
          <Text style={{fontSize: normalize(16)}}>{interest}</Text>
        </Tag>
      )}
      </View>
    )}

    renderAvatarContainer = () => {
        const {name, avatar, avatarLocal} = this.props.profile;
        const avatarSource = avatarLocal || avatar;
      return (
          <View style={styles.avatarMainContainer}>
              <Image style={styles.blurImage}
                     source={{uri: avatarSource}}
                     resizeMode="cover"
                     blurRadius={10}>

              </Image>
              <View style={styles.avatarContainer}>
                  <View style={styles.avatarHeader}/>
                  <View style={styles.avatarRoundedContainer}>
                    <Avatar avatarStyle={styles.avatar} url={avatarSource} />
                  </View>
                  <View style={styles.nameContainer}>
                    <Text style={styles.nameText}>{name || ''}</Text>
                  </View>
              </View>
          </View>
      );
    };

  render() {
    const {phone} = this.props.profile;
    return (
      <View style={styles.container}>
          {this.renderAvatarContainer()}
        <ScrollView>
          <View style={styles.profileContainer}>
            <Form>
              <Content title="Телефон">
                <View style={[profileStyles.lineContent, profileStyles.separator, {justifyContent: 'flex-start'}]}>
                  <Icon style={{marginRight: 10}} size={24} name="icon_phone" color="gray" />
                  <Text style={{fontSize: normalize(18)}}>{ '+ ' + phone}</Text>
                </View>
              </Content>
              <Content title="В гараже">
                {this.renderCars()}
              </Content>
              {/*<Content title="Мои интересы">*/}
              {/*  {this.renderInterests()}*/}
              {/*</Content>*/}
            </Form>
          </View>
          <View style={styles.logOutContainer}>
            <TouchableOpacity style={styles.logOutButton} onPress={this.logout.bind(this)} >
              <Text style={styles.logOutText}>Выйти</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    )
  }
}

const propsMapping = (state) => {
  return {
    profile: state.profile,
  }
};
const actionMapping = dispatch => {
  return {
    logOut: () => dispatch(setInitialState()),
  }
};

export default connect(propsMapping, actionMapping)(ProfileContainer);

const { width, height } = Dimensions.get('window');
const avatarContainerHeight = height * 0.425;
const avatarSize = avatarContainerHeight * 0.62;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f2f2',
  },
  emptySection: {
    paddingBottom: 15,
    paddingTop: 15,
  },
  avatarMainContainer: {
    height: avatarContainerHeight,
    width: width,
    position: 'absolute',
    top: 0,
    left: 0,
    backgroundColor: 'transparent',
  },
  blurImage: {
    flex: 1,
      height: avatarContainerHeight,
      width: width,
      position: 'absolute',
  },
  avatarContainer: {
      flex: 1,
      flexDirection:'column',
      justifyContent: 'center',
      alignItems: 'center',
  },
  avatarRoundedContainer: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  nameContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  avatarHeader: {
    flex: 0.1,
  },
  avatar: {
    width: avatarSize,
    height: avatarSize,
    borderRadius: avatarSize / 2,
    borderColor: 'white',
    borderWidth: 2,
  },
  nameText: {
    color: 'white',
    fontSize: normalize(24),
    padding:5,
    textShadowColor:'#000000',
    textShadowOffset:{width:-1, height: 1},
    textShadowRadius:5,
  },
  profileContainer: {
    //flex: 1,
    marginTop: height * 0.34,
    margin: 8,
    backgroundColor: '#fff',
    padding: 10,
    shadowColor: '#a6a6a6',
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.5,
    shadowRadius: 3,
  },
  lineContent: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingBottom: 10,
    paddingTop: 10,
  },
  tags: {
    alignItems: 'flex-start',
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingTop: 20,
    paddingBottom: 10,
  },
  logOutContainer: {
    marginTop: 20,
    margin: 8,
    backgroundColor: '#fff',
    padding: 10,
    shadowColor: '#a6a6a6',
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.5,
    shadowRadius: 3,
  },
  logOutButton: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  logOutText: {
    color: '#FF5B5B',
    fontSize: normalize(16)
  }
});
