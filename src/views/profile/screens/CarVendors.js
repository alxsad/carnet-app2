import React, {Component} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, ScrollView} from 'react-native';
import {Icon} from '../../../components/index';
import {DEFAULT_COLOR, NavStyles} from '../../../styles'

class CarVendors extends Component {

  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    return {
      title: 'Марка',
      headerLeft:
        <TouchableOpacity onPress={() => navigation.dispatch({ type: 'Navigation/BACK' })} >
          <Icon name="icon_back" style={NavStyles.navIcon} size={20} color={'white'}/>
        </TouchableOpacity>,
      headerRight: <View/>,
    }
  };

  renderSelectedFlag(brand) {
    if (brand === this.props.navigation.state.params.value) {
      return  <Icon name="icon_checkmark_small" size={13} color={DEFAULT_COLOR}/>;
    }
  }
  renderRow(row, i) {
    return (
      <View key={i} style={styles.rowContainer}>
        <TouchableOpacity  style={styles.row} onPress={() => this.props.navigation.state.params.onSelect(row)}>
          <Text style={styles.rowText}>{row}</Text>
          {this.renderSelectedFlag(row)}
        </TouchableOpacity>
      </View>
    )
  }
  render() {
    const {collection} = this.props.navigation.state.params;
    return (
      <ScrollView>
        <View style={styles.container}>
          {collection.map(this.renderRow.bind(this))}
          <View style={styles.rowContainer}>
            <TouchableOpacity  style={styles.row} onPress={() => this.props.navigation.state.params.onSelect('other')}>
              <Text style={styles.rowText}>Другая</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 0,
    padding: 15,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    backgroundColor: '#fff',
  },
  rowContainer: {
    borderColor: '#dcdcdc',
    borderBottomWidth: 1,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: 12,
    paddingBottom: 12,
  },
  rowText: {
    fontSize: 15,
  }
});

export default CarVendors;
