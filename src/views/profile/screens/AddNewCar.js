import React, {Component} from 'react';
import {StyleSheet, View, TouchableOpacity, Alert, ActivityIndicator} from 'react-native';
import {Form, TextField, LabelField} from '../../../components/form/index';
import {CarNumberInput} from '../../../components/index';
import {Icon} from '../../../components/index';
import {CarBrands} from '../../../domain';
import profileStyles from '../styles';
import {Profile} from '../../../services';
import {connect} from 'react-redux';
import {NavStyles} from '../../../styles';
import {normalize} from '../../../conf';
import Helpers from '../../../utils/helpers';

class AddNewCar extends Component {

  static navigationOptions = ({ navigation }) => {
    const { params = {showHeaderLoader: false} } = navigation.state;
    return {
      title: 'Добавить ТС',
      headerLeft:
        <TouchableOpacity onPress={() => navigation.dispatch({ type: 'Navigation/BACK' })} >
          <Icon name="icon_back" style={NavStyles.navIcon} size={20} color={'white'}/>
        </TouchableOpacity>,
      headerRight:
        params.showHeaderLoader &&
        <ActivityIndicator style={NavStyles.navIcon} animating color="white" size="small"/> ||
        <TouchableOpacity onPress={() => params.createCar && params.createCar()}>
          <Icon name="icon_checkmark" style={NavStyles.navIcon} size={20} color={'white'}/>
        </TouchableOpacity>
    }
  };

  constructor(props, context){
    super(props, context);
    this.state = {
      car: {
        brand: '',
        model: '',
        number: '',
      },
      isMarkEditable: false,
    };
  };

  componentDidMount() {
    this.props.navigation.setParams({ createCar: () => this.onCreateCar()});
  }

  onSelectBrand() {
    this.props.navigation.navigate('CarVendors', {
      value: this.state.car.brand,
      collection: CarBrands,
      onSelect: value => {
        this.onChangeMark('brand', value);
        this.props.navigation.dispatch({ type: 'Navigation/BACK' });
      },
    });
  }

  onChangeMark(field, text) {
    if (text === 'other') {
      this.setState({
        isMarkEditable: true,
        car: { ...this.state.car, [field]: '' },
      });
    } else {
      this.setState({
        car: { ...this.state.car, [field]: text },
      });
    }
  }

  async onCreateCar() {
    if(this.state.car.number.length <= 0  || this.state.car.brand.length <= 0 || this.state.car.model.length <= 0){
      Alert.alert(null, 'Введите марку, модель и номер автомобиля');
      return;
    }
    if(this.state.car.number.length < 9){
      Alert.alert(null, 'Номер автомобиля введен неверно');
      return;
    }
    const { cars } = this.props.navigation.state.params;
    if (cars && cars.filter((car, index) => car.number === this.state.car.number).length > 0) {
      Alert.alert(null, 'Автомобиль с таким номером уже зарегистрирован');
      return;
    }
    if (!this.props.isConnected) {
      return Helpers.showNoConnectionMessage();
    }
    this.props.navigation.setParams({ showHeaderLoader: true });
    const publicProfile = await Profile.getPublicProfile(this.state.car.number, this.props.accessToken);
    if (!publicProfile) {
      this.props.navigation.state.params.onCreate(this.state.car);
      this.props.navigation.dispatch({ type: 'Navigation/BACK' });
    } else {
      Alert.alert(null, 'Автомобиль с таким номером уже зарегистрирован');
    }
    this.props.navigation.setParams({ showHeaderLoader: false });
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={[styles.carContent, profileStyles.separator]}>
          <Form>
            <View style={[styles.lineContent, profileStyles.separator]}>
              {
                this.state.isMarkEditable &&
                <TextField
                  autoFocus={true}
                  title="Марка"
                  titleStyle={styles.title}
                  value={this.state.car.brand}
                  valueStyle={styles.value}
                  placeholder="Введите марку"
                  placeholderTextColor={placeholderTextColor}
                  onChangeText={text => this.setState({ car: { ...this.state.car, brand: text } })}
                />
                ||
                <LabelField
                  placeholder="Выберите марку"
                  title="Марка"
                  titleStyle={styles.title}
                  editable={false}
                  value={this.state.car.brand}
                  valueStyle={styles.value}
                  onPress={this.onSelectBrand.bind(this)}
                />
              }
            </View>
            <View style={[styles.lineContent, profileStyles.separator]}>
              <TextField
                title="Модель"
                titleStyle={styles.title}
                placeholder="Введите модель"
                placeholderTextColor={placeholderTextColor}
                value={this.state.car.model}
                valueStyle={styles.value}
                onChangeText={text => this.setState({ car: { ...this.state.car, model: text } })}
              />
            </View>
            <View style={styles.lineContent}>
              <CarNumberInput
                titleStyle={styles.title}
                title="Гос.номер"
                value={this.state.car.number}
                placeholderTextColor={placeholderTextColor}
                caretHidden={false}
                onChangeText={text => this.setState({ car: { ...this.state.car, number: text.toUpperCase()}})}
                inputStyle={[styles.value, {flex: 1, textAlign: 'right'}]}
              />
            </View>
          </Form>
        </View>
      </View>
    );
  }
}
const placeholderTextColor = '#cacaca';
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'stretch',
  },
  lineContent: {
    //flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingBottom: 20,
    paddingTop: 20,
  },
  carContent: {
    backgroundColor: '#fff',
    paddingLeft: 15,
    paddingRight: 15,
  },
  title: {
    fontSize: normalize(19),
  },
  value: {
    fontSize: normalize(19),
    color:'#cacaca'
  }
});

const getAccessToken = state => state.auth.accessToken;
const isConnected = state => state.network.isConnected;

const propsMapping = state => {
  return {
    accessToken: getAccessToken(state),
    isConnected: isConnected(state),
  };
};

export default connect(propsMapping, null)(AddNewCar);
