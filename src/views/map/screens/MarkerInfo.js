import React, {Component} from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Dimensions,
  Modal,
  ScrollView,
  ActivityIndicator
} from 'react-native';
import {connect} from 'react-redux';

import ImageViewer from "react-native-image-zoom-viewer";
import moment from 'moment';
import {normalize} from "../../../conf";
import {Icon} from "../../../components";
import {Button} from "react-native-elements";
import {deleteMarker} from "../../../app/actions/map";
import {showMessage} from "react-native-messages";
import {DEFAULT_ERROR_MESSAGE} from "../../../domain/MessagesList";
import {MapConf} from "../../../domain";
import Helpers from "../../../utils/helpers";
import {ifIphoneX} from "react-native-iphone-x-helper";

class MarkerInfo extends Component {

  state = {
    modalVisible: false,
    modalImages: [],
    showDeleteLoader: false
  };

    goBack = () => {
      this.props.navigation.dispatch({ type: 'Navigation/BACK' });
    };

    componentDidMount(){
      const {marker} = this.props;
        this.setState({
            marker: marker
        });

        if(marker.photos.length > 0){
            this.setState({modalImages:[{
                url: marker.photos[0].url,
                freeHeight: true
            }]})
        }
  }

  renderImageView() {
    return (
          <Modal
              visible={this.state.modalVisible}
              onRequestClose={() => this.setState({ modalVisible: false })}
          >
            <ImageViewer
                imageUrls={this.state.modalImages}
                onSwipeDown={() => {
                  this.setState({ modalVisible: false });
                }}
                // onMove={data => console.log(data)}
                enableSwipeDown={true}
                renderHeader={() => this.renderHeader()}
                renderIndicator={() => {return null}}
            />
          </Modal>
    )
  }

  renderHeader = () => {
        return (
            <View style={styles.popupHeader}>
                <TouchableOpacity
                    onPress={() => this.setState({modalVisible: false})}
                    style={{marginRight: 30}}
                >
                    <Icon size={15} color='white'>Закрыть</Icon>
                </TouchableOpacity>
            </View>
        )
    };

  onRemoveMarker = () => {
      const {marker} = this.props;
      this.toggleDeleteLoader();
      this.props.deleteMarker(marker.id, this.goBack, this.onRemoveMarkerError);
  };

  onRemoveMarkerError = () => {
      this.toggleDeleteLoader(false);
      showMessage({ type: 'error', text: DEFAULT_ERROR_MESSAGE });
  };

  toggleDeleteLoader = (val) => {
      this.setState({showDeleteLoader: val ?? !this.state.showDeleteLoader})
  };

  render() {
    const marker = this.state.marker;
    if(marker)
      return (
          <View style={{flex: 1, backgroundColor: '#fff'}}>
            <ScrollView contentContainerStyle={{justifyContent: 'center'}} >
                <View style={styles.container}>
                  <View style={styles.textContainer}>
                    <Text style={[styles.centerText, {fontSize: normalize(30)}]}>
                        {marker.title}
                    </Text>
                    <Text style={[styles.centerText, {fontSize: normalize(15), paddingBottom: 5, color: '#6a6a6a'}]}>
                        добавлено {moment(marker.created_date).fromNow()}
                    </Text>
                    {marker.status === 'unknown' &&
                      <Text style={[styles.centerText, {fontSize: normalize(15), color: 'red'}]}>
                          {`отмечено как неактуальное ${moment(marker.marked_as_unknown_date).fromNow()}`}
                      </Text>
                    }
                    {marker.extra &&
                      Object.keys(marker.extra).map((e, extraIndex) =>
                          <View key={extraIndex} style={{flexDirection: 'row', marginTop: 5, paddingHorizontal: 10}}>
                              {Object.keys(marker.extra[e]).map((p, propIndex) =>
                                  <Text key={propIndex} style={{paddingBottom: 10, fontSize: normalize(15)}}>
                                      {propIndex === 0 && Helpers.capitalizeFirstLetter(MapConf.Optional[e][p]) ||
                                          MapConf.Optional[e][p] }
                                      {Helpers.renderLineDivider(propIndex, marker.extra[e])}
                                  </Text>
                              )}
                          </View>
                    )}

                    <Text style={[styles.centerText, {fontSize: normalize(16)}]}>{marker.description}</Text>
                    {marker.photos.length > 0 &&
                        <View style={styles.thumbWrap}>
                          <TouchableOpacity onPress={() => this.setState({modalVisible: true})}>
                            <Image
                                style={styles.thumb}
                                source={{uri: marker.photos[0].url}}
                                resizeMode="cover"/>
                          </TouchableOpacity>
                        </View>
                    }
                  </View>
                  {this.props.profileId === marker.user_id &&
                      <View style={[styles.buttonContainer, {marginTop: 20}]}>
                              <Button
                                  disabled={this.state.showDeleteLoader}
                                  icon={this.state.showDeleteLoader &&
                                    <ActivityIndicator color={!this.state.showDeleteLoader ? 'red': 'gray'} animating size="small"/> ||
                                    <Icon name="icon_bin" size={23} color={'red'}/>}
                                  type="clear"
                                  titleStyle={[styles.button, {color: 'red'}]}
                                  title="Удалить"
                                  onPress={() => this.onRemoveMarker()}
                              />
                      </View>
                  }
                </View>
            </ScrollView>
            {this.renderImageView()}
          </View>
      );

    return  null;
  }
}

const propsMapping = state => {
  return {
    isConnected: state.network.isConnected,
    profileId: state.profile.id
  };
};

const actionMapping = dispatch => {
    return {
        deleteMarker: (markerId, finishCb, errorCb) => dispatch(deleteMarker(markerId, finishCb, errorCb))
    }
};

export default connect(propsMapping, actionMapping)(MarkerInfo);

const { height, width } = Dimensions.get('window');
const imageHeight = height * 0.4;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  textContainer: {
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  thumbWrap: {
    justifyContent: 'space-around',
    alignItems: 'center',
    marginVertical: 10
  },
  thumb: {
    width: width,
    height: imageHeight,
  },
  popupHeader: {
    zIndex: 15,
    position: 'absolute',
    right: 0,
    backgroundColor: 'transparent',
    top: height/15,
  },
  centerText: {
    fontSize: normalize(16),
    paddingHorizontal: 10,
    paddingBottom: 10
  },
  closeButton: {
      zIndex: 1,
      position: 'absolute',
      left: 3,
      top: height/ 25,
      paddingHorizontal: 12,
  },
    buttonContainer: {
        padding: 10,
        flexDirection:'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderBottomWidth: 1,
        borderTopWidth: 1,
        borderColor: '#dcdcdc',
    },
    button: {
        fontSize: normalize(19),
        paddingHorizontal: 10,
        maxHeight: 30,
        height: 30
    }
});
