import React, { Component } from 'react'
import {StyleSheet, View, Text, Image, TouchableOpacity, Dimensions} from 'react-native';
import {MapConf} from '../../../../domain'
import {normalize} from "../../../../conf";

export default class Type extends Component {
    constructor () {
        super();
        this.state = {
            selectedType: {}
        }
    }

    renderViewTypeIcon(type){
        const imageSize = width / 4.5;
        const typeFormatted = MapConf.TypesDescription[type].replace(' ', '\n');
        return (
            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <TouchableOpacity key={type} onPress={() => this.onTypeClick(type)}>
                    {this.state.selectedType && this.state.selectedType === type
                    &&
                        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                            <Image style={{width: imageSize, height: imageSize}} source={MapConf.MarkerImages[type]} />
                            <Text style={{fontSize: normalize(17), fontWeight: 'bold', textAlign: 'center'}}>
                                {typeFormatted}
                            </Text>
                        </View>
                    ||
                        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                            <Image style={{width: imageSize, height: imageSize}} source={MapConf.MarkerImages[`${type}_gray`]} />
                            <Text style={{fontSize: normalize(17), color: 'gray', textAlign: 'center'}}>
                                {typeFormatted}
                            </Text>
                        </View>
                    }
                </TouchableOpacity>
            </View>
        )
    }

    renderViewTypeIcons = () => {
        return (
            <React.Fragment>
                <View style={{flex: 1, flexDirection: 'row'}}>
                    {this.renderViewTypeIcon('gai')}
                    {this.renderViewTypeIcon('traffic')}
                </View>
                <View style={{flex: 1, flexDirection: 'row'}}>
                    {this.renderViewTypeIcon('camera')}
                    {this.renderViewTypeIcon('accident')}
                </View>
                <View style={{flex: 1, flexDirection: 'row'}}>
                    {this.renderViewTypeIcon('other')}
                </View>
            </React.Fragment>
        )
    };


    onTypeClick(type) {
        this.setState({selectedType: type});
        this.props.addMarkerPropCb({type});
    }

    render () {
       return <View style={styles.typeContainer}>
           <View style={{flex: 0.1, justifyContent: 'center', alignItems:'center', marginTop: 20}}>
               <Text style={{fontSize: normalize(17)}}>
                   Выберите тип метки
               </Text>
           </View>
           <View style={{flex: 0.8, flexDirection: 'column', justifyContent: 'center'}}>
               {this.renderViewTypeIcons()}
           </View>
       </View>
    }
}

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
    typeContainer: {
        flex: 1,
    }
});
