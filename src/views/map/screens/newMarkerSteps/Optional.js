import React, { Component } from 'react'
import {StyleSheet, View, Platform, Image, TouchableOpacity, Dimensions, TextInput, Text} from 'react-native'

import {Content, Icon, ImagePicker} from "../../../../components";
import {normalize, platformText} from "../../../../conf";

import {Modal} from 'react-native';
import ImageViewer from 'react-native-image-zoom-viewer';
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import {AutoGrowingTextInput} from "react-native-autogrow-textinput";
import {ifIphoneX} from 'react-native-iphone-x-helper';

import LinesSelector from './Optional/LinesSelector';
import {MapConf} from "../../../../domain";
import {DEFAULT_COLOR} from "../../../../styles";
import profileStyles from "../../../profile/styles";

const imagePickerOptions = {
    title: Platform.OS === 'ios' ? 'Выберите фото': null,
    takePhotoButtonTitle: 'Сфотографировать',
    chooseFromLibraryButtonTitle: 'Выбрать фотографию',
    cancelButtonTitle: platformText.Cancel,
    mediaType: 'photo',
    maxWidth: 700,
    maxHeight: 700,
};

export default class Optional extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            modalImages: [],
            image: null,
        }
    }

    onDescriptionChange = (event) => {
        const text = event.nativeEvent.text || '';
        this.setState({ desc: text});
        this.props.addMarkerPropCb({description: text})
    };

    onTitleChange(text) {
        this.setState({title: text});
        this.props.addMarkerPropCb({title: text})
    };

    renderImagePicker() {
        return (
                <ImagePicker
                    ref={picker => {this.imagePicker = picker}}
                    cb={this.onSelectImage}
                />
        );
    }

    renderTypeOptionalSelector() {
        const markerType = this.props.markerType;
        if(markerType &&
            markerType === MapConf.MarkerTypes.accident ||
            markerType === MapConf.MarkerTypes.traffic
        ) return (
            <View>
                <Content titleStyle={styles.titleStyle} title="Ряд с затрудненным движением">
                    <View style={styles.lineContent}>
                        <LinesSelector {...this.props}/>
                    </View>
                </Content>
            </View>
            );
        return null;
    }

    renderImageView() {
        return (
            <View style={{padding: 10}}>
                <Modal
                    visible={this.state.modalVisible}
                    onRequestClose={() => this.setState({ modalVisible: false })}
                >
                    <ImageViewer
                        imageUrls={this.state.modalImages}
                        onSwipeDown={() => {
                            this.setState({ modalVisible: false });
                        }}
                        onMove={data => console.log(data)}
                        enableSwipeDown={true}
                        renderHeader={() => this.renderHeader()}
                        renderIndicator={() => {return null}}
                    />
                </Modal>
            </View>
        )
    }

    renderHeader = () => {
        return (
            <View style={styles.popupHeader}>
                <TouchableOpacity
                    onPress={() => this.setState({modalVisible: false})}
                    style={{marginLeft: 30}}
                >
                    <Icon size={15} color='white'>Готово</Icon>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={this.removeImage}
                    style={{marginRight: 30}}
                >
                    <Icon name="icon_bin" size={25} color='white'/>
                </TouchableOpacity>
            </View>
        )
    };

    renderDescriptionInput() {
        return (
            <View style={styles.contentContainer}>
                <Content titleStyle={styles.titleStyle} title="Описание">
                    <View style={[styles.lineContent, profileStyles.separator]}>
                        <AutoGrowingTextInput
                            value={this.state.desc}
                            style={styles.textInput}
                            placeholder={'Что произошло ?'}
                            placeholderTextColor={'#E2E2E2'}
                            underlineColorAndroid="transparent"
                            onChange={(e) => this.onDescriptionChange(e)}
                        />
                    </View>
                </Content>
            </View>
        );
    }

    renderTitleInput = () => {
        const initialTitle = this.props.markerType ? MapConf.TypesDescription[this.props.markerType] : '';
        return(
            <View style={styles.contentContainer}>
                <Content titleStyle={styles.titleStyle} title="Заголовок метки">
                    <View style={[styles.lineContent, profileStyles.separator]}>
                    <TextInput
                        placeholder={initialTitle}
                        placeholderTextColor={'#E2E2E2'}
                        value={this.state.title}
                        style={styles.textInput}
                        maxLength={20}
                        underlineColorAndroid="transparent"
                        onChangeText={text => this.onTitleChange(text)}
                    />
                    </View>
                </Content>
            </View>
        )
    };

    onSelectImage = (image) => {
        this.setState({image, modalImages:[{
                url: image.uri,
                freeHeight: true
            }]});
        this.props.addMarkerPropCb({image})
    };

    removeImage = () => {
        this.setState({
            image: null,
            modalImages:[],
            modalVisible: false
        });

        this.props.addMarkerPropCb({image: null})
    };

    render () {
       const imagePickerDisabled = this.state.image !== null;
       return (
           <View style={{flex: 0.98}}>
               <KeyboardAwareScrollView
                   keyboardDismissMode="interactive"
                   automaticallyAdjustContentInsets={false}
                   keyboardShouldPersistTaps='never'
               >
                   <View style={styles.container}>
                       <React.Fragment>
                           {this.renderTitleInput()}
                           {this.renderDescriptionInput()}
                           {this.renderTypeOptionalSelector()}
                       </React.Fragment>
                       {this.renderImagePicker()}
                       {this.state.image &&
                       <View style={styles.thumbWrap}>
                           <TouchableOpacity onPress={() => this.setState({modalVisible: true})}>
                               <Image
                                   style={styles.thumb}
                                   source={{uri: this.state.image.uri}}
                                   resizeMode="cover"/>
                           </TouchableOpacity>
                           <TouchableOpacity
                               onPress={this.removeImage}
                               style={styles.removeImageButton}
                           >
                               <Icon name="rounded_close" size={35} color={DEFAULT_COLOR}/>
                           </TouchableOpacity>
                       </View>
                       }
                   </View>

               </KeyboardAwareScrollView>

               {this.renderImageView()}

               {!imagePickerDisabled &&
                   <View style={styles.photoAdd}>
                       <TouchableOpacity
                           onPress={() => this.imagePicker.show(imagePickerOptions)}
                       >
                           <Icon name="image_add" size={35} color={DEFAULT_COLOR}/>
                       </TouchableOpacity>
                   </View>
               }
           </View>
       )}
}

const { height, width } = Dimensions.get('window');
const imageHeight = height * 0.4;

const styles = StyleSheet.create({
    container: {
        ...ifIphoneX({
            marginTop: 15,
        }, {
            marginTop: 10,
        }),
        padding: 10,
    },
    contentContainer: {
        marginBottom: 20
    },
    thumbWrap: {
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    thumb: {
        width: width,
        height: imageHeight,
    },
    textInput: {
        width: width,
        fontSize: normalize(17),
        paddingBottom: 0,
        lineHeight: 20,
    },
    photoAdd: {
        position: 'absolute',
        bottom: height/20,
        right: 25,
    },
    removeImageButton: {
        position: 'absolute',
        padding: 10,
        top: 0,
        right: 0,
    },
    popupHeader: {
        zIndex: 5,
        position: 'absolute',
        left: 0,
        right: 0,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: 'transparent',
        ...ifIphoneX({
            top: height/12,
        }, {
            top: height/30,
        })
    },
    lineContent: {
        paddingTop: 10,
        paddingBottom: 15,
    },
    titleStyle: {
        fontSize: normalize(16),
    }
});
