import React, { Component } from 'react';
import { View, ActivityIndicator } from 'react-native';
import { WebView } from 'react-native-webview';
import {DEFAULT_COLOR} from "../../../../styles";


export default class Intro extends Component {
    constructor(props) {
        super(props);
        this.state = {visible: true};
    }

    hideSpinner=()=> {
        this.setState({ visible: false });
    };

    render() {
        return (
            <View style={{ flex: 0.94, paddingHorizontal: 15 }}>
                <WebView
                    cacheEnabled={false}
                    // onLoadStart={() => (this.showSpinner())}
                    onLoad={() => this.hideSpinner()}
                    source={{uri: 'https://carnet.davidovi.ch/markerDescription.html'}}
                    style={{flex: 1}}
                />
                {this.state.visible && (
                    <ActivityIndicator
                        color={DEFAULT_COLOR}
                        style={{
                            flex: 1,
                            left: 0,
                            right: 0,
                            top: 0,
                            bottom: 0,
                            position: 'absolute',
                            alignItems: 'center',
                            justifyContent: 'center' }}
                        size="large"
                    />
                )}
            </View>

        )
    }
}


