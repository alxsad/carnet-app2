import React, {Component} from 'react';
import {View, Text, StyleSheet, Dimensions} from 'react-native';
import MapView, {PROVIDER_GOOGLE} from "react-native-maps";
import Geocoder from "react-native-geocoder";
import {Icon} from "../../../../components";
import {DEFAULT_COLOR} from "../../../../styles";

Geocoder.fallbackToGoogle('AIzaSyDpiFEeY_AcnP3v6trUepKj82zxzn1wIZM');

const initialRegion = {
    latitude: 53.91634580916043,
    longitude: 27.529487907886505,
    latitudeDelta: 0.01,
    longitudeDelta: 0.01,
};

export default class Location extends Component {

    constructor(props) {
        super(props);
        this.state = {
            region: initialRegion,
            address: '',
            isLocationLoaded: false,
        };
    }

    componentDidMount() {
        this._mounted = true;
        navigator.geolocation.getCurrentPosition(
            (position) => {
                const region = {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    latitudeDelta: initialRegion.latitudeDelta,
                    longitudeDelta: initialRegion.longitudeDelta,
                };
                this._mounted && this.setRegion(region);
            },

            (error) => alert(error.message),
            { enableHighAccuracy: true, timeout: 20000 }
        );

        this.watchID = navigator.geolocation.watchPosition((position) => {
        });
    }

    componentWillUnmount() {
        this._mounted = false;
        navigator.geolocation.clearWatch(this.watchID);
    }

    onRegionChangeComplete(region) {
        this.setState({ region },  this.setAddress(region));
    }

    setRegion(region) {
        this.setState({ region }, this.setAddress(region));
        this.map.animateToRegion(region)
    }

    setAddress(coordinates) {
        const position = {
            lat: coordinates.latitude,
            lng: coordinates.longitude,
        };
        Geocoder.geocodePosition(position).then(res => {
            const result = res['0'];
            // const obl = getOblast(result.adminArea);
            // const country = result.country;
            console.log(result.adminArea);
            let address = result.formattedAddress;
            if(result.streetName && result.locality === 'Minsk'){
                address = result.streetName + ', Minsk'
            }
            if(this._mounted){
                this.setState({
                    address: address,
                    isLocationLoaded: true
                });
                this.props.addMarkerPropCb({
                    location: {
                        // address: address,
                        latitude: coordinates.latitude,
                        longitude: coordinates.longitude
                    }});
            }

        })
            .catch(err => {
                console.log(err); return '';
            });
    }

    render() {
        return (
            <View style={{flex: 0.9}}>
                <MapView
                    ref={ map => { this.map = map }}
                    provider={PROVIDER_GOOGLE}
                    style={{flex: 1}}
                    initialRegion={this.state.region}
                    onRegionChangeComplete={this.onRegionChangeComplete.bind(this)}
                >
                </MapView>

                <View style={styles.marker}>
                    <Icon name="marker" size={30} color={DEFAULT_COLOR}/>
                </View>

                <View style={styles.bubble}>
                    <Text style={{ textAlign: 'center' }}>
                        {this.state.address}
                    </Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    marker: {
        position: 'absolute',
        left: (Dimensions.get('window').width / 2) - 10,
        top: (Dimensions.get('window').height * 0.8 / 2) - 50,
    },
    bubble: {
        position: 'absolute',
        bottom: 0,
        width: Dimensions.get('window').width,
        backgroundColor: 'rgba(255,255,255,0.7)',
        paddingHorizontal: 18,
        paddingVertical: 12,
        borderRadius: 20,
    },
});

