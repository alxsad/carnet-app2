import React, {Component} from 'react';
import {ActivityIndicator, Dimensions, Image, StyleSheet, Text, View} from 'react-native';
import Geocoder from "react-native-geocoder";
import MapboxGL from '@mapbox/react-native-mapbox-gl';
import {Bubble, Icon} from "../../../../components";
import {DEFAULT_COLOR} from "../../../../styles";
import {connect} from "react-redux";
import {MapConf} from "../../../../domain";
import moment from 'moment';

Geocoder.fallbackToGoogle('AIzaSyDpiFEeY_AcnP3v6trUepKj82zxzn1wIZM');

const belarusCenterLatitude = 53.9045,
    belarusCenterLongitude = 27.5615;

class Location extends Component {
    constructor(props) {
        super(props);
        this.state = {
            address: '',
            isLocationLoading: false,
            initialRegion: [belarusCenterLongitude, belarusCenterLatitude]
        };
    }

    componentDidMount(): void {
         this._mounted = true;
            navigator.geolocation.getCurrentPosition(
                async ({coords}) => {
                    const position = {
                        longitude: coords.longitude,
                        latitude: coords.latitude,
                    };
                    if(this._mounted){
                        this.setState((state, props) => {
                            return {
                                initialRegion: [position.longitude, position.latitude]
                            }
                        });

                        this.props.addMarkerPropCb && this.props.addMarkerPropCb({
                            location: {
                                latitude: coords.latitude,
                                longitude: coords.longitude
                            }});

                        const address = await this.getAddress(position);
                        this.setState((state, props) => {
                            return {
                                address: address
                            }
                        });
                    }
                },(e) => {
                    console.log(e.message)
                })
    }

    componentWillUnmount(): void {
        this._mounted = false;
    }

    getAddress = async(coords) => {
       const position = {
           lat: coords.latitude,
           lng: coords.longitude,
       };
       const res = await Geocoder.geocodePosition(position);
       return res[0].formattedAddress;
    };

    toggleLocationLoader = () => {
        this.setState({isLocationLoading: !this.state.isLocationLoading});
    };

    onRegionDidChange = async () => {
        this.toggleLocationLoader();

        const center = await this._map.getCenter();
        const coords = {longitude: center[0], latitude: center[1]};
        const address = await this.getAddress(coords);

        this.setState({address: address});
        this.toggleLocationLoader();

        this.props.addMarkerPropCb && this.props.addMarkerPropCb({
             location: {
                 latitude: coords.latitude,
                 longitude: coords.longitude
             }});
    };

    prepareFeatures = () => {
        const {markerType, featureCollection} = this.props;
        const {features} = featureCollection;

        return features.filter((f) => f.properties.icon === markerType);
    };

    renderAnnotations() {
        const features = this.prepareFeatures();
        return features.map((f, i) => {
            const marker = f.properties;
            const calloutTitle = `${marker.title} \nдобавлено ${moment(marker.created).fromNow()}`;
            return <MapboxGL.PointAnnotation
                key={marker.id}
                id={marker.id}
                title={calloutTitle}
                coordinate={f.geometry.coordinates}
            >
                {marker.status === 'active' &&
                    <Image style={{width: 30, height: 30}} source={MapConf.MarkerImages[marker.icon]}/> ||
                    <Image style={{width: 30, height: 30}} source={MapConf.MarkerImages[`${marker.icon}_gray`]}/>
                }

                <MapboxGL.Callout title={calloutTitle}>
                </MapboxGL.Callout>
            </MapboxGL.PointAnnotation>
        });
    }

    render() {
        if(!this.props.markerType){
            return null;
        }
        return (
            <View style={{flex: 0.94}}>
                <MapboxGL.MapView
                    onRegionDidChange={this.onRegionDidChange}
                    zoomLevel={14}
                    ref={c => (this._map = c)}
                    onPress={this.onPress}
                    centerCoordinate={this.state.initialRegion}
                    style={{flex: 1}}
                    logoEnabled={false}
                    attributionEnabled={false}
                    pitchEnabled={false}
                    compassEnabled={false}
                    // showUserLocation={true}
                    // userTrackingMode={MapboxGL.UserTrackingModes.Follow}
                >
                    {this.renderAnnotations()}
                </MapboxGL.MapView>

                <View style={styles.marker}>
                    <Icon name="marker" size={30} color={DEFAULT_COLOR}/>
                </View>
                <Bubble
                    isVisible={true}
                    style={styles.bubble}
                >
                    {
                        !this.state.isLocationLoading &&
                        <Text style={{ textAlign: 'center' }}>
                            {this.state.address}
                        </Text> ||
                        <ActivityIndicator
                            animating={true}
                            size="small"
                            color={DEFAULT_COLOR}
                        />
                    }
                </Bubble>
            </View>
        );
    }
}


const propsMapping = state => {
    return {
        featureCollection: state.map.featureCollection,
    };
};

export default connect(propsMapping, null)(Location);

const styles = StyleSheet.create({
    marker: {
        position: 'absolute',
        left: (Dimensions.get('window').width / 2) - 11,
        top: (Dimensions.get('window').height * 0.87 / 2) - 50,
    },
    bubble: {
        bottom: 0,
        left: 0,
        width: Dimensions.get('window').width,
        backgroundColor: 'rgba(255,255,255,0.7)',
    }
});

