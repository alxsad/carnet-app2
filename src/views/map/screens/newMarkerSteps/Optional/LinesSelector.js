import React, { Component } from 'react'
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import {Checkbox} from "../../../../../components";
import {normalize} from "../../../../../conf";
import {MapConf} from '../../../../../domain';
import Helpers from '../../../../../utils/helpers';

export default class LinesSelector extends Component {
    constructor () {
        super();
        this.state = {
            selectedType: {}
        }
    }

    onCheckBoxPress = line => {
        switch (line){
            case 'left':
                var {left, ...other} = this.state.lines ?? {};
                if(left){
                    this.setLine(other);
                }
                else {
                    this.setLine({...this.state.lines, left: true});
                }
                break;
            case 'center':
                var {center, ...other} = this.state.lines ?? {};
                if(center){
                    this.setLine(other);
                }
                else {
                    this.setLine({...this.state.lines, center: true});
                }
                break;
            case 'right':
                var {right, ...other} = this.state.lines ?? {};
                if(right){
                    this.setLine(other);
                }
                else {
                    this.setLine({...this.state.lines, right: true});
                }
                break;
        }
    };

    setLine(linesObj){
        this.setState({lines: {...linesObj}});
        let res = null;

        if(Object.keys(linesObj).length > 0){
            res = {lines: {...linesObj}};
        }
        this.props.addMarkerPropCb({extra: res});
    }

    renderLineDevider = (index) => {
        const lines = Object.keys(this.state.lines);
        const DOT = '.';
        const COMMA = ', ';
        switch(index){
            case 0:
                if(lines.length === 1) return DOT;
                return COMMA;
            case lines.length -1 :
                return DOT;
            default:
               return COMMA;
        }
    };

    render () {
       const {left, center, right} = this.state.lines ?? {};
       return (
           <View style={styles.chckbxContainer}>
                       <View style={styles.chckbxs}>
                           <TouchableOpacity
                               style={styles.chckbx}
                               onPress={() => this.onCheckBoxPress('left')}
                           >
                               <Checkbox size={30} checked={left}/>
                           </TouchableOpacity>
                           <TouchableOpacity
                               style={styles.chckbx}
                               onPress={() => this.onCheckBoxPress('center')}
                           >
                               <Checkbox size={30} checked={center}/>
                           </TouchableOpacity>
                           <TouchableOpacity
                               style={styles.chckbx}
                               onPress={() => this.onCheckBoxPress('right')}
                           >
                               <Checkbox size={30} checked={right}/>
                           </TouchableOpacity>
                       </View>
                       <View style={{flexDirection: 'row', marginTop: 5}}>
                           {this.state.lines && (
                               Object.keys(this.state.lines)
                               .map((key, i) => <Text style={{fontSize: normalize(16),}} key={i}>{MapConf.Optional.lines[key]}
                                   {Helpers.renderLineDivider(i, this.state.lines)}</Text>)
                           ) ||
                           <Text>{''}</Text>}
                       </View>
                   </View>
       )
    }
}

const styles = StyleSheet.create({
    chckbxContainer: {
        marginTop: 10,
        marginBottom: 15,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },
    chckbxs: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'

    },
    chckbx: {
        marginRight: 20,
        marginLeft: 20
    }
});
