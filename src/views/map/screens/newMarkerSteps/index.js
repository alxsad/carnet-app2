import React, { Component } from 'react';
import {StyleSheet, View, Platform, SafeAreaView} from 'react-native';
import { IndicatorViewPager, PagerDotIndicator } from 'rn-viewpager';
import {Button} from 'react-native-elements'

import Step1 from './Intro';
import Step3 from './Location1';
import Step2 from './Type';
import Step4 from './Optional';
import {DEFAULT_COLOR} from "../../../../styles";

const PAGES = [1, 2, 3, 4];
import {connect} from "react-redux";
import {addMarker} from "../../../../app/actions/map";
import {showMessage} from "react-native-messages";
import {DEFAULT_ERROR_MESSAGE, NO_CONNECTION} from "../../../../domain/MessagesList";

class Index extends Component {
  constructor (props) {
    super(props);
    this.state = {
      currentPage: 0,
      isLoading: false,
      marker: {},
    }
  }

  componentWillReceiveProps (nextProps, nextState) {
    if (nextState.currentPage && nextState.currentPage !== this.state.currentPage) {
      if (this.viewPager) {
        this.viewPager.setPage(nextState.currentPage)
      }
    }
  }

  _renderDotIndicator() {
    return <PagerDotIndicator
                               dotStyle={{backgroundColor: '#b7deff'}}
                               selectedDotStyle={styles.dotSelected}
                               pageCount={PAGES.length}
                               hideSingle={false}
    />;
  }

  saveFinishCb = (marker) => {
    this.toggleLoading();
    this.props.addRecentlyCreatedMarker(marker);
    this.props.navigation.dispatch({ type: 'Navigation/BACK' });
  };

  onError = () => {
    this.toggleLoading();
    showMessage({ type: 'error', text: DEFAULT_ERROR_MESSAGE });
  };

  onNextPress = position => {
    if (!this.props.isConnected){
      showMessage({type: 'warning', text: NO_CONNECTION});
      return;
    }

    if(position === PAGES.length){
      this.toggleLoading();
      this.props.addMarker(this.state.marker, this.saveFinishCb, this.onError);
      return;
    }

    this.setState({ currentPage: position });
    this.viewPager.setPage(position);
  };

  renderViewPagerPage = page => {
    switch (page){
      case 1:
        return <View key={1}>
            <Step1/>
        </View>;
      case 2:
        return <View key={2}>
            <Step2
            addMarkerPropCb={this.addMarkerProp}
            />
        </View>;
      case 3:
        return <View key={3}>
            <Step3
            addMarkerPropCb={this.addMarkerProp}
            markerType={this.state.marker.type}
        />
        </View>;
      default:
        return <View key={4}>
          <Step4
              markerType={this.state.marker.type}
              addMarkerPropCb={this.addMarkerProp}
          />
        </View>;
    }
  };

  addMarkerProp = prop => {
    let result = {...this.state.marker, ...prop};
    Object.keys(prop).map((key, i) => {
          if (!prop[key]) {
            delete result[key];
          }
        }
    );
    this.setState({marker: result});
  };

  toggleLoading = () => {
    this.setState({isLoading: !this.state.isLoading})
  };

  render () {
    const swipeEnabled = (!(this.state.currentPage === 1 && !this.state.marker.type));
    return (
        <SafeAreaView style={{flex: 1}}>
        <View style={styles.container}>
          <IndicatorViewPager
              style={{ flex: 1 }}
              ref={viewPager => {
                this.viewPager = viewPager
              }}
              onPageSelected={page => {
                this.setState({ currentPage: page.position })
              }}
              {
                ...(Platform.OS === 'ios' && {scrollEnabled:swipeEnabled} || {horizontalScroll:swipeEnabled})
              }
              indicator={this._renderDotIndicator()}
          >
            {PAGES.map(page => this.renderViewPagerPage(page))}
          </IndicatorViewPager>
          <Button
              loading={this.state.isLoading}
              containerStyle={styles.nextButton}
              title={this.state.currentPage === PAGES.length - 1 ? 'Добавить' : 'Далее'}
              type="outline"
              onPress={() => {
                this.onNextPress(this.state.currentPage + 1);

              }}
              disabled={this.state.currentPage === 1 && !this.state.marker.type}
          />
        </View>
        </SafeAreaView>
    )
  }
}

const propsMapping = state => {
  return {
    isConnected: state.network.isConnected,
  };
};

const actionMapping = dispatch => {
  return {
    addMarker: (m, finishCb, errorCb) => dispatch(addMarker(m, finishCb, errorCb)),
  }
};

export default connect(propsMapping, actionMapping)(Index);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff'
  },
  page: {
    flex: 1,
    // padding: 10,
    paddingBottom: 20,
  },
  nextButton: {
    bottom: 0,
    width: '100%',
    paddingBottom: 12,
    paddingHorizontal: 18,
  },
    dotSelected: {
    height: 8,
    width: 8,
    borderRadius: 8 >> 1,
    margin: 8 >> 1,
    backgroundColor: DEFAULT_COLOR,
    opacity: 0.5
  }
});
