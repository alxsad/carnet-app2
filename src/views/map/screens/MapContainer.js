import React, {Component} from 'react';
import {Dimensions, StyleSheet, View, TouchableOpacity, ActivityIndicator, PixelRatio, Platform} from 'react-native';
import {connect} from 'react-redux';

import MapboxGL from '@mapbox/react-native-mapbox-gl';

import {DEFAULT_COLOR} from '../../../styles/app';
import {MapConf} from '../../../domain';

import {getMarkers, setSelectedMarker} from '../../../app/actions/map';
import Steps from "./newMarkerSteps";
import {Icon} from '../../../components';
import Permissions from "react-native-permissions";
import {showMessage} from "react-native-messages";
import {DEFAULT_ERROR_MESSAGE, NO_CONNECTION} from "../../../domain/MessagesList";
import MapFilters from './MapFilters';

MapboxGL.setAccessToken('pk.eyJ1IjoiZXpoYTg2IiwiYSI6ImNqdmd4ZGFvODBiaDM0YXA3OHM2b28zOW8ifQ.NuVgm0J7nDwRiai2e_zNXw');

const belarusCenterLatitude = 53.9045,
    belarusCenterLongitude = 27.5615;

class MapContainer extends Component {

  static navigationOptions = ({ navigation }) => {
    return {
      header: null,
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      showMapLoader: false,
    };
  }

  async componentWillMount() {
      let locationPermissionEnabled = false;
      try {
          const locationPermissionStatusCheck = await Permissions.check("location", {
                type: "whenInUse"
            });

          if(locationPermissionStatusCheck === 'undetermined'){
              const locationPermissionStatusRequest = await Permissions.request("location", {
                  type: "whenInUse"
              });
              if(locationPermissionStatusRequest === 'authorized'){
                  locationPermissionEnabled = true;
              }
          }
          if (locationPermissionStatusCheck === "authorized") {
              locationPermissionEnabled = true;
          }
      }
      catch (error) {
      }
      this.setState({locationPermissionEnabled: locationPermissionEnabled})
  }

  componentDidMount() {
    this.onLoadMarkers();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
      const currRecentlyAddedMarker = this.state.recentlyAddedMarker;
      const prevRecentlyAddedMarker = prevState.recentlyAddedMarker;
      if(currRecentlyAddedMarker)
      {
          if(prevRecentlyAddedMarker){
              if(currRecentlyAddedMarker.properties.created === prevRecentlyAddedMarker.properties.created){
                  return;
              }
          }
          this._map.moveTo(currRecentlyAddedMarker.geometry.coordinates, 400);
      }
  }

  setFeatureUnselected = () => {
    this.props.setSelectedMarker(null);
  };

  onLoadMarkers = () => {
      if (!this.props.isConnected){
          showMessage({type: 'warning', text: NO_CONNECTION});
          return;
      }
      this.toggleMapLoader();
      this.props.getMarkers(this.toggleMapLoader, this.onError);
  };

  onSourceLayerPress = async (e) => {
    const feature = e.nativeEvent.payload;
    if(feature.properties.cluster){
        this.setFeatureUnselected();
        return;
    }

    let screenPoints = await this._map.getPointInView([feature.geometry.coordinates[0], feature.geometry.coordinates[1]]);
      if (Platform.OS === 'android') {
          screenPoints = screenPoints.map(x => x / PixelRatio.get());
      }

    if(this.centralizeFeature(screenPoints)){
      this._map.moveTo(feature.geometry.coordinates, 400);
    }
    this.props.setSelectedMarker(feature);
  };

  centralizeFeature = (screenPoints) => {
    const heightMaxRange = height - height / 2.2;
    return heightMaxRange <= screenPoints[1];
  };

  async openAddMarkerModal() {
      if (!this.props.isConnected){
          showMessage({type: 'warning', text: NO_CONNECTION});
          return;
      }
      this.props.navigation.navigate('MapModal', {
        content: (
          <Steps
            navigation={this.props.navigation}
            addRecentlyCreatedMarker={(marker) => this.addRecentlyCreatedMarker(marker)}
          />
        ),
        gesturesEnabled: false,
        title: 'Новый маркер',
      });
  }

  initMapProps() {
    if (!this.state.locationPermissionEnabled) {
        return {
            centerCoordinate: [belarusCenterLongitude, belarusCenterLatitude],
            zoomLevel: 5
        }
    }
        return {
            showUserLocation: true,
            zoomLevel: 11,
            userTrackingMode: MapboxGL.UserTrackingModes.Follow
      }
  }

  onMapPress = () => {
    this.setFeatureUnselected();
  };

  toggleMapLoader = () => {
    this.setState({showMapLoader: !this.state.showMapLoader})
  };

  onError() {
      showMessage({ type: 'error', text: DEFAULT_ERROR_MESSAGE });
  }

  onLongPress = () => {
    this.onLoadMarkers();
  };

  prepareFeatures = () => {
    let {features, ...other} = this.props.featureCollection;
    const selectedFeature = this.props.selectedMarker;

    if(selectedFeature){
      features = features.map(obj => {
        if(obj.properties.id === selectedFeature.properties.id){
          return{ ...obj, properties: selectedFeature.properties}
        }
         return obj;
      });
    }

    const pins = features.filter(_ => this.props.envFilters[_.properties.icon] === true);

    return {
      staticPins: {...other, features: pins},
    };
  };

  renderMapLoader = () => {
      if (!this.state.showMapLoader) {
        return null;
      }
      return (
          <View style={styles.loader}>
            <ActivityIndicator color={DEFAULT_COLOR} animating size="large"/>
          </View>
      )
  };

  addRecentlyCreatedMarker = (marker) => {
      this.setState({recentlyAddedMarker: marker});
  };

  render() {
    const {staticPins} = this.prepareFeatures();
    return (
          <View style={styles.container}>
            <MapboxGL.MapView
                onPress={this.onMapPress}
                onLongPress={this.onLongPress}
                ref={c => (this._map = c)}
                {
                  ...(this.initMapProps())
                }
                logoEnabled={false}
                attributionEnabled={false}
                pitchEnabled={false}
                style={{flex: 1}}
                >
              <MapboxGL.ShapeSource
                  id="symbolLocationSource"
                  hitbox={{width: 20, height: 20}}
                  onPress={this.onSourceLayerPress}
                  shape={staticPins}
                  cluster
                  clusterRadius={25}
                  clusterMaxZoom={14}
                  images={{
                    gai: MapConf.MarkerImages['gai'],
                    camera: MapConf.MarkerImages['camera'],
                    traffic: MapConf.MarkerImages['traffic'],
                    accident: MapConf.MarkerImages['accident'],
                    other: MapConf.MarkerImages['other'],
                    gai_gray: MapConf.MarkerImages['gai_gray'],
                    camera_gray: MapConf.MarkerImages['camera_gray'],
                    traffic_gray: MapConf.MarkerImages['traffic_gray'],
                    accident_gray: MapConf.MarkerImages['accident_gray'],
                    other_gray: MapConf.MarkerImages['other_gray']
                  }}
              >
                <MapboxGL.SymbolLayer
                    id="pointCount"
                    style={mapStyles.clusterCount}
                />
                <MapboxGL.CircleLayer
                    id="clusteredPoints"
                    belowLayerID="pointCount"
                    filter={['has', 'point_count']}
                    style={mapStyles.clusteredPoints}
                />
                <MapboxGL.SymbolLayer
                    id="singlePoint"
                    filter={['all',['==', 'status', 'active'], ['!has', 'point_count'], ['!has', 'selected']]}
                    style={mapStyles.singlePoint}
                />
                <MapboxGL.SymbolLayer
                    id="singlePointGray"
                    belowLayerID="singlePoint"
                    filter={['all',['==', 'status', 'unknown'], ['!has', 'point_count'], ['!has', 'selected']]}
                    style={mapStyles.singlePointGray}
                />
                <MapboxGL.SymbolLayer
                    id="selected"
                    belowLayerID="singlePoint"
                    style={mapStyles.selected}
                    filter={['all', ['==', 'status', 'active'], ['has', 'selected']]}
                />
                  <MapboxGL.SymbolLayer
                      id="selectedGray"
                      belowLayerID="singlePointGray"
                      style={mapStyles.selectedGray}
                      filter={['all',['==', 'status', 'unknown'], ['has', 'selected']]}
                  />
              </MapboxGL.ShapeSource>
            </MapboxGL.MapView>

            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('MapModal', {
                content: <MapFilters/>,
                title: 'Фильтр',
                icon: 'icon_checkmark',
              })}
              style={[styles.menuButton, {top: height * 0.4}]}
            >
                <View style={styles.iconContainer}>
                  <Icon name="filter" size={width/10} color={DEFAULT_COLOR}/>
                </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.openAddMarkerModal() }
              style={[styles.menuButton, {top: height * 0.5}]}
            >
                <View style={styles.iconContainer}>
                    <Icon name="plus_fat" size={width/9} color={DEFAULT_COLOR}/>
                </View>
            </TouchableOpacity>
            {this.renderMapLoader()}
          </View>
    );
  }
}
const propsMapping = state => {
  return {
    envFilters: state.filtersPersisted,
    featureCollection: state.map.featureCollection,
    selectedMarker: state.map.selectedMarker,
    isConnected: state.network.isConnected,
  };
};

const actionMapping = dispatch => {
  return {
    getMarkers: (finishCb, errorCb) => dispatch(getMarkers(finishCb, errorCb)),
    setSelectedMarker: (selectedMarker) => dispatch(setSelectedMarker(selectedMarker))
  }
};

export default connect(propsMapping, actionMapping)(MapContainer);

const { width, height } = Dimensions.get('window');

const mapStyles = MapboxGL.StyleSheet.create({
  singlePoint: {
    iconImage: '{icon}',
    iconAllowOverlap: false,
    iconSize: width * 0.0014
  },
  singlePointGray: {
      iconImage: '{icon}_gray',
      iconAllowOverlap: false,
      iconSize: width * 0.0014
  },
  recentlyAdded: {
    iconImage: '{icon}',
    iconAllowOverlap: true,
    iconSize: width * 0.0014
  },
  selected: {
    iconImage: '{icon}',
    iconAllowOverlap: true,
    iconSize: width * 0.0022
  },
  selectedGray: {
      iconImage: '{icon}_gray',
      iconAllowOverlap: true,
      iconSize: width * 0.0022
  },
  clusteredPoints: {
    circlePitchAlignment: 'map',
    circleColor: MapboxGL.StyleSheet.source(
        [
          // [25, DEFAULT_COLOR],
          // [50, DEFAULT_COLOR],
          // [75, 'blue'],
          // [100, 'orange'],
          // [300, 'pink'],
          [750, 'white'],
        ],
        'point_count',
        MapboxGL.InterpolationMode.Exponential,
    ),

    circleRadius: MapboxGL.StyleSheet.source(
        [[0, 15], [100, 20], [750, 30]],
        'point_count',
        MapboxGL.InterpolationMode.Exponential,
    ),

    circleOpacity: 0.84,
    circleStrokeWidth: 2,
    circleStrokeColor: DEFAULT_COLOR,
  },

  clusterCount: {
    textField: '{point_count}',
    textSize: 12,
    textPitchAlignment: 'map',
  },
});

const styles = StyleSheet.create({
  container: {
    width: width,
    height: height,
  },
  menuButton: {
    position: 'absolute',
    right: 15
  },
  iconContainer: {
      width: width/7, height: width/7,
      backgroundColor: '#ffffff',
      borderRadius: width/7/2,
      opacity: 0.8,
      justifyContent: 'center', alignItems: 'center',
      ...Platform.select({
          ios: {
              shadowColor: '#000',
              shadowOffset: { width: 0, height: 2 },
              shadowOpacity: 0.8,
              shadowRadius: 2,
          },
          android: {
              elevation: 5,
          },
      }),
  },
  loader: {
    position: 'absolute',
    right: 0,
    left: 0,
    top: width/7
  }
});
