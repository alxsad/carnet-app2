import React, {Component} from 'react';
import {StyleSheet, Text, View, Switch, Platform, ScrollView} from 'react-native';
import {connect} from 'react-redux';
import {normalize} from '../../../conf';
import {DEFAULT_COLOR} from '../../../styles';
import {switchFilterState} from "../../../app/actions/mapFilters";
import {MapConf} from "../../../domain";
import offenseStyles from "../../offense/styles";

class MapFilters extends Component {

  state = {
    filters: this.props.filters
  };

  toggleSwitch = (val, type) => {
    const filter = {[type]: val};
    this.props.switchFilterState(filter);
    this.setState({filters: {...this.state.filters, ...filter}});
  };

  render() {
    return (
      <ScrollView contentContainerStyle={styles.content}>
        {MapConf.Markers.map((m, i) =>
          <View key={i}
                style={[styles.filterContent, MapConf.Markers.length - 1 !== i && offenseStyles.separator]}>
            <Text style={{fontSize: normalize(17)}}>{MapConf.TypesDescription[m]}</Text>
            <Switch
              thumbColor={Platform.OS === 'android' && DEFAULT_COLOR}
              trackColor={{true: DEFAULT_COLOR}}
              onValueChange={(v) => this.toggleSwitch(v, MapConf.MarkerTypes[m])}
              value={this.state.filters[MapConf.MarkerTypes[m]]}/>
          </View>
        )}
      </ScrollView>
    );
  }
};

const styles = StyleSheet.create({
  content: {
    flex: 1,
    paddingHorizontal: 30,
    paddingVertical: 30,
  },
  filterContent: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 15,
  },
});

const propsMapping = state => {
  return {
    filters: state.filtersPersisted
  };
};

const actionMapping = dispatch => {
  return {
    switchFilterState: (filter) => dispatch(switchFilterState(filter)),
  }
};

export default connect(propsMapping, actionMapping)(MapFilters);
