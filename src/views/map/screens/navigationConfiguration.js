'use strict';
import React from 'react';

import { StackNavigator } from 'react-navigation'
// screens
import MapContainer from './MapContainer'
import MarkerInfo from "../../map/screens/MarkerInfo";

const routeConfiguration = {
  MapContainer: {
    screen: MapContainer,
  },
  MarkerInfo: {
    screen: MarkerInfo,
    navigationOptions: ({navigation}) => ({
      tabBarVisible: false,
    })
  },
};

const stackNavigatorConfiguration = {
  navigationOptions: ({ screenProps }) => ({
  }),
  initialRouteName: 'MapContainer'
};

export const MapNavigation = StackNavigator(routeConfiguration, stackNavigatorConfiguration);
