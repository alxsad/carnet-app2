'use strict';

// React
import React, {Component} from 'react'
import {
    SafeAreaView,
    StatusBar,
    View,
} from 'react-native';

// Navigation
import { addNavigationHelpers } from 'react-navigation'
import { MapNavigation } from './screens/navigationConfiguration'

// Redux
import { connect } from 'react-redux'

const mapStateToProps = (state) => {
  return {
    navigationState: state.mapTab
  }
};
class TabMapNavigation extends Component {

  render(){
    const { navigationState, dispatch } = this.props;
    return (
      <View style={{ flex: 1 }}>
        <MapNavigation
          navigation={
            addNavigationHelpers({
              dispatch: dispatch,
              state: navigationState
            })
          }
        />
      </View>
    )
  }
}
export default connect(mapStateToProps)(TabMapNavigation)
