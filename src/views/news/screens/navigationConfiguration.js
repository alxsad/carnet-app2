'use strict';
import React from 'react';

import { StackNavigator } from 'react-navigation'
import {NavStyles} from '../../../styles';
// screens
import NewsContainer from './NewsContainer'
import SingleNews from './SingleNews'

const routeConfiguration = {
  NewsContainer: {
    screen: NewsContainer,
  },
  SingleNews: {
    screen: SingleNews,
  }
};

const stackNavigatorConfiguration = {
  navigationOptions: ({ screenProps }) => ({
    headerStyle: NavStyles.headerNavStyle,
    title: 'Новости',
    headerTintColor: '#ffffff',
    headerTitleStyle: {alignSelf: 'center'},
  }),
  initialRouteName: 'NewsContainer'
};

export const NewsNavigation = StackNavigator(routeConfiguration,stackNavigatorConfiguration);
