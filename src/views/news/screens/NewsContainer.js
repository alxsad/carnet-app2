import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import {TopTapBar} from '../../../components/tapBar/index';
import NewsList from './NewsList'
import {connect} from 'react-redux';
import {getNewsByGai, getNewsByInterests} from '../../../app/actions/news';

class NewsContainer extends Component {

  renderScene = route => {
    const {navigation} = this.props;
    switch (route.key) {
      case '1':
        return <NewsList
          userId={this.props.userId}
          items={this.props.interestNews}
          fetchCallback={this.props.getNewsByInterests}
          navigation = {navigation}
          isPartnerView = {true}
        />;
      case '2':
        return <NewsList
          userId={this.props.userId}
          items={this.props.gaiNews}
          fetchCallback={this.props.getNewsByGai}
          navigation = {navigation}
        />;
      default:
        return null;
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.newsListContainer}>
          <TopTapBar
            tapBarRoutes={this.props.tapBarRoutes}
            renderAction={this.renderScene}
            lazy={true}
          />
        </View>
      </View>
    );
  }
}
const propsMapping = state => {
  return {
    gaiNews: state.news.gaiNews,
    interestNews: state.news.interestNews,
    userId: state.profile.id
  };
};

const actionMapping = dispatch => {
  return {
    getNewsByGai: (params, finish) => dispatch(getNewsByGai(params, finish)),
    getNewsByInterests: (params, finish) => dispatch(getNewsByInterests(params, finish)),
  }
};

export default connect(propsMapping, actionMapping)(NewsContainer);

NewsContainer.defaultProps = {
  tapBarRoutes: [
    { key: '1', title: 'Мои интересы' },
    { key: '2', title: 'УГАИ МВД' },
  ],
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  newsListContainer: {
    flex: 1,
  },
});
