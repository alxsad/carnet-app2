import React, {Component} from 'react';
import {View, Text, TouchableOpacity, StyleSheet, Dimensions} from 'react-native';
import {DEFAULT_COLOR} from '../../../styles/app'
import moment from 'moment';
import FastImage from 'react-native-fast-image';
const {height} = Dimensions.get('window');

class NewsRow extends Component {

  shouldComponentUpdate(nextProps) {
    return nextProps.item.id !== this.props.item.id;
  }

  renderTags(tags) {
    return (
      <View style={styles.tagsContainer}>
        {tags.map((tag, i) => <Text style={styles.tag} key={i}>{'#' + tag}</Text>)}
      </View>
    );
  }

  render() {
    const { item, onPress, selfStyle } = this.props;
    return (
      <TouchableOpacity onPress={() => onPress(item)} activeOpacity={1}>
        <View style={[styles.container, selfStyle]}>
          <FastImage
            style={styles.image}
            source={{
              uri: item.image,
              priority: FastImage.priority.high,
            }}
            resizeMode={FastImage.resizeMode.cover}
          />
          <View style={styles.descContainer}>
            <Text style={styles.date}>{moment(item.date).format('L')}</Text>
            <Text style={styles.title}>{item.title}</Text>
            <Text numberOfLines={8} style={styles.desc}>{item.description}</Text>
            {item.tags && this.renderTags(item.tags)}
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    borderWidth: 1,
    backgroundColor: '#fff',
    borderColor: 'rgba(0,0,0,0.1)',
    marginTop: 4,
    marginBottom: 4,
    marginRight: 8,
    marginLeft: 8,
    shadowColor: '#ccc',
    shadowOffset: { width: 2, height: 2, },
    shadowOpacity: 0.5,
    shadowRadius: 3,
    flexDirection: 'column',
  },
  image: {
    height: height * 0.3,
    flex: 1,
  },
  descContainer: {
    flex: 1,
    padding: 10,
  },
  title: {
    fontSize: 20,
    lineHeight: 22,
    fontWeight: 'bold',
    paddingTop: 5,
    marginBottom: 15,
  },
  desc: {
    flex: 1,
    fontSize: 16,
    lineHeight: 22,
  },
  date: {
    color: '#b0b0b0',
    //alignSelf: 'flex-start',
    marginTop: 5,
    marginBottom: 5,
  },
  tagsContainer: {
    alignItems: 'flex-start',
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 5,
    marginBottom: 5,
  },
  tag: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    borderColor: '#CDE6FF',
    borderWidth: 1,
    padding: 2,
    marginRight: 4,
    marginBottom: 2,
    color: DEFAULT_COLOR,
    fontSize: 10,
  },
});

export default NewsRow;
