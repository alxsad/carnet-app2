import React, {Component} from 'react';
import {StyleSheet, View, FlatList, ActivityIndicator, RefreshControl} from 'react-native';
const LIMIT_PER_PAGE = 10;
import moment from 'moment';
import {connect} from 'react-redux';
import Helpers from '../../../utils/helpers';
import NewsRow from './NewsRow';
import {DEFAULT_COLOR} from '../../../styles/app'

class NewsList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      refreshing: false,
    }
  }

  componentDidMount() {
    this.setState({ loading: true });
    this.makeRequest();
  }

  makeRequest(date) {
    if (!this.props.isConnected) {
      this.onFinishLoading();
      return Helpers.showNoConnectionMessage();
    }
    const { fetchCallback, userId } = this.props;
    const latestNewsDate = date || moment().format();
    const params = {
      limit: LIMIT_PER_PAGE,
      date: encodeURIComponent(latestNewsDate),
      userId: userId,
      isRefreshing: !date,
    };

    fetchCallback(params, this.onFinishLoading.bind(this));
  };

  loadMore() {
    const { items } = this.props;
    if (!this.onEndReachedCalledDuringMomentum && !this.state.loading) {
      const date = items.length ? items[items.length - 1].date : moment().format();
      this.makeRequest(date);
    }
  };

  handlePullToRefresh() {
    this.setState({ refreshing: true, loading: false }, () => this.makeRequest());
  };

  onFinishLoading() {
    this.setState({ loading: false, refreshing: false });
    this.onEndReachedCalledDuringMomentum = true;
  };

  openSingleNew(item) {
    if (!this.props.isConnected) {
      return Helpers.showNoConnectionMessage();
    }
    this.props.navigation.navigate('SingleNews', {
      newsId: item.id,
      isPartnerView: !!this.props.isPartnerView
    });
  }

  renderRow({item, index}) {
    let margin = null;
    if(index === 0){
      margin = {marginTop: 8}
    }
    return <NewsRow selfStyle={margin} onPress={ () => this.openSingleNew(item) } item={item} />;
  }

  renderFooter() {
    if (!this.state.loading) {
      return null;
    }
    return (
      <View style={styles.footer}>
        <ActivityIndicator color={DEFAULT_COLOR} animating size="large"/>
      </View>
    );
  };

  render() {
    const { items } = this.props;
    return (
      <View style={styles.container}>
        <FlatList
          data={items}
          renderItem={this.renderRow.bind(this)}
          keyExtractor={(item, index) => index.toString()}
          ListFooterComponent={this.renderFooter.bind(this)}
          onEndReached={this.loadMore.bind(this)}
          onEndReachedThreshold={0.1}
          enableEmptySections={true}
          removeClippedSubviews={false} // fixed disappearing content if ListView used
          // fix for multiple call loadMore()
          onMomentumScrollBegin={() => {this.onEndReachedCalledDuringMomentum = false}}
          initialNumToRender={3}
          scrollEnabled={!this.state.loading}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this.handlePullToRefresh.bind(this)}
              tintColor={DEFAULT_COLOR}
              colors={[DEFAULT_COLOR]}
            />
          }
        />
      </View>
    );
  }
}

const isConnected = state => state.network.isConnected;
const getUserId = state => state.profile.id;

const propsMapping = state => {
  return {
    userId: getUserId(state),
    isConnected: isConnected(state),
  };
};

export default connect(propsMapping)(NewsList);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  footer: {
    paddingVertical: 20,
    borderColor: "#CED0CE"
  },
});
