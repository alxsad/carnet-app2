import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  ScrollView,
  TouchableOpacity,
  Alert,
  Share,
  Platform,
} from 'react-native';
import Sheet from '@yfuks/react-native-action-sheet';
import RNImmediatePhoneCall from 'react-native-immediate-phone-call';
import {Button} from 'react-native-elements';
import {Icon} from '../../../components/index';
import moment from 'moment';
import {connect} from 'react-redux';
import {News} from '../../../services';
import {DEFAULT_COLOR, NavStyles} from '../../../styles';
import {Helpers, PermissionsHelper}  from '../../../utils';
import {platformText} from '../../../conf';
import FastImage from 'react-native-fast-image';

class SingleNews extends Component {

  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    return {
      headerLeft:
          <TouchableOpacity  onPress={() => navigation.dispatch({ type: 'Navigation/BACK' })} >
            <Icon name="icon_back" style={NavStyles.navIcon} size={20} color={'white'}/>
          </TouchableOpacity>,
      headerRight: params.handleShare &&
          <TouchableOpacity onPress={params.handleShare}>
            <Icon name="icon_share" style={NavStyles.navIcon} size={25} color={'white'}  />
          </TouchableOpacity> ||
          <View/>,
      }
  };

  constructor(props, context) {
    super(props, context);
    this.state = {
      post: null,
    };
  };

  async componentDidMount() {
    const {newsId, isPartnerView} = this.props.navigation.state.params;
    const fetchPost = isPartnerView ? News.getPartnerNewsById: News.getGaiNewsById;
    const post = await fetchPost(newsId, this.props.accessToken);
    this.setState({ post });
    if(post.url){
      this.props.navigation.setParams({ handleShare: () => this.onActionShare(post) });
    }
  }

  onActionShare (post) {
    Share.share(
      {
        message: post.description,
        url: post.url,
        title: post.title
      })
  }

  renderTags(tags) {
    return (
      <View style={styles.tagsContainer}>
        {tags.map((tag, i) => <Text style={styles.tag} key={i}>{'#' + tag}</Text>)}
      </View>
    );
  }

  renderPhoneButton() {
    return (
      <View style={styles.buttonContainer}>
        <View style={styles.sendButton}>
          <Button
            Component={TouchableOpacity}
            icon={{name: "icon_bell", size: 25, color: 'white', style: {padding: 5}}}
            iconComponent={Icon}
            title="Мне интересно"
            backgroundColor={DEFAULT_COLOR}
            onPress={this.showActionSheet.bind(this)}
          />
        </View>
      </View>
    );
  }

  showActionSheet() {
    const { userId, accessToken } = this.props;
    const { post } = this.state;
    const phoneCallback = async () =>  {
      const permissionAllowed = await PermissionsHelper.requestCallPermission();
      if (permissionAllowed){
        RNImmediatePhoneCall.immediatePhoneCall(post.phone);
      }
    };
    const callMeCallback = async () => {
      if (!this.props.isConnected) {
        return Helpers.showNoConnectionMessage();
      }
      const result = await News.callMeBack(userId, post.id, accessToken);
      if (!result) {
        Helpers.showGenericError();
      } else {
        Alert.alert(null, 'Представитель компании свяжется с Вами в ближайшее время.');
      }
    };
    const actionSheetCallbacks = [ phoneCallback, callMeCallback ];
    const actionSheetIOS = {
      options: [
        'Позвонить',
        'Перезвонить мне',
        platformText.Cancel,
      ],
      cancelButtonIndex: 2,
      tintColor: '#007ff9'
    };
    const actionSheetAndroid = {
      options: [
        'Позвонить',
        'Перезвонить мне',
      ],
    };
    const actionSheet = (Platform.OS === 'ios') ? actionSheetIOS : actionSheetAndroid;

    Sheet.showActionSheetWithOptions(actionSheet, buttonIndex => {
      if (buttonIndex === 0 || buttonIndex === 1){
        actionSheetCallbacks[buttonIndex]();
      }
    });
  };

  render() {
    const { post } = this.state;
    if (!post) {
      return <View/>;
    }
    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.imageContainer}>
            <FastImage
              style={styles.topImage}
              source={{
                uri: post.image,
                priority: FastImage.priority.low,
              }}
              resizeMode={FastImage.resizeMode.cover}
            />
          </View>
          <View style={styles.newsContainer}>
            <Text style={styles.date}>{moment(post.date).format('L') }</Text>
            <Text style={styles.title}>{post.title}</Text>
            <View>
              <Text style={styles.description}>{post.text}</Text>
            </View>
            {post.tags && this.renderTags(post.tags)}
          </View>
        </ScrollView>
        {post.show_button && this.renderPhoneButton()}
      </View>
    );
  }
}

const getAccessToken = state => state.auth.accessToken;
const getUserId = state => state.profile.id;
const isConnected = state => state.network.isConnected;

const propsMapping = state => {
  return {
    accessToken: getAccessToken(state),
    userId: getUserId(state),
    isConnected: isConnected(state),
  };
};

export default connect(propsMapping)(SingleNews);

const { width, height } = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f2f2',
  },
  topImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    height: height * 0.4 ,
    width: width,
  },
  newsContainer: {
    flex: 1,
    marginTop: width * 0.65,
    margin: 8,
    backgroundColor: '#fff',
    padding: 10,
    shadowColor: '#a6a6a6',
    shadowOffset: { width: 2, height: 2, },
    shadowOpacity: 0.5,
    shadowRadius: 3,
  },
  date: {
    color: '#b0b0b0',
    //alignSelf: 'flex-start',
    marginTop: 5,
    marginBottom: 5,
  },
  title: {
    fontSize: 20,
    lineHeight: 22,
    fontWeight: 'bold',
    paddingTop: 5,
    marginBottom: 15,
  },
  description: {
    fontSize: 16,
    lineHeight: 22,
    // ...fonts.fontPrimaryLight,
  },
  tagsContainer: {
    alignItems: 'flex-start',
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 5,
    marginBottom: 3,
  },
  tag: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    borderColor: '#CDE6FF',
    borderWidth: 1,
    padding: 2,
    marginRight: 4,
    marginBottom: 2,
    color: DEFAULT_COLOR,
    fontSize: 10,
  },
  buttonContainer: {
    //flex: 1,
    bottom: 0,
    backgroundColor: DEFAULT_COLOR,
  },
  sendButton: {
    //flex: 1,
    flexDirection:'column',
    justifyContent:'center',
    alignItems:'stretch',
  },
});
