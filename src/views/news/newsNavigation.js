'use strict';

// React
import React, {Component} from 'react'
import {
  View,
} from 'react-native';

// Navigation
import { addNavigationHelpers } from 'react-navigation'
import { NewsNavigation } from './screens/navigationConfiguration'

// Redux
import { connect } from 'react-redux'

const mapStateToProps = (state) => {
  return {
    navigationState: state.newsTab
  }
};
class TabNewsNavigation extends Component {

  render(){
    const { navigationState, dispatch } = this.props;
    return (
      <View style={{ flex: 1 }}>
        <NewsNavigation
          navigation={
            addNavigationHelpers({
              dispatch: dispatch,
              state: navigationState
            })
          }
        />
      </View>
    )
  }
}
export default connect(mapStateToProps)(TabNewsNavigation)
