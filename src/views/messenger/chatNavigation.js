'use strict';

// React
import React, {Component} from 'react'

// Navigation
import { addNavigationHelpers } from 'react-navigation'
import { ChatNavigation } from './screens/navigationConfiguration'

// Redux
import { connect } from 'react-redux'

const mapStateToProps = (state) => {
  return {
    navigationState: state.chatTab
  }
};
class TabChatNavigation extends Component {

  render(){
    const { navigationState, dispatch } = this.props;
    return (
      <ChatNavigation
        navigation={
          addNavigationHelpers({
            dispatch: dispatch,
            state: navigationState
          })
        }
      />
    )
  }
}
export default connect(mapStateToProps)(TabChatNavigation)
