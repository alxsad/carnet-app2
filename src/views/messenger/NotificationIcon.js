import React, {Component} from 'react'
import {View} from 'react-native';
import {connect} from 'react-redux';
import {tabBarStyles} from '../../styles'

import {Icon} from '../../components/index';

class NotificationIcon extends Component {

  render() {
    const {showChatBadge, tintColor} = this.props;
    return (
      <View style={{
        justifyContent: 'center',
        marginBottom: 2,
      }}>
        <Icon style={tabBarStyles.iconStyle} name="menu_3_1" size={30} color={tintColor} />
        {showChatBadge ?
          <View style={tabBarStyles.badgeIcon}>
            <Icon size={8} name="icon_message_unread" color="#FF0405" />
          </View>
          : undefined}
      </View>
    );
  }
}
const showChatBadge = chats => {
  let result = false;
  Object.keys(chats).forEach(k => {
    let chat = chats[k];
    if (chat.has_unread_messages) {
      result = true;
    }
  });
  return result;
};
  const mapStateToProps = state => {
  return {
    showChatBadge: showChatBadge(state.messenger)

  }
};
export default connect(mapStateToProps)(NotificationIcon)

