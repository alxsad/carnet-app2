import React, {Component} from 'react';
import {TouchableOpacity, View, Clipboard, Image, StyleSheet, Platform} from 'react-native';
import {GiftedChat, Bubble, Send, Time} from 'react-native-gifted-chat';
import {sendMessage, removeChatBadge, deleteMessage, resendMessage} from '../../../app/actions/messenger';
import {connect} from 'react-redux';
import {Icon} from '../../../components/index';
import {NavStyles, DEFAULT_COLOR} from '../../../styles';
import Helpers from '../../../utils/helpers';
import {platformText} from '../../../conf';
import FastImage from 'react-native-fast-image';

class Chat extends Component {

  static navigationOptions = ({navigation}) => {
    const {params = {}} = navigation.state;
    return {
      tabBarVisible: false,
      title: params.title,
      headerLeft:
        <TouchableOpacity onPress={() => navigation.dispatch({type: 'Navigation/BACK'})}>
          <Icon name="icon_back" style={NavStyles.navIcon} size={20} color={'white'}/>
        </TouchableOpacity>,
      headerRight:
        <TouchableOpacity onPress={() => params.showProfile && params.showProfile()}>
          <View style={NavStyles.navAvatarRadius}>
            <FastImage
                style={NavStyles.navAvatar}
                source={{uri: params.companion.userAvatar}}
            />
          </View>
        </TouchableOpacity>
    }
  };

  componentDidMount(){
    this.props.navigation.setParams({showProfile: () => this.pressAvatar()});
  }
  componentDidUpdate() {
    this.props.removeChatBadge(this.props.navigation.state.params.chatId);
  }

  onSend(messages) {
    if (!this.props.isConnected) {
      return Helpers.showNoConnectionMessage();
    }
    const chatId = this.props.navigation.state.params.chatId;
    this.props.sendMessage(chatId, messages[0], Helpers.showGenericError);
  }

  renderBubble(props) {
    return (
      <Bubble
        {...props}
        textStyle={{
          right: {
            color: 'black',
          }
        }}
        wrapperStyle={{
          right: {
            backgroundColor: '#e3f8d1',
          },
          left: {
            backgroundColor: '#DBE1E7',
          }
        }}
      />
    );
  }

  renderTicks(currentMessage) {
    if (typeof(currentMessage.sent) !== "undefined") {
      return (
        <View>
          <Icon name="icon_checkmark_small" style={{marginRight: 10}} size={10}
            color={currentMessage.sent ? DEFAULT_COLOR : '#f97d76'}/>
        </View>
      )
    }
  }

  onLongPress(context, message){
    const chatId = this.props.navigation.state.params.chatId;
    const resend = typeof(message.sent) !== "undefined" && !message.sent;
    const options = [
          'Скопировать',
          'Удалить у меня',
          platformText.Cancel,
    ];
    if (resend){
      options.splice(2, 0, 'Отправить повторно')
    }
    const cancelButtonIndex = options.length - 1;
    context.actionSheet().showActionSheetWithOptions({
        options,
        cancelButtonIndex,
      },
      (buttonIndex) => {
        switch (buttonIndex) {
          case 0:
            Clipboard.setString(message.text);
            break;
          case 1:
            this.props.deleteMessage(chatId, message);
            break;
          case 2:
            resend && this.props.resendMessage(chatId, message, Helpers.showGenericError);
            break;
        }
      });
  }

  renderSend(props) {
    return (
      <Send
        label="Отправить"
        {...props}
      >
      </Send>
    );
  }
  renderTime(props) {
    const {...timeProps} = props;
    return <Time
      {...timeProps}
      textStyle={{
        right: {
          color: '#aaa',
        }
      }}
    />;
  }
  pressAvatar() {
    this.props.navigation.navigate('ChatProfile', {
       user: this.props.navigation.state.params.companion,
    });
  }

  // renderAvatar(props) {
  //   return(
  //   <TouchableOpacity onPress={() => this.pressAvatar(props.currentMessage.user)}>
  //     <FastImage
  //       style={styles.avatar}
  //       source={{
  //         uri: props.currentMessage.user.avatar,
  //         priority: FastImage.priority.normal,
  //       }}
  //     />
  //   </TouchableOpacity>
  //   )}

  render() {
    return (
      <GiftedChat
        messages={this.props.messages}
        onSend={this.onSend.bind(this)}
        user={this.props.user}
        isAnimated={true}
        placeholder={'Напишите сообщение...'}
        locale={'ru'}
        renderBubble={this.renderBubble}
        renderTicks={this.renderTicks}
        onLongPress={this.onLongPress.bind(this)}
        renderSend={this.renderSend}
        renderTime={this.renderTime}
        // onPressAvatar={this.pressAvatar.bind(this)}
        renderAvatar={null}
      />
    );
  }
}

const getChat = (state, id) => state.messenger[id];
const getChatMessages = (state, id) => getChat(state, id).messages || [];
const getUser = state => {return {_id: state.profile.id, name: state.profile.name}};
const isConnected = state => state.network.isConnected;

const propsMapping = (state, { navigation: { state: { params: { chatId } } } }) => {
  return {
    messages: getChatMessages(state, chatId),
    user: getUser(state),
    isConnected: isConnected(state),
  };
};

const actionMapping = dispatch => {
  return {
    sendMessage: (chatId, message, fail) => dispatch(sendMessage(chatId, message, fail)),
    deleteMessage: (chatId, message) => dispatch(deleteMessage(chatId, message)),
    resendMessage: (chatId, message, fail) => dispatch(resendMessage(chatId, message, fail)),
    removeChatBadge: chatId => dispatch(removeChatBadge(chatId)),
  }
};

export default connect(propsMapping, actionMapping)(Chat);
