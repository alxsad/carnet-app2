'use strict';
import React from 'react';
import { StackNavigator } from 'react-navigation'
import {NavStyles} from '../../../styles';
import NotificationIcon from '../NotificationIcon'
// screens
import ChatList from './ChatList'
import Chat from './Chat'
import ChatProfile from './ChatProfile'

const routeConfiguration = {
  ChatList: {
    screen: ChatList,
  },
  Chat: {
    screen: Chat,
  },
  ChatProfile: {
    screen: ChatProfile,
  },
};

const stackNavigatorConfiguration = {
  navigationOptions: ({ screenProps }) => ({
    tabBarIcon: ({ tintColor }) => <NotificationIcon tintColor={tintColor}/>,
    headerStyle: NavStyles.headerNavStyle,
    title: 'Общение',
    headerTintColor: '#ffffff',
    headerTitleStyle: {alignSelf: 'center'},
  }),
  initialRouteName: 'ChatList',
};

export const ChatNavigation = StackNavigator(routeConfiguration,stackNavigatorConfiguration);
