import React, {Component} from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Dimensions,
  ScrollView} from 'react-native';
import {NavStyles} from '../../../styles';
import {Profile} from '../../../services'
import {connect} from 'react-redux';
import {Icon, CarNumberPlate, Tag} from '../../../components';
import {normalize} from '../../../conf';
import FastImage from 'react-native-fast-image';

class ChatProfile extends Component {
  static navigationOptions = ({navigation}) => {
    const {params = {}} = navigation.state;
    return {
      tabBarVisible: false,
      title: 'Профиль',
      headerLeft:
        <TouchableOpacity onPress={() => navigation.dispatch({type: 'Navigation/BACK'})}>
          <Icon name="icon_back" style={NavStyles.navIcon} size={20} color={'white'}/>
        </TouchableOpacity>,
      headerRight: <View/>,
    }
  };

  constructor(props, context){
    super(props, context);
    this.state = {};
  };

  async componentDidMount(){
    const {userId, carNumber} = this.props.navigation.state.params.user;
    let profile = null;
    if(carNumber){
      profile = await Profile.getPublicProfile(carNumber, this.props.accessToken);
    }
    else{
      profile = await Profile.getPublicProfileByUserId(userId, this.props.accessToken);
    }
    profile && this.setState({profile: profile});
  }

  render() {
    const profile = this.state.profile;
    if(profile){
      return (
          <ScrollView
            style={styles.container}
            removeClippedSubviews={false}//fixed disappearing content
          >
            <View style={styles.avatarContainer}>
              <FastImage
                style={styles.avatar}
                source={{
                  uri: this.props.navigation.state.params.user.userAvatar,
                  priority: FastImage.priority.normal,
                }}
                resizeMode={FastImage.resizeMode.cover}
              />
            </View>
            <View style={styles.nameContainer}>
              <Text style={styles.nameText}>{profile.name && '∼' + profile.name || 'Водитель'}</Text>
            </View>
              {profile.car &&
                <View style={styles.carNumberContainer}>
                  <Text style={styles.text}>{profile.car.brand} {profile.car.model}</Text>
                  <CarNumberPlate style={styles.carNumberPlate} carNumber={profile.car.number}/>
                </View>
              }
              {/*{profile.interests.length > 0 &&*/}
              {/*  <View style={styles.interestsContainer}>*/}
              {/*    <Text style={styles.text}>Интересы</Text>*/}
              {/*    <View style={styles.tags}>*/}
              {/*      {profile.interests && profile.interests.map((interest, i) =>*/}
              {/*        <Tag*/}
              {/*          styles={{marginRight: 5,marginBottom: 5}}*/}
              {/*          key={i}*/}
              {/*        >*/}
              {/*          <Text style={{fontSize: normalize(16)}}>{interest}</Text>*/}
              {/*        </Tag>*/}
              {/*      )}*/}
              {/*    </View>*/}

              {/*  </View>*/}
              {/*}*/}
          </ScrollView>
      );
    }
    return <View></View>

  }
}

const propsMapping = state => {
  return {
    accessToken: state.auth.accessToken,
    isConnected: state.network.isConnected,
  };
};

export default connect(propsMapping, null)(ChatProfile);


const { height, width } = Dimensions.get('window');
const avatarContainerHeight = height * 0.4;
const styles = StyleSheet.create({
  container: {
  },
  avatarContainer: {
    flexDirection:'column',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  avatar: {
    width: width,
    height: avatarContainerHeight,
  },
  nameContainer: {
    backgroundColor: '#ffffff',
    marginBottom: 20,
    paddingVertical: 10,
    paddingLeft: 10,
  },
  nameText: {
    color: '#7C8082',
    fontSize: normalize(17),
    fontWeight: 'bold',
    marginVertical: 10,
  },
  carNumberContainer: {
    backgroundColor: '#ffffff',
    marginBottom: 20,
    paddingVertical: 20,
    paddingLeft: 10,
  },
  text: {
    color: '#7C8082',
    fontSize: normalize(15),
    fontWeight: 'bold',
  },
  carNumberPlate: {
    justifyContent: 'flex-start',
    alignSelf: 'flex-start',
    marginTop: 10,
  },
  interestsContainer: {
    backgroundColor: '#ffffff',
    marginBottom: 20,
    paddingVertical: 10,
    paddingLeft: 10,
  },
  tags:{
    alignItems: 'flex-start',
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 10,
  }
});
