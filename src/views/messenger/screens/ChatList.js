import React, {Component} from 'react';
import {StyleSheet, Text, View, ListView, TouchableOpacity, TouchableHighlight, Platform, Image} from 'react-native';
import {connect} from 'react-redux';
import {SwipeListView} from 'react-native-swipe-list-view';
import {Icon} from '../../../components/index';
import {deleteChat, receiveUnreadMessages, removeChatBadge} from '../../../app/actions/messenger';
import moment from 'moment';
import {NavStyles} from '../../../styles';
import FastImage from 'react-native-fast-image';
import {normalize} from '../../../conf';

class Messenger extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {showActivity: false} } = navigation.state;
    return {
      headerLeft: <View/>,
      headerRight:
        <TouchableOpacity onPress={() => navigation.navigate('FindCarModal', {
          shouldEnableGestures: false,
          cb: (title, chatId) =>  this.props.navigation.navigate('Chat', {
            title: title,
            chatId: chatId
          })
        })} >
          <Icon name="icon_add_white" style={NavStyles.navIcon} size={25} color={'white'}/>
        </TouchableOpacity>
    }
  };

  openChat(chat) {
    this.props.removeChatBadge(chat.chat_id);
    this.props.navigation.navigate('Chat', {
      title: chat.title,
      chatId: chat.chat_id,
      companion: {
        userId: chat.user_id,
        userAvatar: chat.user_avatar,
        carNumber: chat.car_number
      }
    });
  }

  deleteChat(chat, secId, rowId, rowMap) {
    rowMap[`${secId}${rowId}`].closeRow();
    this.props.deleteChat(chat.chat_id);
  }

  renderChat(chat, sectionID, rowID) {
    if(Platform.OS === 'android'){
      //preload avatars for later usage
      FastImage.preload([
        {
          uri: chat.user_avatar,
        }
      ]);
    }

    return (
      <TouchableHighlight onPress={() => this.openChat(chat)}>
        <View style={styles.container }>
          <View style={[styles.avatarContainer, this.props.chats.indexOf(chat) === this.props.chats.length-1
            && {borderBottomWidth: 0.5, borderBottomColor: '#B2B2B2'}]}>
                <View style={styles.avatarRadius}>
                  <FastImage
                    style={styles.avatar}
                    source={{
                      uri: chat.user_avatar,
                      priority: FastImage.priority.normal,
                    }}
                  />
                </View>
          </View>
          <View style={styles.chatContainer}>
            <Text style={styles.userLabel}>{chat.title}</Text>
            <Text numberOfLines={2} style={styles.lastMessage}>{chat.last_message_text}</Text>
          </View>
          <View style={styles.dateTime}>
            {chat.has_unread_messages && <Icon size={10} name="icon_message_unread" color="#FF0405" />}
            <Text style={styles.dateTimeText}> {moment(chat.last_message_date).format('HH:mm')}</Text>
          </View>
        </View>
      </TouchableHighlight>
    );
  }
  renderHiddenRow(chat, secId, rowId, rowMap) {
    return (
      <View style={styles.swipeChat}>
        <TouchableOpacity style={styles.swipeButton}
                          onPress={_ => this.deleteChat(chat, secId, rowId, rowMap) }>
          <Icon name="icon_bin" size={30} color={'white'}/>
        </TouchableOpacity>
      </View>
    );
  }
  render() {
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    if (this.props.chats.length) {
      return (
        <SwipeListView
          dataSource={ds.cloneWithRows(this.props.chats)}
          renderRow={this.renderChat.bind(this)}
          renderHiddenRow={this.renderHiddenRow.bind(this)}
          rightOpenValue={-70}
          disableRightSwipe={true}
          enableEmptySections={true}
          removeClippedSubviews={false}//fixed disappearing content
        />
      );
    }
    else {
      return <View style={styles.noChats}>
        <Text style={styles.noChatsText}>Нажмите на ＋ для поиска авто по регистрационному номеру</Text>
      </View>;
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'white',
  },
  avatarContainer: {
    padding: 10,
  },
  noChats: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  noChatsText: {
    textAlign: 'center',
    fontSize: normalize(16),
    color: '#9B9B9B',
  },
  avatarRadius:{
    borderRadius: 25,
    width: 50,
    height: 50,
    overflow: 'hidden',
  },
  avatar: {
    flex: 1,
  },
  chatContainer: {
    flex: 5,
    // paddingLeft: 10,
    paddingTop: 13,
    paddingBottom: 0,
    flexDirection: 'column',
    borderBottomWidth: 0.5,
    borderBottomColor: '#B2B2B2',
  },
  dateTime: {
    flex: 1.5,
    paddingTop: 10,
    paddingRight: 10,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    borderBottomWidth: 0.5,
    borderBottomColor: '#B2B2B2',
  },
  dateTimeText: {
    fontSize: 12,
    color: '#9B9B9B',
  },
  userLabel: {
    fontWeight: 'bold',
    paddingBottom: 3,
  },
  lastMessage: {
    fontSize: 12,
    color: '#9B9B9B',
  },
  swipeChat: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 0.5,
    borderBottomColor: '#B2B2B2',
  },
  swipeButton: {
    alignItems: 'center',
    bottom: 0,
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    width: 70,
    backgroundColor: '#FF5B5B',
    right: 0
  },
  androidFixCircleClipping: {
    position: 'absolute',
    top: 0,
    bottom: 0, right: 0,
    left: 0, borderRadius: 35,
    borderWidth: 10, borderColor: 'white'
  },
});

const getChats = state => {
  let chats = [];
  Object.keys(state).forEach(k => {
    chats.push(state[k]);
  });
  return chats.sort((a, b) => {
    return moment(b.last_message_date).unix() - moment(a.last_message_date).unix();
  });
};

const propsMapping = state => {
  return {
    chats: getChats(state.messenger),
  };
};

const actionMapping = dispatch => {
  return {
    deleteChat: chatId => dispatch(deleteChat(chatId)),
    receiveUnreadMessages: () => dispatch(receiveUnreadMessages()),
    removeChatBadge: chatId => dispatch(removeChatBadge(chatId)),
  }
};

export default connect(propsMapping, actionMapping)(Messenger);
