import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Platform,
  Alert,
  Image,
  ActivityIndicator,
} from 'react-native';
import {connect} from 'react-redux';
import {TextInputMask} from 'react-native-masked-text';
import LoadSpinner from '../../../components/ActivityIndicator';
import CountDownTimer from '../../../components/countDownTimer/CountDownTimer';
import { verifyOTP, resendOTP } from '../../../app/actions/auth';
import { normalize } from '../../../conf';
import Helpers from '../../../utils/helpers';

class AuthOTP extends Component {

  static navigationOptions = ({ navigation }) => {
    return {
      header: null,
    }
  };

  constructor(props, context){
    super(props, context);
    this.state = {
      code: '',
      isLoading: false,
      showResendLoader: false,
      isResendTimerShown: false,
    };
  };

  componentDidMount() {
    this.mounted = true;
    const { OTPNextAvailableDate } = this.props;
    OTPNextAvailableDate && new Date(parseInt(OTPNextAvailableDate)) > Date.now() && this.showTimer();
    this.codeInput.getElement().focus();
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  changeCodeNumber(code) {
    this.setState({ code });
    if (code && code.length >= 6) {
      setTimeout(() => {
        this.sendOTP();
      }, 1000);
    }
  };

  enableProcessing() {
    this.setState({ isLoading: true });
  }

  disableProcessing() {
    this.setState({ isLoading: false });
  }

  showResendLoader(){
    this.setState({ showResendLoader: true });
  }

  hideResendLoader(){
    this.setState({ showResendLoader: false });
  }

  showTimer() {
    this.setState({ isResendTimerShown: true });
  };

  hideTimer() {
    this.setState({ isResendTimerShown: false });
  };

  async sendOTP() {
    if (!this.props.isConnected) {
      return Helpers.showNoConnectionMessage();
    }
    const params = {
      code: Helpers.getNumeric(this.state.code),
      platform: Platform.OS === 'ios' ? 'ios': 'android',
    };
    this.enableProcessing();
    await this.props.verifyOTP(params, this.onError);
    this.mounted && this.disableProcessing();
  };

  async resendCode() {
    if (!this.props.isConnected) {
      return Helpers.showNoConnectionMessage();
    }
    this.showResendLoader();
    await this.props.resendOTP(this.onResendCode.bind(this), this.onError);
    this.hideResendLoader();
  };

  onResendCode(message) {
    this.showTimer();
    Alert.alert(null, message);
  }

  onError(message) {
    if (message) {
      Alert.alert(null, message);
    } else {
      Helpers.showGenericError();
    }
  }

  renderTimer() {
    const { OTPNextAvailableDate } = this.props;
    return (
      <CountDownTimer
        date={OTPNextAvailableDate && new Date(parseInt(OTPNextAvailableDate))}
        days={{plural: 'д ', singular: 'д '}}
        hours=':'
        mins=':'
        segs=''
        onEnd={this.hideTimer.bind(this)}
        key={OTPNextAvailableDate}
        daysStyle={styles.timerText}
        hoursStyle={styles.timerText}
        minsStyle={styles.timerText}
        secsStyle={styles.timerText}
        firstColonStyle={styles.timerText}
        secondColonStyle={styles.timerText}
      />
    );
  }

  renderInput() {
    return (
      <TextInputMask
        ref={ref => { this.codeInput = ref }}
        style={styles.codeText}
        placeholder="• • • • • •"
        value={this.state.code}
        maxLength={6}
        onChangeText={this.changeCodeNumber.bind(this)}
        type={'custom'}
        options={{mask: '999999'}}
        keyboardType={(Platform.OS === 'ios') ? 'number-pad' : 'numeric'}
        underlineColorAndroid="transparent"
        placeholderTextColor="#B4DAF5"
      />
    );
  }

  renderMainView() {
    return (
      <View style={styles.container}>
        <View style={styles.bckgrndContainer}>
          <Image
            style={{flex: 1, resizeMode: 'cover'}}
            source={require('../assets/auth_bckgrnd.jpg')}
            blurRadius={3}
          />
        </View>
          <View style={styles.header}>
            <Text style={styles.headerText}>Регистрация</Text>
          </View>
          <View style={styles.description}>
            <Text style={styles.text}>Мы выслали код подтверждения</Text>
            <Text style={styles.text}>на Ваш мобильный телефон</Text>
          </View>

        <View style={styles.codeContainer}>
          {this.renderInput()}
        </View>
        <View style={styles.buttonsContainer}>
          {this.state.showResendLoader &&
          <ActivityIndicator animating color="white" size="small"/> ||
          <View style={styles.resendCodeContainer}>
            <TouchableOpacity disabled={this.state.isResendTimerShown} onPress={this.resendCode.bind(this)}>
              <Text style={styles.buttonText}>Отправить код повторно</Text>
            </TouchableOpacity>
            {this.state.isResendTimerShown && this.renderTimer()}
          </View>
          }
          <TouchableOpacity onPress={() => this.props.navigation.dispatch({ type: 'Navigation/BACK' })}>
            <Text style={styles.buttonText}>Изменить номер</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.footer}></View>
      </View>
    )
  };

  render() {
    if (this.state.isLoading) {
      return <LoadSpinner animating={true} description="обработка"/>;
    } else {
      return this.renderMainView();
    }
  }
}

const actionMapping = dispatch => {
  return {
    verifyOTP: (params, fail) => dispatch(verifyOTP(params, fail)),
    resendOTP: (success, fail) => dispatch(resendOTP(success, fail)),
  }
};
const propsMapping = state => {
  return {
    OTPNextAvailableDate: state.auth.OTPNextAvailableDate,
    phone: state.auth.phoneNumber,
    uuid: state.auth.uuid,
    isConnected: state.network.isConnected,
  }
};

export default connect(propsMapping, actionMapping)(AuthOTP);

let footerFlex = 5;
if(Platform.OS === 'android'){
  footerFlex = 4;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'center',
    padding: 20,
  },
  bckgrndContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    flex: 1,
    // width: '100%',
    // height: '100%',
  },
  header: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  description: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerText: {
    color: '#FFFFFF',
    backgroundColor: 'transparent',
    textAlign: 'center',
    fontSize: normalize(22),
  },
  text: {
    color: '#B4DAF5',
    backgroundColor: 'transparent',
    textAlign: 'center',
    fontSize: normalize(17),
  },
  codeContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    borderColor: '#B4DAF5',
    borderBottomWidth: 0.5,
    marginBottom: 20,
  },
  codeText: {
    flex: 1,
    color: '#eeeeee',
    fontSize: normalize(40),
    textAlign: 'center',
    paddingVertical: 0,
  },
  buttonsContainer: {
    flex: 2,
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'stretch',
  },
  buttonText: {
    fontSize: normalize(15),
    color: '#B4DAF5',
    backgroundColor: 'transparent',
    textAlign: 'center',
  },
  resendCodeContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  // backButtonTitle: {
  //   fontSize: 15,
  //   position: 'relative',
  //   left: -(Dimensions.get('window').width * 0.03),
  // },
  timerText: {
    fontSize: normalize(15),
    color: '#B4DAF5',
    backgroundColor: 'transparent',
  },
  footer: {
    flex: footerFlex,
  }
});
