import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Platform,
  Alert,
  Image,
  Linking,
} from 'react-native';
import {TextInputMask} from 'react-native-masked-text';
import {connect} from 'react-redux';
import {Button} from 'react-native-elements';
import {Icon} from '../../../components/index';
import {sendPhone} from '../../../app/actions/auth';
import {DEFAULT_COLOR} from '../../../styles/app';
import {normalize} from '../../../conf';
import Helpers, {BackHandler} from '../../../utils/helpers';
import SplashScreen from 'react-native-splash-screen';

const countryCode = "375";
const phoneLength = 9;

class AuthPhone extends Component {

  static navigationOptions = ({ navigation }) => {
    // const { params = {} } = navigation.state;
    // const { hideHeader } = params;
    // const header = { title: 'Регистрация в Carnet' };
    // return hideHeader ? { header: null } : header;
    return {
      header: null,
    }
  };

  constructor(props, context) {
    super(props, context);
    this.state = {
     phone: '',
     isLoading: false,
     isButtonDisabled: true
    };
  };

  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }

  componentDidMount() {
    const { phoneNumber } = this.props;
    if (phoneNumber) {
      this.setState({ phone: phoneNumber, isButtonDisabled: false });
    }
    if (this.props.OTPWasSent) {
      this.navigateToOtpScreen();
    }
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.dispatch({ type: 'Navigation/BACK' });
    return true;
  };

  enableProcessing() {
    this.props.navigation.setParams({ hideHeader: true });
    this.setState({ isLoading: true });
  };

  disableProcessing() {
    this.props.navigation.setParams({ hideHeader: false });
    this.setState({ isLoading: false });
  };

  navigateToOtpScreen() {
    this.props.navigation.navigate('AuthOTP', { phoneTitle: `+${countryCode} ${this.props.phoneNumber}` });
  };

  async onSendPhoneNumber() {
    const {phoneNumber, OTPWasSent} = this.props;
    if (phoneNumber) {
      if (phoneNumber === this.state.phone && OTPWasSent) {
        return this.navigateToOtpScreen();
      }
    }
    if (!this.props.isConnected) {
      return Helpers.showNoConnectionMessage();
    }
    this.enableProcessing();
    await this.props.sendPhone(
      { countryCode, phoneNumber: this.state.phone },
      this.navigateToOtpScreen.bind(this),
      this.onError
    );
    this.disableProcessing();
  };

  onError(message) {
    if (message) {
      Alert.alert(null, message);
    } else {
      Helpers.showGenericError();
    }
  }

  changePhoneNumber(phone) {
    this.setState({ phone: phone, isButtonDisabled: phone.length < phoneLength });
  };
// for some android background image finish loading after component render, so it cause blinking
// seems it working like that on old android, so hide splash after background loaded
//for other fast phones, which load background immediately, just hide splash after 1 sec delay
  backgroundLoaded(){
    if(Platform.OS === 'android' && Platform.Version < 24){
      SplashScreen.hide();
    }
    else{
      setTimeout(function(){
        SplashScreen.hide();
      }, 1000);
    }
  }

  renderMainView() {
    return (
      <View style={styles.container}>
        <View style={styles.bckgrndContainer}>
            <Image
              onLoadEnd={this.backgroundLoaded}
              style={{flex: 1, resizeMode: 'cover'}}
              source={require('../assets/auth_bckgrnd.jpg')}
              blurRadius={3}
            />
        </View>
        <View style={styles.header}>
          <Text style={styles.headerText}>Регистрация</Text>
        </View>
        <View style={styles.description}>
          <Text style={styles.text}>Добро пожаловать в Carnet.</Text>
          <Text style={styles.text}>Укажите Ваш номер мобильного</Text>
          <Text style={styles.text}>телефона и нажмите «Продолжить»</Text>
        </View>
        <View style={styles.phoneContainer}>
          <Icon style={styles.phoneIcon} size={26} name="icon_phone" color="#B4DAF5" />
          <Text style={styles.countryCode}>+{countryCode} </Text>
          <TextInputMask
            style={styles.phoneText}
            placeholder="Номер телефона"
            value={this.state.phone}
            maxLength={phoneLength}
            onChangeText={this.changePhoneNumber.bind(this)}
            type={'custom'}
            options={{mask: '999999999'}}
            keyboardType={Platform.OS === 'ios' ? 'number-pad' : 'numeric'}
            underlineColorAndroid="transparent"
            placeholderTextColor="#FFFFFF"
          />
        </View>
        <View style={styles.buttonContainer}>
          <Button
            large
            loading={this.state.isLoading}
            disabled={this.state.isButtonDisabled || this.state.isLoading}
            title='Продолжить'
            color={DEFAULT_COLOR}
            backgroundColor='#FFFFFF'
            onPress={this.onSendPhoneNumber.bind(this)}
            fontSize={normalize(17)}
          />
        </View>
        <View style={styles.footer}>
          <Text style={styles.policyText}>Регистрируясь в Carnet вы соглашаетесь с
          </Text>
          <Text
            style={[styles.policyText, {color: '#B4DAF5',textDecorationLine: 'underline'}]}
            onPress={() => {Linking.openURL('https://carnet.davidovi.ch/terms.html')}}
          >
            условиями предоставления услуг и политикой конфиденциальности
          </Text>
        </View>
      </View>
    );
  }

  render() {
      return this.renderMainView();
  }
}

const propsMapping = state => {
  return {
    phoneNumber: state.auth.phoneNumber,
    OTPWasSent: state.auth.OTPWasSent,
    isConnected: state.network.isConnected,
  }
};

const actionMapping = dispatch => {
  return {
    sendPhone: (fullPhone, success, fail) => dispatch(sendPhone(fullPhone, success, fail)),
  }
};

export default connect(propsMapping, actionMapping)(AuthPhone);

let footerFlex = 3;
if(Platform.OS === 'android'){
    footerFlex = 4;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'center',
    padding: 20,
  },
  bckgrndContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    flex: 1,
    // width: '100%',
    // height: '100%',
  },
  header: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  description: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerText: {
    color: '#FFFFFF',
    backgroundColor: 'transparent',
    textAlign: 'center',
    fontSize: normalize(22),
  },
  text: {
    color: '#B4DAF5',
    backgroundColor: 'transparent',
    textAlign: 'center',
    fontSize: normalize(17),
  },
  phoneContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    borderColor: '#B4DAF5',
    borderBottomWidth: 1,
  },
  phoneIcon: {
    backgroundColor: 'transparent',
    marginRight: 5,
  },
  countryCode: {
    color: '#B4DAF5',
    backgroundColor: 'transparent',
    marginRight: 5,
    fontSize: normalize(25),
    alignItems: 'center',
  },
  phoneText: {
    color: '#eeeeee',
    flex: 1,
    fontSize: normalize(25),
    paddingVertical: 0,
  },
  buttonContainer: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  footer: {
    flex: footerFlex,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  policyText: {
    color: '#eeeeee',
    backgroundColor: 'transparent',
    textAlign: 'center',
    fontSize: normalize(14),
  }
});
