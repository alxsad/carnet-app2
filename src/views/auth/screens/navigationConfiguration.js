'use strict';
import React from 'react';

import { StackNavigator } from 'react-navigation';
// screens
import AuthPhone from './AuthPhone'
import AuthOTP from './AuthOTP'

const routeConfiguration = {
  AuthPhone: {
    screen: AuthPhone,
  },
  AuthOTP: {
    screen: AuthOTP,
  },
};

const stackNavigatorConfiguration = {
  navigationOptions: ({ screenProps }) => ({
    headerMode: 'screen'
    //header: null,
    //headerStyle: styles.headerNavStyle,
    // title: 'Ваш номер телефона',
    //headerTintColor: '#ffffff',
  }),
  initialRouteName: 'AuthPhone',
};

export const AuthNavigation = StackNavigator(routeConfiguration, stackNavigatorConfiguration);
