import Api from '../utils/api'
import {Media} from "./index";

const Map = {
  async getMarkers(accessToken) {
    try {
      const response = await Api.get(`/markers`, accessToken);
      if (!response.ok) {
        return false;
      }
      return await response.json();
    } catch (e) {
      console.log(e);
      return false;
    }
  },

  async getMarkerInfo(id, accessToken) {
    try {
      const response = await Api.get(`/markers/${id}`, accessToken);
      if (response.status === 404) return false;
      if(response.status === 200) return await response.json();
    } catch (e) {
      console.log(e);
    }
    return null
  },

  async saveMarker(marker, accessToken) {
    try {
      const response =  await Api.post(`/markers`, marker, accessToken);
      if (response.ok) {
        return await response.json();
      }
    } catch (e) {
      console.log(e);
    }
    return false;
  },

  async deleteMarker(markerId, accessToken) {
    try {
      const response = await Api.delete(`/markers/${markerId}`, accessToken);
      return 204 === response.status;

    } catch (e) {
      console.log(e);
      return false;
    }
  },

  async voteMarker(markerId, vote, accessToken){
    try {
      const response = await Api.post(`/markers/${markerId}/votes`, vote, accessToken);
      if (response.status === 200)  return await response.json();
      if (response.status === 404) return false;
    } catch (e) {
      console.log(e);
    }
    return null;
  },

  async uploadMarkerImage(base64, accessToken) {
    try {
        return await Media.uploadImage(base64, accessToken, 'markers');
    } catch (e) {
      console.log(e);
      return false;
    }
  }
};

export default Map;
