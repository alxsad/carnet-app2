import Api from '../utils/api'

const News = {

  async getNewsByGai(params, accessToken) {
    try {
      const response = await Api.get(`/gai-news?limit=${params.limit}&date=${params.date}&userId=${params.userId}`, accessToken);
      if (!response.ok) {
        return false;
      }
      return await response.json();
    } catch (e) {
      console.log(e);
      return false;
    }
  },

  async getNewsByInterests(params, accessToken) {
     try {
       const response = await Api.get(`/partner-news?limit=${params.limit}&date=${params.date}&userId=${params.userId}`, accessToken);
       if (!response.ok) {
         return false;
       }
       return await response.json();
     } catch (e) {
       console.log(e);
       return false;
     }
  },

  async getGaiNewsById(newsId, accessToken) {
    try {
      const response = await Api.get(`/gai-news/${newsId}`, accessToken);
      if (!response.ok) {
        return false;
      }
      return await response.json();
    } catch (e) {
      console.log(e);
      return false;
    }
  },

  async getPartnerNewsById(newsId, accessToken) {
    try {
      const response = await Api.get(`/partner-news/${newsId}`, accessToken);
      if (!response.ok) {
        return false;
      }
      return await response.json();
    } catch (e) {
      console.log(e);
      return false;
    }
  },

  async callMeBack(userId, newsId, accessToken) {
    const params = { user_id: userId, post_id: newsId };
    try {
      const response = await Api.post(`/call-requests`, params, accessToken);
      return response.ok;
    } catch (e) {
      console.log(e);
      return false;
    }
  }
};

export default News;
