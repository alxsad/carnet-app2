import Api from '../utils/api';

const Domain = {
  async getInterestsList(accessToken) {
    try {
      const response = await Api.get(`/interests`, accessToken);
      if(response.ok){
        return response.json();
      }
    } catch (e) {
      console.log(e);
    }
    return null;
  },

  async isTokenExpired(accessToken) {
    try {
      const response = await Api.get(`/interests`, accessToken);
      return response.status === 401;
    } catch (e) {
      console.log(e);
    }
    return false;
  },

};
export default Domain;
