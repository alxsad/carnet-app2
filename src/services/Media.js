import Api from '../utils/api';

const Media = {

  async uploadImage(base64, accessToken, namespace) {
    try {
      const response = await Api.xhr(`/storage/images/${namespace}`, base64, 'POST',  accessToken, false);
      if (!response.ok) {
        return false;
      }
      const json = await response.json();
      return json.url;
    } catch(e) {
      console.log(e);
      return false;
    }
  },

  async bulkUploadImage(imagesBase64, accessToken, namespace) {
    const result = [];
    let promises = imagesBase64.map(async image => {
      const response = await this.uploadImage(image.data, accessToken, namespace);
      return status(response);
    });
    try {
      const responses = await Promise.all(promises);
      responses.map(image => result.push({ url: image }));
      return result;
    } catch(e) {
      console.log(e);
    }
    return false;
  },
};

const status = response => {
  if (false === response) {
    return Promise.reject(response);
  } else {
    return Promise.resolve(response);
  }
};

export default Media;
