import Api from '../utils/api';

const Offense = {

  async createOffense(offense, accessToken) {
    try {
      const response =  await Api.post(`/offenses`, offense, accessToken);
      if (response.ok) {
        return await response.json();
      }
    } catch (e) {
      console.log(e);
    }
    return false;
  },

  async getOffenses(userId, accessToken) {
    try {
      const response = await Api.get(`/offenses?user_id=${userId}`, accessToken);
      if (response.ok) {
        return await response.json();
      }
    } catch (e) {
      console.log(e);
    }
    return false;
  },
};

export default Offense;
