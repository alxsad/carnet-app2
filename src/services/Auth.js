import Api from '../utils/api';

const Auth = {

  async updateDeviceToken(uuid, deviceToken, accessToken) {
    try {
      const response = await Api.patch(`/devices/${uuid}`, {token: deviceToken}, accessToken);
      return 204 === response.status;
    } catch (e) {
      console.log(e);
      return false;
    }
  },

  async sendPhone(phone) {
    try {
      const response = await Api.post(`/passwords`, { phone });
      if (204 === response.status) {
        return true;
      }
      if (400 === response.status) {
        return false;
      }
    } catch (e) {
      console.log(e);
    }
    return null;
  },

  async verifyOTP(phone, code, uuid, platform) {
    const params = { phone, code, uuid, platform };
    try {
      const response = await Api.post(`/access-tokens`, params);
      if (400 === response.status) {
        return false;
      }
      if (200 === response.status) {
        return await response.json();
      }
    } catch (e) {
      console.log(e);
    }
    return null;
  }
};

export default Auth;
