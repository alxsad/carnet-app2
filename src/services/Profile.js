import Api from '../utils/api'

const Profile = {

  async saveProfile(profile, accessToken) {
    try {
      const response = await Api.put(`/users/${profile.id}`, profile, accessToken);
      return response.ok && response.status === 200;
    } catch (e) {
      console.log(e);
      return false;
    }
  },

  async getPublicProfile(carNumber, accessToken) {
    const encodedCarNumber = encodeURIComponent(carNumber);
    try {
      const response = await Api.get(`/public-profiles?car_number=${encodedCarNumber}`, accessToken);
      if (!response.ok) {
        return false;
      }
      return await response.json();
    } catch (e) {
      console.log(e);
      return false;
    }
  },

  async getPublicProfileByUserId(userId, accessToken) {
    try {
      const response = await Api.get(`/public-profiles?user_id=${userId}`, accessToken);
      if (!response.ok) {
        return false;
      }
      return await response.json();
    } catch (e) {
      console.log(e);
      return false;
    }
  },

  async getProfileByUserId(userId, accessToken) {
    try {
      const response = await Api.get(`/users/${userId}`, accessToken);
      if (!response.ok) {
        return null;
      }
      return await response.json();
    } catch (e) {
      console.log(e);
      return null;
    }
  }
};

export default Profile;
