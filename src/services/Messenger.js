import Api from '../utils/api'

class Messenger {

  async receiveUnreadMessages(accessToken) {
    try {
      const response = await Api.get('/messages', accessToken);
      return 200 === response.status ? await response.json() : false;
    } catch (e) {
      console.log(e);
      return false;
    }
  }

  async sendMessage(id, chatId, recipientId, text, accessToken) {
    try {
      const data = {id, chat_id: chatId, recipient_id: recipientId, text};
      const response = await Api.post('/messages', data, accessToken);
      return 204 === response.status;
    } catch (e) {
      console.log(e);
      return false;
    }
  }

  async markMessagesAsRead(accessToken) {
    try {
      const response = await Api.delete('/messages', accessToken);
      return 204 === response.status;
    } catch (e) {
      console.log(e);
      return false;
    }
  }

  async markMessageAsRead(messageId, accessToken) {
    try {
      const response = await Api.delete(`/messages/${messageId}`, accessToken);
      return 204 === response.status;
    } catch (e) {
      console.log(e);
      return false;
    }
  }
}

export default new Messenger();
