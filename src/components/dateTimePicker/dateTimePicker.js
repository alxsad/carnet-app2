import React, { Component } from 'react';
import DateTimePicker from 'react-native-modal-datetime-picker';
import {Text, View} from 'react-native';
import {platformText} from '../../conf'

import {modalStyle} from '../modalPicker/style';
const cancelButton = <Text style={modalStyle.cancelText}>{platformText.Cancel}</Text>;
const customTitle =  <View style={modalStyle.titleContainer}>
  <Text style={modalStyle.title}>Выберите дату</Text>
</View>;
const customConfirm =  <View style={modalStyle.confirmButton}>
  <Text style={modalStyle.confirmText}>Выбрать</Text>
</View>;

export default class  extends Component {
  state = {
    isDateTimePickerVisible: false,
  };

  show = () => {
    this.setState({ isDateTimePickerVisible: true });
  };

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

  _handleDatePicked = (dateTime) => {
    this.props.cb(dateTime);
    this._hideDateTimePicker();
  };

  render () {
    const {maximumDate} = this.props;
    return (
        <DateTimePicker
          show={() => this.show()}
          customTitleContainerIOS={customTitle}
          customCancelButtonIOS={cancelButton}
          customConfirmButtonIOS={customConfirm}
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this._handleDatePicked}
          onCancel={this._hideDateTimePicker}
          mode="datetime"
          maximumDate={maximumDate}
          contentContainerStyleIOS={modalStyle.contentContainer}
          datePickerContainerStyleIOS={modalStyle.modalPickerContainer}
          reactNativeModalPropsIOS={{
            backdropOpacity: 0.4,
            onBackdropPress: this._hideDateTimePicker
          }}
          //datePickerModeAndroid='spinner'
        />
    );
  }
}
