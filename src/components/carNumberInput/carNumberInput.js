'use strict';

import React, {Component} from 'react';
import {StyleSheet, View, Text, Alert} from 'react-native';
import {TextInputMask,} from 'react-native-masked-text';

class CarNumberInput extends Component {
  constructor(props, context){
    super(props, context);
  }
  render() {
    const {
      refer = 'textNumberRef',
      autoFocus = false,
      onChangeText,
      value,
      title,
      inputStyle,
      titleStyle,
      placeholderTextColor,
      caretHidden = true
    } = this.props;
    return (
      <View style={styles.container}>
        {
          title &&
          <Text style={[styles.text, titleStyle]}>{title}</Text>
        }
        <TextInputMask
          ref={refer}
          placeholder='0001 WZ-7'
          placeholderTextColor={placeholderTextColor}
          maxLength={9}
          type={'custom'}
          style={[styles.text, inputStyle]}
          options={{mask: '9999 AA-9'}}
          autoFocus={autoFocus}
          autoCorrect={false}
          caretHidden={caretHidden}
          autoCapitalize="characters"
          value={value}
          underlineColorAndroid="transparent"
          onChangeText={onChangeText}
          check={(checkCb, successCb) => this.check(checkCb, successCb)}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  text: {
    // paddingVertical: 0,
    fontSize: 17,
    backgroundColor: '#FFFFFF',
  }
});

export default CarNumberInput;
