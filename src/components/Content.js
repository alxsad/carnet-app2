'use strict';

import React, {Component, PropTypes} from 'react';
import {StyleSheet, Text, View} from 'react-native';

class Content extends Component {
  renderTitle() {
    return this.props.title ?
      <Text style={[styles.text, this.props.titleStyle]}>{this.props.title}</Text> :
      null
    ;
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.title}>{this.renderTitle()}</View>
        <View style={styles.content}>{this.props.children}</View>
      </View>
    );
  }
}

// Content.propTypes = {
//   title: PropTypes.string
// };

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //alignItems: 'stretch',
    flexDirection: 'column',
  },
  title: {
    marginTop: 15,
  },
  content: {
  },
  text: {
    fontSize: 14,
    color: 'gray',
  }
});

export default Content;
