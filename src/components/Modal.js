import React from 'react'
import {
    Text,
    View,
    Dimensions,
    TouchableOpacity,
    ActivityIndicator,
    TouchableWithoutFeedback
} from 'react-native';
import SlidingUpPanel from 'rn-sliding-up-panel'
import {connect} from "react-redux";
import {normalize} from "../conf";
import {Divider} from 'react-native-elements';
import {DEFAULT_COLOR} from "../styles";
import {setSelectedMarker, voteMarker, deleteMarkerFromMap} from "../app/actions/map";
import MarkerInfo from '../views/map/screens/MarkerInfo';
import {MapConf} from '../domain';
import {Icon} from "./index";
import {Map} from "../services";
import moment from 'moment';
import {showMessage} from "react-native-messages";
import {
    DEFAULT_ERROR_MESSAGE,
    MARKER_VOTED_MESSAGE,
    NO_CONNECTION,
    MARKER_HAS_BEEN_REMOVED
} from "../domain/MessagesList";

const {height, width} = Dimensions.get('window');

const styles = {
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'white',
        justifyContent: 'space-between',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
    },
    infoContainer: {
        flex: 0.65,
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'center',
        paddingHorizontal: 10
    },
    buttonContainer: {
        flex: 0.35,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderTopWidth: 1,
        borderColor: '#dcdcdc',
    },
    dividerContainer: {
        position: 'absolute',
        zIndex: 2,
        top: 0,
        right: 100,
        left: 100,
        justifyContent: 'center',
        alignItems: 'center',
    },
    divider: {
        marginTop: 8,
        backgroundColor: DEFAULT_COLOR,
        width: width / 12,
        height: 5,
        borderRadius: 8 >> 1,
    },
    closeButton: {
        flexDirection: 'row',
        padding: 10,
        alignItems: 'center',
        zIndex: 1,
        position: 'absolute',
        right: 0,
        top: 0,
        height: 60,
        width: 60
    }
};

class BottomSheet extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      showMoreLoader: false,
      voteButtonsDisabled: false,
    };
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.state.showMoreLoader !== prevState.showMoreLoader) {
      return;
    }

    if (this.props.selectedMarker && prevProps.selectedMarker) {
      return;
    }

    if (!this.props.selectedMarker && prevProps.selectedMarker) {
      this._panel.hide();
      return;
    }

    if (this.props.selectedMarker) {
      this._panel.show(400);
      setTimeout(() => this._panel.hide(), 500);//TODO: this is a library bug( rework didupdate later
      return;
    }
    this._panel.hide();
  }

  onCloseModal = () => {
    this.props.setSelectedMarker(null);
  };

  toggleShowMoreLoader = () => {
    this.setState({showMoreLoader: !this.state.showMoreLoader})
  };

  toggleVoteButtonsDisability = (val) => {
    this.setState({voteButtonsDisabled: val ?? !this.state.voteButtonsDisabled});
  };

  showMessage = (type = 'error', text = DEFAULT_ERROR_MESSAGE) => {
    this.toggleVoteButtonsDisability(false);
    showMessage({type, text});
  };

  showMoreInfo = async () => {
    const {properties} = this.props.selectedMarker;

    this.toggleShowMoreLoader();
    const marker = await Map.getMarkerInfo(properties.id, this.props.accessToken);
    this.toggleShowMoreLoader();

    if (marker) {
      this.props.navigation.navigate('MapModal', {
        content: <MarkerInfo navigation={this.props.navigation} marker={marker}/>,
        title: MapConf.TypesDescription[marker.type],
      });
    } else if (marker === false) {
      this.showMessage('warning', MARKER_HAS_BEEN_REMOVED);
      this.props.deleteMarkerFromMap(properties.id);
    } else {
      this.showMessage();
    }
  };

  onMarkerVote = async (vote) => {
    if (!this.props.isConnected) {
      showMessage({type: 'warning', text: NO_CONNECTION});
      return;
    }

    const {selectedMarker} = this.props;
    this.toggleVoteButtonsDisability();

    const res = await this.props.voteMarker(selectedMarker.properties.id, vote);
    if (res) {
      this.showMessage('success', MARKER_VOTED_MESSAGE);
      return;
    }
    if (res === false) {
      this.showMessage('warning', MARKER_HAS_BEEN_REMOVED);
      this.props.deleteMarkerFromMap(selectedMarker.properties.id);
      return;
    }
    this.showMessage();
  };

  render() {
    const {selectedMarker} = this.props;

    let innerChildView = <View style={styles.container}/>;

    if (selectedMarker) {
      innerChildView = (
        <View style={styles.container}>
          <TouchableWithoutFeedback disabled={this.state.showMoreLoader}
                                    onPress={() => this.showMoreInfo(selectedMarker.id)}>
            <View style={styles.infoContainer}>
              <View style={styles.dividerContainer}>
                {this.state.showMoreLoader &&
                <ActivityIndicator style={{marginTop: 8}} color={DEFAULT_COLOR} animating
                                   size="small"/> ||
                <Divider style={styles.divider}/>
                }
              </View>
              <TouchableOpacity
                onPress={() => this.onCloseModal()}
                style={styles.closeButton}
              >
                <Icon name="rounded_close" size={30} color={DEFAULT_COLOR}/>
              </TouchableOpacity>
              <Text style={{fontSize: normalize(17), fontWeight: 'bold', marginBottom: 5}}>
                {MapConf.TypesDescription[selectedMarker.properties.icon]}
              </Text>
              <Text style={{fontSize: normalize(16), marginBottom: 4}}>
                {selectedMarker.properties.title}
              </Text>
              <Text style={{fontSize: normalize(14), color: '#6a6a6a'}}>
                Добавлено {moment(selectedMarker.properties.created).fromNow()}
              </Text>
              {selectedMarker.properties.status === 'unknown' &&
              <Text style={{fontSize: normalize(14), marginTop: 3, color: 'red'}}>
                {`Отмечено как неактуальное ${moment(selectedMarker.properties.markedAsUnknownDate).fromNow()}`}
              </Text>
              }
            </View>
          </TouchableWithoutFeedback>
          <View style={styles.buttonContainer}>
            <View style={{
              position: 'absolute',
              right: 100,
              left: 100,
              justifyContent: 'center',
              alignItems: 'center'
            }}>
              <Divider style={{backgroundColor: '#dcdcdc', width: 1.4, height: 50}}/>
            </View>
            <View style={{flex: 1}}>
              <TouchableOpacity style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}
                                onPress={() => this.onMarkerVote(true)}
                                disabled={this.state.voteButtonsDisabled}>
                <Text style={{fontSize: normalize(18), color: '#0F3E08'}}>Актуально</Text>
              </TouchableOpacity>
            </View>
            <View style={{flex: 1}}>
              <TouchableOpacity style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}
                                onPress={() => this.onMarkerVote(false)}
                                disabled={this.state.voteButtonsDisabled}>
                <Text style={{fontSize: normalize(18), color: '#3D0D09'}}>Неактуально</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      );
    }
    return (
      <View>
        <SlidingUpPanel
          ref={c => this._panel = c}
          showBackdrop={false}
          draggableRange={{top: height * 0.35, bottom: 0}}
          height={height * 0.35}
          onDragEnd={() => this.onCloseModal()}
        >
          {innerChildView}
        </SlidingUpPanel>
      </View>
    )
  }
}

const propsMapping = state => {
    return {
        selectedMarker: state.map.selectedMarker,
        accessToken: state.auth.accessToken,
        isConnected: state.network.isConnected,
    }
};
const actionMapping = dispatch => {
    return {
        setSelectedMarker: (selectedMarker) => dispatch(setSelectedMarker(selectedMarker)),
        voteMarker: (markerId, vote, finishCb, errorCb) => dispatch(voteMarker(markerId, vote, finishCb, errorCb)),
        deleteMarkerFromMap: markerId => dispatch(deleteMarkerFromMap(markerId)),
    }
};

export default connect(propsMapping, actionMapping)(BottomSheet)
