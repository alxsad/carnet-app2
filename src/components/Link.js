import React, {Component, PropTypes} from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {Icon} from '../components/index';
import {DEFAULT_COLOR} from '../styles/app'

class Link extends Component {
  render() {
    const {onPress, icon, title, titleStyle, disabled = false} = this.props;
    return <TouchableOpacity
      style={styles.container}
      onPress={onPress}
      disabled={disabled}
    >
      <Icon name={icon} size={20} color={disabled ? '#cacaca':DEFAULT_COLOR}/>
      <Text style={[styles.text, titleStyle, disabled && {color: '#cacaca',}]}>{title}</Text>
    </TouchableOpacity>
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  text: {
    color: DEFAULT_COLOR,
    marginLeft: 10,
  },
});

export default Link;
