import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
  Alert,
} from 'react-native';

import MapView from 'react-native-maps';
import Geocoder from 'react-native-geocoder';
import { PROVIDER_GOOGLE } from 'react-native-maps';
import Regions, {RegionEnum} from '../../domain/Region';
import {Icon} from '../../components/index';
import {DEFAULT_COLOR} from '../../styles';

Geocoder.fallbackToGoogle('AIzaSyDpiFEeY_AcnP3v6trUepKj82zxzn1wIZM');

const LATITUDE_DELTA = 0.01;
const LONGITUDE_DELTA = 0.01;
const LATITUDE = 53.91634580916043;

const initialRegion = {
    latitude: LATITUDE,
    longitude: 27.529487907886505,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
};

const getOblast = (adminArea) => {
    switch (adminArea) {
        case Regions.BREST_EN:
        case Regions.BREST_RU:
            return RegionEnum.brest;
        case Regions.GOMEL_EN:
        case Regions.GOMEL_RU:
            return RegionEnum.gomel;
        case Regions.GRODNO_EN:
        case Regions.GRODNO_RU:
            return RegionEnum.grodno;
        case Regions.MOGILEV_EN:
        case Regions.MOGILEV_RU:
            return RegionEnum.brest;
        case Regions.VITSEBSK_EN:
        case Regions.VITSEBSK_RU:
            return RegionEnum.vitebsk;
        case Regions.MINSK_EN:
        case Regions.MINSK_RU:
            return RegionEnum.minsk;
        default:
            return RegionEnum.default;
    }
};

export default class GoogleMarkerMap extends Component {

  constructor(props) {
    super(props);
    this.state = {
      region: { },
      address: '',
      obl: 'none',
      isLocationLoaded: false,
    };
  }

  componentDidMount() {
    this._mounted = true;
    navigator.geolocation.getCurrentPosition(
      (position) => {
        const region = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          };
        this._mounted && this.setRegion(region);
      },

      (error) => alert(error.message),
      { enableHighAccuracy: true, timeout: 20000 }
    );

    this.watchID = navigator.geolocation.watchPosition((position) => {
    });
  }

  componentWillUnmount() {
    this._mounted = false;
    navigator.geolocation.clearWatch(this.watchID);
  }

    onRegionChangeComplete(region) {
    this.setState({ region },  this.setAddress(region));

  }

  setRegion(region) {
    setTimeout(() => this.map.animateToRegion(region), 400);
  }

  setAddress(coordinates) {
      if(coordinates.latitude === null || coordinates.latitude === LATITUDE) {
          return;
      }
    const position = {
      lat: coordinates.latitude,
      lng: coordinates.longitude,
    };
    Geocoder.geocodePosition(position).then(res => {
      const result = res['0'];
      if(res.adminArea === null){
          return '';
      }
      const obl = getOblast(result.adminArea);
      const country = result.country;
      console.log(result.adminArea);
      let address = result.formattedAddress;
      if(result.streetName && result.locality === 'Minsk'){
         address = result.streetName + ', Minsk'
      }
      this._mounted && this.setState({
        address: address,
        obl,
        country,
        isLocationLoaded: true
      });
     })
      .catch(err => {
        console.log(err); return '';
      });
  }

  mapConfirm = () => {
      const region = this.state.country.toLowerCase();
      if(region !== RegionEnum.default && region !== 'беларусь'){
          Alert.alert(
              'Похоже, что метка находится за пределами республики Беларусь',
          );
          return;
      }

    const result =
      {
        location: {
            longitude: this.state.region.longitude,
            latitude: this.state.region.latitude,
            address:  this.state.address
        },
        region: this.state.obl
      };

    this.props.locationCb(result);
    this.props.navigation.goBack();
  };

  render() {
    return (
      <View style={styles.container}>
        <MapView
          ref={ map => { this.map = map }}
          provider={PROVIDER_GOOGLE}
          style={styles.map}
          initialRegion={initialRegion}
          onRegionChangeComplete={this.onRegionChangeComplete.bind(this)}
        >
        </MapView>
        <View style={styles.buttonContainer}>
            <TouchableOpacity style={styles.bubbleButtonClose} onPress={() => this.props.navigation.goBack()}>
              <Text style={{ textAlign: 'center' }}>
                Отмена
              </Text>
            </TouchableOpacity>
            {this.state.isLocationLoaded &&
              <TouchableOpacity style={styles.bubbleButtonSubmit} onPress={() => this.mapConfirm()}>
                <Text style={{ textAlign: 'center' }}>
                  Выбрать
                </Text>
              </TouchableOpacity> ||
              <ActivityIndicator
                animating={true}
                style={styles.bubbleButtonSubmit}
                size="small"
              />
            }
        </View>

          <View style={styles.marker}>
              <Icon name="marker" size={30} color={DEFAULT_COLOR}/>
          </View>

        <View style={styles.bubble}>
          <Text style={{ textAlign: 'center' }}>
            {this.state.address}
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
  map: {
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
    },
  bubble: {
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20,
  },
  buttonContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start'
  },
  bubbleButtonClose: {
    marginTop: 40,
    marginLeft: 10,
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20,
  },
  bubbleButtonSubmit: {
    marginTop: 40,
    marginRight: 10,
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20,
  },
    marker: {
        position: 'absolute',
        left: (Dimensions.get('window').width / 2) - 10,
        top: (Dimensions.get('window').height / 2) - 50,
    }
});
