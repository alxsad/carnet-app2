'use strict';

import React, {Component, PropTypes} from 'react';
import {StyleSheet, Text, View} from 'react-native';

class Form extends Component {
  renderTitle() {
    return this.props.title ?
      <Text style={styles.title}>{this.props.title}</Text> :
      null
    ;
  }
  render() {
    return (
      <View style={styles.container}>
        {this.renderTitle()}
        {this.props.children}
      </View>
    );
  }
}

// Form.propTypes = {
//   title: PropTypes.string
// };

const styles = StyleSheet.create({
  container: {
    flex: 0,
    flexDirection: 'column',
    backgroundColor: '#fff',
  },
  title: {
  }
});

export default Form;
