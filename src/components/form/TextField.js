'use strict';

import React, {Component, PropTypes} from 'react';
import {StyleSheet, Text, View, TextInput} from 'react-native';

class TextField extends Component {
  render() {
    const {
      title,
      titleStyle,
      keyboardType,
      placeholder,
      value,
      valueStyle,
      onChangeText,
      editable,
      maxLength,
      placeholderTextColor,
      autoFocus = false,
    } = this.props;
    return (
      <View style={styles.container}>
        <Text style={[styles.title , titleStyle]}>{title}</Text>
        <TextInput
            onChangeText={onChangeText}
            style={[styles.input, valueStyle]}
            title={title}
            keyboardType={keyboardType}
            placeholder={placeholder}
            placeholderTextColor={placeholderTextColor}
            defaultValue={value}
            editable={editable}
            maxLength={maxLength}
            autoFocus={autoFocus}
            underlineColorAndroid="transparent"
            autoCorrect={false}
        />
      </View>
    );
  }
}

// TextField.propTypes = {
//     placeholder: PropTypes.string,
//     keyboardType: PropTypes.string,
//     title: PropTypes.string.isRequired,
//     value: PropTypes.string,
//     onChangeText: PropTypes.func,
//     editable: PropTypes.bool,
//     maxLength: PropTypes.number
// };

TextField.defaultProps = {
  keyboardType: 'default',
  editable: true,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  title: {
    //flex: 1,
  },
  input: {
    flex: 1,
    textAlign: 'right',
    paddingVertical: 0,
  }
});

export default TextField;
