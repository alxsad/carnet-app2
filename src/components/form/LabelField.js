'use strict';

import React, {Component, PropTypes} from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';

class LabelField extends Component {
  render() {
    const {
      placeholder,
      onPress, value,
      title,
      titleStyle,
      valueStyle,
      disabled=false
    } = this.props;
    const text = value || placeholder;
      return (
        <TouchableOpacity disabled={disabled} style={styles.container} onPress={onPress}>
          <Text style={[styles.title, titleStyle]}>{title}</Text>
          { text &&
          <View style={styles.input}>
            <Text style={[styles.text, valueStyle]} >{text}</Text>
          </View>
          }
        </TouchableOpacity>
      )
  }
}
export default LabelField;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  title: {
    marginRight: 5,
    //flex: 1,
  },
  input: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  text: {
    color: '#cacaca',
    fontSize: 17,
  },
});


