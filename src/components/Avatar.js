import React, {Component, PropTypes} from 'react';
import {StyleSheet, TouchableOpacity, Image, View, Dimensions} from 'react-native';

class Avatar extends Component {
  render() {
    const {avatarStyle} = this.props;
    return (
      <View style={styles.container}>
        <TouchableOpacity disabled={this.props.disabled} onPress={this.props.onPress}>
          <Image style={[styles.avatar, avatarStyle]} source={{uri: this.props.url}} />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
  },
  avatar: {
    height: 150,
    width: 150,
    borderRadius: 75,
    borderColor: 'white',
    borderWidth: 2,
  },
});
// Avatar.propTypes = {
//   url: PropTypes.string,
//   onPress: PropTypes.func,
// };

Avatar.defaultProps = {
  url: undefined,
  onPress: undefined,
};

export default Avatar;
