import React, { Component } from 'react';
import { Animated, View, StyleSheet, Platform} from 'react-native';
import { TabViewAnimated, TabBar, TabViewPagerPan , TabViewPagerScroll} from 'react-native-tab-view';
import {DEFAULT_COLOR} from '../../styles/app'

export default class TopTapBar extends Component {

  constructor(props) {
   super(props);
 }

  static backgroundColor = '#fff';
  static tintColor = '#222';
  static appbarElevation = 0;

  // static propTypes = {
  //   style: View.propTypes.style,
  // };

  state = {
    index: 0,
    routes: this.props.tapBarRoutes,
    lazy: this.props.lazy

  };
  componentWillReceiveProps(nextProps){
      nextProps.tapBarRoutes.map((item, index) => {
        if(item.hint && item.hint !== this.props.tapBarRoutes[index].hint){
          this.setState({routes: nextProps.tapBarRoutes});
          return;
        }
      })
  }

  // _first: Object;
  // _second: Object;

  _handleChangeTab = (index) => {
    this.setState({
      index,
    });
  };

//scrolling up at tab title press
  _handleTabItemPress = route => {
    if (route !== this.state.routes[this.state.index]) {
      return;
    }
    switch (route.key) {
    case '1':
      //this._first.scrollTo({ y: 0 });
      break;
    case '2':
      //this._second.scrollTo({ y: 0 });
      break;
    }
  };

  _renderLabel = (props: any) => ({ route, index }) => {
    const inputRange = props.navigationState.routes.map((x, i) => i);
    //const outputRangeColour = inputRange.map(inputIndex => inputIndex === index ? '#fff' : '#fff');// fontColor taps
    const outputRangeFontSize = inputRange.map(inputIndex => inputIndex === index ? 15 : 13);//fontSize taps
    const fontSize = props.position.interpolate({
      inputRange,
      outputRange: outputRangeFontSize,
    });

    const outputRangeCountFontSize = inputRange.map(inputIndex => inputIndex === index ? 13 : 10);
    const fontCountSize = props.position.interpolate({
      inputRange,
      outputRange: outputRangeCountFontSize,
    });

    return (
      <View style={{flexDirection:'row', alignItems:'center', justifyContent: 'center'}}>
        <Animated.Text style={[ styles.label, { fontSize } ]}>
          {route.title}
        </Animated.Text>
        {route.hint &&
          <Animated.Text style={[ styles.label, { fontSize: fontCountSize } ]}>
            ({ route.hint })
          </Animated.Text>
        }
      </View>
    );
  };

  _renderHeader = (props) => {
    return (
      <TabBar
        {...props}
        pressColor='rgba(255, 64, 129, .5)'
        //onTabPress={this._handleTabItemPress}
        renderLabel={this._renderLabel(props)}
        indicatorStyle={styles.indicator}
        tabStyle={styles.tab}
        style={styles.tabbar}
      />
    );
  };

  _renderScene =({route}) => this.props.renderAction(route);

  _renderPager = props => {
    return (Platform.OS === 'ios') ? <TabViewPagerScroll {...props} /> : <TabViewPagerPan {...props}/>
  };

  render() {
    return (
      <TabViewAnimated
        style={[ styles.container, this.props.style ]}
        navigationState={this.state}
        renderScene={this._renderScene}
        renderHeader={this._renderHeader}
        onIndexChange={this._handleChangeTab}
        lazy={this.state.lazy}
        renderPager={this._renderPager}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  indicator: {
    backgroundColor: '#fff',// полоска под табами
  },
  label: {
    color: '#fff',
    //fontWeight: 'bold',
    margin: 3,
  },
  tabbar: {
    backgroundColor: DEFAULT_COLOR,// шапка
    //zIndex: 0
  },
  tab: {
    opacity: 1,
    //width: 150,
    height: 40,
  },
  page: {
    backgroundColor: '#f9f9f9',
  },
});
