import React from 'react';
import { ActivityIndicator, View, StyleSheet, Text } from 'react-native';

//obsolete since auth screens reworked
export default LoadSpinner = props => {
  return (
    <View style={styles.container}>
      <ActivityIndicator
        animating={props.animating}
        style={styles.activityIndicator}
        size="large"
      />
      <Text>{props.description}</Text>
    </View>
  );
}

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  activityIndicator: {
  }
});
