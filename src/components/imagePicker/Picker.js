'use strict';

import React, {Component} from 'react';
import {View} from 'react-native';

import ImagePicker from 'react-native-image-picker';

export default class extends Component {
  state = {
  };

  show = (opt) => {
    ImagePicker.showImagePicker(opt, response => {
      if(response && response.data) {
        this.props.cb(response)
      }
    });
  };

  render() {
    return(
      <View show={(options) => this.show(options)}/>
    );
  }
}


