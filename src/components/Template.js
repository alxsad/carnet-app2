import React, {Component, PropTypes} from 'react';
import {StyleSheet, Text, View} from 'react-native';

class Template extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>Template</Text>
      </View>
    );
  }
}

Template.propTypes = {
};

Template.defaultProps = {
};

const styles = StyleSheet.create({
  container: {
  },
});

export default Template;
