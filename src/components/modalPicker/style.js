import { StyleSheet, Platform } from 'react-native';

const BORDER_RADIUS = 14;
const BACKGROUND_COLOR = 'white';
const BORDER_COLOR = '#d5d5d5';
const TITLE_FONT_SIZE = 13;
const TITLE_COLOR = '#A8AFAD';
const BUTTON_FONT_WEIGHT = 'normal';
const BUTTON_FONT_COLOR = '#007ff9';
const BUTTON_FONT_SIZE = 20;


 const androidStyles = StyleSheet.create({
   contentContainer: {
     justifyContent: 'center',
     margin: 40,
   },
   modalPickerContainer: {
     backgroundColor: BACKGROUND_COLOR,
     // borderRadius: BORDER_RADIUS,
     marginBottom: 8,
   },
   itemContainer: {
     borderBottomWidth: 1,
     borderColor: '#dcdcdc',
   },
   itemText: {
     fontSize: 20,
     color:'#3b3b3b',
     padding: 15,
   },
   buttonContainer: {
     flexDirection: 'row',
     justifyContent: 'flex-end',
     padding: 18,
   },
   button: {
     fontSize: 14,
     color: '#5CC3E9',
     fontWeight: 'bold',
   }
 });

 const iosStyles = StyleSheet.create({
  contentContainer: {
    justifyContent: 'flex-end',
    margin: 11,
  },
  modalPickerContainer: {
    backgroundColor: BACKGROUND_COLOR,
    borderRadius: BORDER_RADIUS,
    marginBottom: 8,
    opacity: 1,
  },
  titleContainer: {
    borderBottomColor: BORDER_COLOR,
    borderBottomWidth: StyleSheet.hairlineWidth,
    padding: 12,
    backgroundColor: 'transparent',
  },
  title: {
    textAlign: 'center',
    color: TITLE_COLOR,
    fontSize: TITLE_FONT_SIZE,
  },
  confirmButton: {
    borderColor: BORDER_COLOR,
    borderTopWidth: StyleSheet.hairlineWidth,
    backgroundColor: 'transparent',
  },
  confirmText: {
    textAlign: 'center',
    color: BUTTON_FONT_COLOR,
    fontSize: BUTTON_FONT_SIZE,
    fontWeight: BUTTON_FONT_WEIGHT,
    backgroundColor: 'transparent',
    padding: 10,
  },
  cancelButton: {
    backgroundColor: BACKGROUND_COLOR,
    borderRadius: BORDER_RADIUS,
  },
  cancelText: {
    padding: 16,
    textAlign: 'center',
    color: BUTTON_FONT_COLOR,
    fontSize: BUTTON_FONT_SIZE,
    fontWeight: '600',
    backgroundColor: 'transparent',
  },
});

export const modalStyle = Platform.OS === 'ios' ? iosStyles : androidStyles;
