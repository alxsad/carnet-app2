import React, { Component } from 'react';
import { Picker, TouchableOpacity, View, Text } from 'react-native';
// import Modal from 'react-native-modal';
import {platformText} from '../../conf';

import {modalStyle} from './style';

const Item = Picker.Item;

export default class ModalPicker extends Component {
  constructor(props, context){
    super(props, context);
    this.state = {
      selectedItem: props.items[0].value,
      visibleModal: null
    }
  };

  _renderModalContent = (children) => (
    <View>
      <View style={modalStyle.modalPickerContainer}>
        <View style={modalStyle.titleContainer}>
          <Text style={modalStyle.title}>Выберите тип нарушения</Text>
        </View>
        <View>
          {children}
        </View>
        <View style={modalStyle.confirmButton}>
          <TouchableOpacity
            onPress={this._handleConfirm}
            //disabled={this.state.userIsInteractingWithPicker}
          >
            <Text style={modalStyle.confirmText}>Выбрать</Text>
          </TouchableOpacity>
        </View>
      </View>

      <View style={modalStyle.cancelButton}>
        <TouchableOpacity onPress={() => this.setState({ visibleModal: null })}>
          <Text style={modalStyle.cancelText}>{platformText.Cancel}</Text>
        </TouchableOpacity>
      </View>
    </View>
  );

  show = () => {
    this.setState({visibleModal: 1})
  };

  _handleConfirm = () => {
    const offenseType = this.props.items.filter((_, index) => _.value === this.state.selectedItem)[0];
    this.setState({ visibleModal: null },
      this.props.cb(offenseType));
  };

  render () {
    const {items} = this.props;
    const content = (
      <Picker
        style={{width: '100%'}}
        selectedValue={this.state.selectedItem}
        onValueChange={item => {this.setState({selectedItem: item})
        }}
      >
        {items.map((l, i) => {return <Item label={l.label}  value={l.value} key={i} /> })}
      </Picker>
    );
    // return (
    //   <Modal
    //     show={() => this.show()}
    //     style={modalStyle.contentContainer}
    //     isVisible={this.state.visibleModal === 1}
    //     onBackdropPress={() => this.setState({ visibleModal: null })}
    //     backdropOpacity={0.5}
    //   >
    //     {this._renderModalContent(content)}
    //   </Modal>
    // );
  }

}
