import React, { Component } from 'react';
import { TouchableOpacity, View, FlatList, Text } from 'react-native';

// import Modal from 'react-native-modal';
import {modalStyle} from './style';
import {platformText} from '../../conf';

export default class extends Component {
  constructor(props, context){
    super(props, context);
    this.state = {
      selectedItem: props.items[0],
      visibleModal: null
    }
  };

  _renderModalContent = (children) => (
    <View >
      <View style={modalStyle.modalPickerContainer}>
        <View>
          {children}
        </View>
      </View>
    </View>
  );

  show = () => {
    this.setState({visibleModal: 1})
  };

  renderRow = (item) => {
    return (
        <TouchableOpacity onPress={() => this.setState({ selectedItem: item, visibleModal: null }, this.props.cb(item))}>
          <Text style={modalStyle.itemText}>{item.label}</Text>
        </TouchableOpacity>
    )
  };

  render () {
    const {items} = this.props;
    const content = (
      <View>
      <FlatList
        data={items}
        renderItem={({item}) => this.renderRow(item)}
        keyExtractor={(item, index) => index}

        // enableEmptySections={true}
      />
        <View>
          <TouchableOpacity style={modalStyle.buttonContainer} onPress={() => this.setState({ visibleModal: null })}>
            <Text style={modalStyle.button}>{platformText.Cancel}</Text>
          </TouchableOpacity>
        </View>
      </View>

    );

    // return(
    //   <Modal
    //     show={() => this.show()}
    //     style={modalStyle.contentContainer}
    //     isVisible={this.state.visibleModal === 1}
    //     onBackdropPress={() => this.setState({ visibleModal: null })}
    //     animationIn={'zoomIn'}
    //     animationOut={'zoomOut'}
    //     animationInTiming={300}
    //     animationOutTiming={300}
    //     // backdropTransitionInTiming={1000}
    //     // backdropTransitionOutTiming={1000}
    //   >
    //     {this._renderModalContent(content)}
    //   </Modal>
    // )
  }

}
