import React, {Component} from 'react';
import {Text, View, StyleSheet} from 'react-native';
import { ifIphoneX } from 'react-native-iphone-x-helper';

class Message extends Component {
  render() {
    const {type, text} = this.props.message;
    return (
      <View style={[styles.container, styles[type]]}>
        <Text style={styles.text}>{text}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 20,
    paddingLeft: 10,
    paddingRight: 10,
    ...ifIphoneX({
      paddingTop: 55
    }, {
      paddingTop: 30
    })
  },
  warning: {
    backgroundColor: 'orange',
  },
  error: {
    backgroundColor: 'red',
  },
  success: {
    backgroundColor: 'green',
  },
  text: {
    color: 'white',
  },
});

export default Message;
