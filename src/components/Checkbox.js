import React, {Component, PropTypes} from 'react';
import {StyleSheet} from 'react-native';
import {Icon} from '../components/index';
import {DEFAULT_COLOR} from '../styles/app'

class Checkbox extends Component {
  render() {
    const {checked, ...other} = this.props;

    if (checked) {
      return (
        <Icon name="icon_check_checked" size={20} color={DEFAULT_COLOR} style={styles.container} {...other}/>
      )
    } else {
      return (
        <Icon name="icon_check" size={20} color={DEFAULT_COLOR}  style={styles.container} {...other}/>
      )
    }
  }
}

// Checkbox.propTypes = {
//   checked: PropTypes.bool
// };
//
// Checkbox.defaultProps = {
//   checked: false,
// };

const styles = StyleSheet.create({
  container: {
  },
});

export default Checkbox;
