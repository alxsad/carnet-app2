'use strict';

import React, {Component, PropTypes} from 'react';
import {View, StyleSheet} from 'react-native';

class Tag extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.styles] }>
        {this.props.children}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    borderColor: '#dcdcdc',
    borderWidth: 2,
    padding: 5,
  },
});

export default Tag;

// Tag.propTypes = {
//   styles: PropTypes.object,
// };

