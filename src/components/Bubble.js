import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import {ifIphoneX} from "react-native-iphone-x-helper";

const styles = StyleSheet.create({
  container: {
    borderRadius: 30,
    position: 'absolute',
    left: 48,
    right: 48,
    paddingVertical: 14,
    paddingHorizontal: 10,
    minHeight: 60,
    justifyContent: 'center',
    backgroundColor: 'white',
    ...ifIphoneX({
      bottom: 85
    }, {
      bottom: 70
    }),
  },
  onPressContainer: {
    // alignItems: 'center'
  }
});

class Bubble extends React.PureComponent {

  render() {
    if(!this.props.isVisible){
      return null;
    }

    let innerChildView = this.props.children;

    if (this.props.onPress) {
      innerChildView = (
        <TouchableOpacity style={[styles.onPressContainer, this.props.onPressStyle]} onPress={this.props.onPress}>
          {this.props.children}
        </TouchableOpacity>
      );
    }

    return (
      <View style={[styles.container, this.props.style]}>{innerChildView}</View>
    );
  }
}

export default Bubble;
