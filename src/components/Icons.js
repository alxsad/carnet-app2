'use strict';
import React, {Component} from 'react';

import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from '../assets/iconsFont/selection.json';
const Icon = createIconSetFromIcoMoon(icoMoonConfig);

class CarnetIcon extends Component {
  render() {
    const {name,
      size,
      color,
    } = this.props;
    return (
      <Icon {...this.props} name={name} size={size} color={color}/>
    );
  }
}

export default CarnetIcon;
