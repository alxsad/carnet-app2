'use strict';

import React, {Component} from 'react';
import {StyleSheet, View, Text, Image} from 'react-native';

class CarNumberPlate extends Component {
  constructor(props, context){
    super(props, context);
  }
  render() {
    const {carNumber, style} = this.props;
    return (
      <View style={[styles.carNumberContainer, style]}>
        <Image style={{resizeMode: 'contain'}}  source={require('./assets/BY.png')}>
        </Image>
        <View>
          <Text style={styles.carNumberText}>{carNumber}</Text>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  carNumberContainer: {
    flexDirection:'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    borderRadius: 3,
    borderColor: '#000000',
    borderWidth: 1,
    padding: 3,
    paddingTop: 0,
    paddingBottom: 0,
  },
  carNumberText: {
    fontSize: 21,
    marginLeft: 4,
  },
});

export default CarNumberPlate;
