import React, { Component } from 'react'
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  Modal,
  Platform,
} from 'react-native'
import Swiper from 'react-native-swiper'
import PhotoView from 'react-native-photo-view';
import {Icon} from '../../components/index';

const { width, height } = Dimensions.get('window');

const styles = {
  wrapper: {
    backgroundColor: '#000',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0
  },
  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 8,
  },
  photo: {
    width: width,
    height: height,
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  },
  thumbWrap: {
    height: 158,//TODO: make responsive
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  thumbItem: {
    flex: 1,
    marginLeft: 7,
    marginTop: 5,
    marginBottom: 5,
  },
  thumbImage: {
    flex: 1,
  },
};

const renderPagination = (index, total, context) => {
  return (
      <View style={{
        flex: 1,
        backgroundColor: 'black',
        position: 'absolute',
        justifyContent: 'space-between',
        alignItems: 'stretch',
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: Platform.OS === 'android' ? 15 : 30,
        paddingBottom: 15,
        top: 0,
        flexDirection: 'row',
        left: 0,
        right: 0,
      }}>
        <Text
          style={{flex:2, color: '#fff', fontSize: 14, paddingLeft: 5}}
          onPress={context.props.viewerPressHandle}
        >
          Готово
        </Text>
        <View style={{
          borderRadius: 7,
          backgroundColor: 'rgba(255,255,255,.15)',
          padding: 3,
          paddingHorizontal: 7,
        }}>
          <Text style={{
            color: '#fff',
            fontSize: 14,
          }}>{index + 1} из {total}</Text>
        </View>
        <TouchableOpacity
          style={{flex: 2, paddingRight: 10}}
          onPress={(currIndex) => context.props.removeImage(index)}
        >
          <Icon style={{alignSelf: 'flex-end',  }} name='icon_bin'  size= {20} color='white' />
        </TouchableOpacity>
      </View>
  )
};

const Viewer = props => <Swiper
                          index={props.index} style={styles.wrapper}
                          renderPagination={renderPagination}
                          viewerPressHandle = {props.pressHandle}
                          removeImage = {props.removeImage}>
  {
    props.imgList.map((item, i) => <View key={i} style={styles.slide}>
        <PhotoView
          source={{uri: item.uri}}
          // minimumZoomScale={1}
          maximumZoomScale={3}
          //onTap = {props.pressHandle}
          androidScaleType={"fitCenter"}
          style={styles.photo}
          centerContent={true}/>
    </View>)
  }
</Swiper>;

 export default class ImageSwiper extends Component {
  constructor (props) {
    super(props);
    this.state = {
      showViewer: false,
      showIndex: 0,
    };

    this.viewerPressHandle = this.viewerPressHandle.bind(this);
    this.thumbPressHandle = this.thumbPressHandle.bind(this);
  }

  viewerPressHandle () {
    this.setState({
      showViewer: false,
    });
  }

   thumbPressHandle (i) {
       this.setState({
         showIndex: i,
         showViewer: true,
       });
       const self = this;
       setTimeout(function(){
           self.setState({swiper_key: Math.random()})
       }, 0)
   };

  render () {
    const {images, deleteCb} = this.props;
    return (
      <View>
        {this.state.showViewer &&
          <Modal onRequestClose={() => this.viewerPressHandle()}>
            <Viewer
              key={this.state.swiper_key}
              index={this.state.showIndex}
              pressHandle={this.viewerPressHandle}
              removeImage={(index) => {
                deleteCb && deleteCb(index);
                const self = this;
                setTimeout(function(){
                    self.setState({swiper_key: Math.random()});
                },0)

              }}
              imgList={images}
            />
           </Modal>
        }
        <View style={styles.thumbWrap}>
          {
            images.map((item, i) =>
              <TouchableOpacity style={[styles.thumbItem, i === 0 && { marginLeft: 0}]}
                                key={i} onPress={e => this.thumbPressHandle(i)}
              >
                <Image style={styles.thumbImage} resizeMode="cover" source={{uri: item.uri}} />
              </TouchableOpacity>)
          }
        </View>
      </View>
    );
  }
}
//swiper_key needs for the stupid mother fucker android cuz it cant wait till state with images finishes changing
//https://github.com/leecade/react-native-swiper/issues/380
