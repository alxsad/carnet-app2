import * as types from '../actions/types';

const initialState = {
  gaiNews: [],
  interestNews: [],
};

const news = (state = initialState, action = {}) => {

  const { type, payload } = action;

  switch (type) {
    case types.NEWS_GET_BY_GAI:
      return {
        ...state,
        gaiNews: payload.isRefreshing ? payload.news : [...state.gaiNews, ...payload.news],
      };
    case types.NEWS_GET_BY_INTERESTS:
      return {
        ...state,
        interestNews: payload.isRefreshing ? payload.news : [...state.interestNews, ...payload.news],
      };
    default:
      return state;
  }
};

export default news;
