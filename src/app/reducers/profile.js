import * as types from '../actions/types';

const initialState = {
  cars: [],
  interests: [],
};

const profile = (state = initialState, action = {}) => {
  switch (action.type) {
    case types.PROFILE_INITIAL_SAVE:
      return {...initialState, ...action.profile};
    case types.PROFILE_SAVE:
        return action.profile;
    case types.SET_INITIAL_STATE:
      return initialState;
    default:
      return state;
  }
};

export default profile;
