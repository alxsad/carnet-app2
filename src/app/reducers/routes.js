
import {Root} from '../navConfig/modals/navigationConfiguration'
import {TabBar} from '../navConfig/tabs/navigationConfiguration'
import { NewsNavigation } from '../../views/news/screens/navigationConfiguration'
import { OffensesNavigation } from '../../views/offense/screens/navigationConfiguration'
import { ChatNavigation } from '../../views/messenger/screens/navigationConfiguration'
import { ProfileNavigation } from '../../views/profile/screens/navigationConfiguration'
import { MapNavigation } from '../../views/map/screens/navigationConfiguration'

export const  root = (state,action) => Root.router.getStateForAction(action,state);

//in order to navigate from anywhere to tab 1 (index 0)
// just do  () => this.props.navigation.dispatch({ type:'JUMP_TO_TAB', payload:{index:0} })
export const tabBar = (state, action) => {
    if (action.type === 'JUMP_TO_TAB') {
        return { ...state, index: action.payload.index }
    } else {
        return TabBar.router.getStateForAction(action, state)
    }
};

export const  mapTab = (state,action) => MapNavigation.router.getStateForAction(action,state);
export const  newsTab = (state,action) => NewsNavigation.router.getStateForAction(action,state);
export const  offensesTab = (state,action) => OffensesNavigation.router.getStateForAction(action,state);
export const  chatTab = (state,action) => ChatNavigation.router.getStateForAction(action,state);
export const  profileTab = (state,action) => ProfileNavigation.router.getStateForAction(action,state);





