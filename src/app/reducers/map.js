import * as types from '../actions/types';
import MapboxGL from "@mapbox/react-native-mapbox-gl";

const initialState = {
};

const initialStatePersisted = {
  camera: true,
  gai: true,
  traffic: true,
  accident: true,
  other: true
};

const initialStateMap = {
  featureCollection: MapboxGL.geoUtils.makeFeatureCollection(),
};

export const map = (state = initialStateMap, action = {}) => {
  switch (action.type) {
    case types.MAP_GET_MARKERS:
    {
      return {featureCollection: action.featureCollection};
    }
    case types.MAP_ADD_MARKER:
    {
      const {features, ...other} = state.featureCollection;
      return {featureCollection: {...other, features: [...features, action.feature]}};
    }
    case types.MAP_SET_SELECTED:
    {
      if(action.feature !== null)   return {...state, selectedMarker: action.feature};
      const {selectedMarker, ...other} = state;
      return {...other };
    }
    case types.MAP_DELETE_MARKER: {
      let {features, ...other} = state.featureCollection;
      const filtered = features.filter((f) => f.id !== action.markerId);
      return {featureCollection: {...other, features: filtered}};
    }
    case types.MAP_CHANGE_MARKER_STATE: {
      let {features, ...other} = state.featureCollection;
      const newFeatures = features.map(x => x.properties.id === action.feature.properties.id ? action.feature : x);
      return {...state, featureCollection: {...other, features: newFeatures}};
    }
    default:
      return state;
  }
};

export const filtersPersisted = (state = initialStatePersisted, action = {}) => {
  const {filter} =  action;
  switch (action.type) {
    case types.FILTER_MAP_SWITCH_PERSIST:
      return { ...state, ...filter};
    case types.SET_INITIAL_STATE:
      return initialStatePersisted;
    default:
      return state;
  }
};

