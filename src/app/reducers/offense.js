import * as types from '../actions/types';

const initialState = {
  offensesList: [],
};

const offense = (state = initialState, action = {}) => {
  switch (action.type) {
    case types.OFFENSE_ADD_NEW:
        return { ...state, offensesList: [ action.offense, ...state.offensesList ] };
    case types.OFFENSE_GET_LIST:
      return { ...state, offensesList: action.list };
    default:
      return state;
  }
};

export default offense;
