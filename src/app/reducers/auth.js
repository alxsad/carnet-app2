import * as types from '../actions/types';

const initialState = {
  //if OTP has been sent and user closed the app - at next start he will be redirected to OTP screen
  OTPWasSent: false,
  // number of sent OTP via press "resend" button
  OTPResendAttempt: 0,
  //nextResendWillBeAvailableIn: 5000, //resend button availability in seconds
  OTPNextAvailableDate: null,

  // web socket connection options
  wsTimestamp: null,
  wsToken: null,

  userId: null,
  countryCode: null,
  phoneNumber: null,
  uuid: '',
  accessToken: '',
  isLogged: false,
};

const auth = (state = initialState, action = {}) => {
  switch (action.type) {
    case types.AUTH_SAVE_PHONE:
      return {
        ...state,
        phoneNumber: action.phoneNumber,
        countryCode: action.countryCode,
      };
    case types.AUTH_OTP_WAS_SENT:
      return {
        ...state,
        OTPWasSent: true,
        uuid: action.uuid,
      };
    case types.AUTH_OTP_RESEND:
      return {
        ...state,
        OTPResendAttempt: action.OTPResendAttempt ,
        OTPNextAvailableDate: action.OTPNextAvailableDate,
      };
    case types.AUTH_LOG_IN:
      return {
        ...state,
        isLogged: true,
        userId: action.userId,
        accessToken: action.accessToken,
        wsTimestamp: action.wsTimestamp,
        wsToken: action.wsToken,
      };
    case types.AUTH_CREATE_ACCESS_TOKEN:
      return {...state, accessToken: action.accessToken};
    case types.SET_INITIAL_STATE:
      return initialState;
    default:
      return state;
  }
};

export default auth;
