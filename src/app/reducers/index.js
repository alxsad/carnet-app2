import {
  root,
  tabBar,
  newsTab,
  offensesTab,
  chatTab,
  profileTab,
  mapTab
} from './routes';
import profile from './profile';
import auth from './auth';
import offense from './offense';
import news from './news';
import messenger from './messenger';
import {filtersPersisted, map} from './map'
import {domain, domainPersisted} from './domain';
import {reducer as network} from 'react-native-offline';

const reducer = {
  network,
  profile,
  auth,
  offense,
  news,
  messenger,
  filtersPersisted,
  //routes reducers
  root,
  tabBar,
  newsTab,
  offensesTab,
  chatTab,
  profileTab,
  mapTab,
  domain,
  domainPersisted,
  map
};

export default reducer;
