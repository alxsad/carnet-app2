import * as types from '../actions/types';
import moment from 'moment';
const DEFAULT_USER = 'Водитель';

const messageReducer = (state = [], action = {}) => {
  const { type, payload, sent } = action;
  switch (type) {
    case types.MESSENGER_RECEIVE_MESSAGE:
    case types.MESSENGER_RECEIVE_UNREAD_MESSAGES:
      var message = payload.message;
      return [{
        _id: message.id,
        text: message.text,
        createdAt: payload.message.created_date,
        delivered: true,
        user: {
          _id: message.sender.user_id,
          name: message.sender.name || DEFAULT_USER,
          avatar: message.sender.avatar,
        },
      }, ...state];
    case types.MESSENGER_SEND_MESSAGE:
      var message = payload.message;
      return [{
        _id: message._id,
        text: message.text,
        createdAt: moment(message.createdAt).format(),
        delivered: true,
        user: {
          _id: message.user._id,
          name: message.user.name || DEFAULT_USER,
        },
        //sent : null,
        // received : true,
      }, ...state];
    case types.MESSENGER_SET_MESSAGE_SENT:
      var message = payload.message;
      return state.map(m => ( m._id === message._id) ? {...m, sent: sent} : m);
    case types.MESSENGER_REMOVE_MESSAGE:
      var message = payload.message;
      return state.filter(m => ( m._id !== message._id));
    default:
      return state;
  }
};

const chatInitalState = {
  user_id: undefined,
  user_name: undefined,
  user_avatar: undefined,
  has_unread_messages: false,
  chat_id: undefined,
  title: undefined,
  car_number: undefined,
  last_message_text: undefined,
  last_message_date: undefined,
  messages: messageReducer(undefined, {}),
};

const chatReducer = (state = chatInitalState, action = {}) => {
  const { type, payload } = action;
  switch (type) {
    case types.MESSENGER_RECEIVE_MESSAGE:
    case types.MESSENGER_RECEIVE_UNREAD_MESSAGES:
      var message = payload.message;
      var userName = message.sender.name || DEFAULT_USER;
      return {
        ...state,
        user_id: message.sender.user_id,
        chat_id: message.chat_id,
        user_avatar: message.sender.avatar,
        has_unread_messages: true,
        title: state.car_number ? `${userName} (${state.car_number})` : userName,
        user_name: userName,
        last_message_text: message.text,
        last_message_date: message.created_date,
        messages: messageReducer(state.messages, action)
      };
    case types.MESSENGER_REMOVE_MESSAGE:
    case types.MESSENGER_SET_MESSAGE_SENT:
    case types.MESSENGER_SEND_MESSAGE:
      var message = payload.message;
      return {
        ...state,
        has_unread_messages: false,
        last_message_text: message.text,
        last_message_date: moment(message.createdAt).format(),
        messages: messageReducer(state.messages, action)
      };
    case types.MESSENGER_CREATE_CHAT:
      var userName = payload.profile.name || DEFAULT_USER;
      return {
        ...state,
        user_id: payload.profile.user_id,
        user_name: userName,
        chat_id: payload.chatId,
        title: `${userName} (${payload.carNumber})`,
        car_number: payload.carNumber,
        user_avatar: payload.profile.avatar,
        last_message_date: moment().format(),
        messages: messageReducer(undefined, action)
      };
    case types.MESSENGER_REMOVE_CHAT_BADGE:
      return {...state, has_unread_messages: false};
    default:
      return state;
  }
};

const messenger = (state = {}, action = {}) => {
  const { type, payload } = action;
  switch (type) {
    case types.MESSENGER_DELETE_CONVERSATION:
      const chats = {...state};
      delete chats[payload.chatId];
      return chats;
    case types.MESSENGER_RECEIVE_UNREAD_MESSAGES:
      return payload.unreadMessages.reduce((result, message) => {
        action.payload.message = message;
        result[message.chat_id] = chatReducer(result[message.chat_id], action);
        return result;
      }, {...state});
    case types.MESSENGER_REMOVE_MESSAGE:
    case types.MESSENGER_SEND_MESSAGE:
    case types.MESSENGER_SET_MESSAGE_SENT:
      var chatId = payload.chatId;
      return {
        ...state,
        [chatId]: chatReducer(state[chatId], action)
      };
    case types.MESSENGER_RECEIVE_MESSAGE:
      var chatId = payload.message.chat_id;
      return {
        ...state,
        [chatId]: chatReducer(state[chatId], action)
      };
    case types.MESSENGER_CREATE_CHAT:
      return {
        ...state,
        [payload.chatId]: chatReducer({}, action),
      };
    case types.MESSENGER_REMOVE_CHAT_BADGE:
      var chatId = payload.chatId;
      return {
        ...state,
        [chatId]: chatReducer(state[chatId], action)
      };
    case types.SET_INITIAL_STATE:
      return [];
    default:
      return state;
  }
};

export default messenger;
