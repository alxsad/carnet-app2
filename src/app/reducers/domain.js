import * as types from '../actions/types';

const initialState = {
  interestsList: [],
};
const initialStatePersisted = {
  isAndroidNotificationAlertShown: false,
};

export const domain = (state = initialState, action = {}) => {
  switch (action.type) {
    case types.DOMAIN_SET_INTERESTS_LIST:
      return { ...state, interestsList: action.interestsList };
    default:
      return state;
  }
};

export const domainPersisted = (state = initialStatePersisted, action = {}) => {
  switch (action.type) {
    case types.DOMAIN_PERSISTED_SET_ANDROID_NOTIFICATION_ALERT_IS_SHOWN:
      return { ...state, isAndroidNotificationAlertShown: true };
    case types.SET_INITIAL_STATE:
      return initialStatePersisted;
    default:
      return state;
  }
};

