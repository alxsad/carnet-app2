'use strict';

import React, { Component } from 'react';
import { NavigationActions } from 'react-navigation';
import {InitialRootNavigator} from './rootNavigation'
import {connect} from 'react-redux';
import {updateDeviceToken} from '../actions/auth';
import {receiveMessage, receiveUnreadMessages} from '../actions/messenger';
import firebase from 'react-native-firebase';
import Centrifuge from 'centrifuge';
import { AppState, View } from 'react-native';
import { Message } from '../../components';
import { MessageBar } from 'react-native-messages';
import SplashScreen from 'react-native-splash-screen';
import {setInitialState} from "../actions/profile";
import {Domain} from "../../services";
import {getMarkers} from "../actions/map";

class AppInit extends Component {
  componentDidMount() {
    firebase.notifications().setBadge(0);
    firebase.notifications().removeAllDeliveredNotifications();
    this.routeOnAuth();
  }

  componentDidUpdate() {
    this.routeOnAuth();
  }

  componentWillUnmount() {
    this.notificationListener();
    this.refreshTokenListener();
    this.centrifuge.disconnect();
    AppState.removeEventListener('change', this.onChangeAppState);
  }

  onChangeAppState(nextAppState) {
    if ('active' === nextAppState) {
      this.props.receiveUnreadMessages();
      firebase.notifications().setBadge(0);
      firebase.notifications().removeAllDeliveredNotifications();
      this.props.getMarkers();
    }
  }

  async routeOnAuth() {
    const isLoggedIn = this.props.isLogged;

    const navigateTo = (routeName) => {
      const actionToDispatch = NavigationActions.reset({
        index: 0,
        key: null,
        actions: [NavigationActions.navigate({routeName})],
      });
      this.navigator.dispatch(actionToDispatch);
    };

    if (isLoggedIn) {
      const isTokenExpired = await Domain.isTokenExpired(this.props.accessToken);
      if (isTokenExpired) {
        this.props.logOut();
        navigateTo('AuthScreen');
        return;
      }

      this.centrifuge = new Centrifuge({
        url: 'wss://carnet.davidovi.ch/centrifugo/connection/websocket',
        // url: 'wss://carnet-int.davidovi.ch/centrifugo/connection/websocket',
        // url: 'ws://localhost:8583/connection/websocket',
        user: this.props.userId,
        timestamp: this.props.wsTimestamp.toString(),
        token: this.props.wsToken,
      });
      this.centrifuge.subscribe(`messages#${this.props.userId}`, message => {
        console.log(message);
        this.props.receiveMessage(message.data);
      });
      this.centrifuge.on('connect', () => {
        console.log('connected');
      });
      this.centrifuge.on('disconnect', error => {
        console.log('disconnected');
        console.log(error);
      });
      this.centrifuge.on('error', error => {
        console.log(error)
      });
      this.centrifuge.connect();
      this.setupPushNotifications();
      this.props.receiveUnreadMessages();
      this.hideSplash();
      navigateTo('TabBar');
    } else {
      navigateTo('AuthScreen');
    }
  }

  setupPushNotifications() {
    this.refreshTokenListener = firebase.messaging().onTokenRefresh(fcmToken => {
      this.props.updateDeviceToken(fcmToken);
    });
    this.notificationListener = firebase.messaging().onMessage(message => {
      console.log('push', message);
    });
    AppState.addEventListener('change', this.onChangeAppState.bind(this));

    firebase.messaging().requestPermission()
      .then(() => {
        firebase.messaging().getToken()
          .then(fcmToken => {
            if (fcmToken) {
              this.props.updateDeviceToken(fcmToken);
            } else {
              console.log("user doesn't have a device token yet");
            }
          });
      })
      .catch(error => {
        console.log("User has rejected permissions");
      });
  }

  hideSplash(){
    setTimeout(function(){
      SplashScreen.hide();
    }, 1000);
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <InitialRootNavigator
          ref={(nav) => {
            this.navigator = nav;
          }}
        />
        <MessageBar messageComponent={Message} duration={2000} />
      </View>
    );
  }
}

const propsMapping = state => {
  return {
    isLogged: state.auth.isLogged,
    wsTimestamp: state.auth.wsTimestamp,
    wsToken: state.auth.wsToken,
    userId: state.auth.userId,
    androidNotificationAlertWasShown: state.domainPersisted.isAndroidNotificationAlertShown,
    accessToken: state.auth.accessToken,
  }
};

const actionMapping = dispatch => {
  return {
    updateDeviceToken: token => dispatch(updateDeviceToken(token)),
    receiveMessage: message => dispatch(receiveMessage(message)),
    receiveUnreadMessages: () => dispatch(receiveUnreadMessages()),
    logOut: () => dispatch(setInitialState()),
    getMarkers: () => dispatch(getMarkers()),
  }
};

export default connect(propsMapping, actionMapping)(AppInit);
