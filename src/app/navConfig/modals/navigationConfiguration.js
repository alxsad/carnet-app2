import { StackNavigator } from 'react-navigation'
import Tabs from '../tabs/tabBarNavigation';
import { MarkLocationMap, FindCar, MapModal } from '../../../views/modals';
import {NavStyles} from '../../../styles';

// this is a root navigator for the  tabs and all modals that tabs need
export const Root = StackNavigator({
  Tabs: {
    screen: Tabs,
    navigationOptions: ({navigation}) => ({
      header: null,
    })
  },
  MarkLocationMapModal: {
    screen: MarkLocationMap,
    navigationOptions: ({navigation}) => ({
      header: null,
    })
  },
  FindCarModal: {
    screen: FindCar,
  },
  MapModal: {
    screen: MapModal,
  },
}, {
  navigationOptions: ({ screenProps }) => ({
    headerStyle: NavStyles.headerNavStyle,
    headerTintColor: '#ffffff',
    headerTitleStyle: {alignSelf: 'center'},
  }),
  mode: 'modal',
  headerMode: "screen"
});
