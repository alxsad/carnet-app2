// React
import React, {Component} from 'react'

// Navigation
import { addNavigationHelpers } from 'react-navigation'
import { Root } from './navigationConfiguration'

//Redux
import { connect } from 'react-redux'
const mapStateToProps = (state) => {
  return {
    navigationState: state.root,
  }
};

class RootNavigation extends Component {
  render(){
    const { dispatch, navigationState } = this.props;
    return (
      <Root
        navigation={
          addNavigationHelpers({
            dispatch: dispatch,
            state: navigationState,
          })
        }
      />
    )
  }
}
export default connect(mapStateToProps)(RootNavigation)
