'use strict';
import { StackNavigator } from 'react-navigation';

// Screens
import SplashScreen from  '../../views/splash/Splash'
import {AuthNavigation} from  '../../views/auth/screens/navigationConfiguration'
import RootNavigation from './modals/modalsNavigation'


const routeConfiguration = {
  SplashScreen: { screen: SplashScreen },
  AuthScreen: { screen: AuthNavigation },
  TabBar: { screen: RootNavigation },
};

const stackNavigatorConfiguration = {
  //mode: 'modal',
  headerMode: 'none',
};

// this is a root navigator for the application init
export const InitialRootNavigator = StackNavigator(routeConfiguration, stackNavigatorConfiguration);

