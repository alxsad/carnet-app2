'use strict';

import React from 'react';
import {Platform, View} from 'react-native';
import { TabNavigator } from 'react-navigation';
import {DEFAULT_COLOR} from '../../../styles';
import {Icon} from '../../../components/index';
import {tabBarStyles} from '../../../styles'

// Tab-Navigators

// import {NewsNavigation} from '../../../views/news/screens/navigationConfiguration'
// import {OffensesNavigation} from '../../../views/offense/screens/navigationConfiguration'
import {ChatNavigation} from '../../../views/messenger/screens/navigationConfiguration'
import {ProfileNavigation} from '../../../views/profile/screens/navigationConfiguration'
import {MapNavigation} from '../../../views/map/screens/navigationConfiguration'


const routeConfiguration = {
  // NewsNavigation: {
  //   screen: NewsNavigation,
  //   navigationOptions: {
  //     tabBarLabel: 'Новости',
  //     tabBarIcon: ({ tintColor }) => <Icon style={tabBarStyles.iconStyle} name="menu_1_1" size={25} color={tintColor} />,
  //   }
  // },
  // Offenses: {
  //   screen: OffensesNavigation,
  //   navigationOptions: {
  //     tabBarLabel: 'Сообщить',
  //     tabBarIcon: ({ tintColor }) => <Icon style={tabBarStyles.iconStyle} name="menu_2_1" size={27} color={tintColor} />
  //   }
  // },
  Map: {
    screen: MapNavigation,
    navigationOptions: {
      tabBarLabel: 'Карта',
      tabBarIcon: ({ tintColor }) => <Icon style={[tabBarStyles.iconStyle, {right: -4}]} name="icon_pin" size={28} color={tintColor} />
    }
  },
  Messenger: {
    screen: ChatNavigation,
    navigationOptions: ({ screenProps }) => ({
      tabBarLabel: 'Общение',
    })
  },
  Profile: {
    screen: ProfileNavigation,
    navigationOptions: {
      tabBarLabel: 'Профиль',
      tabBarIcon: ({ tintColor }) => <Icon style={tabBarStyles.iconStyle} name="menu_4_1" size={28} color={tintColor} />
    }
  }
};

const tabBarConfiguration = {
  tabBarPosition: 'bottom',
  lazy: true,
  swipeEnabled: false,
  gesturesEnabled:false,
  //...other configs
  tabBarOptions: {
    showIcon: true,
    showLabel: Platform.OS === 'ios',
    // tint color is passed to text and icons (if enabled) on the tab bar
    activeTintColor: DEFAULT_COLOR,
    indicatorStyle: { backgroundColor: DEFAULT_COLOR, },// or {backgroundColor: 'transparent'} android
    inactiveTintColor: '#A2A2A2',
    labelStyle: {
      // fontSize: 10
    },
    iconStyle: {
      //works for android, not for ios
      width: 35,
      height: 30
    },
    tabStyle: {
      // padding: 0
    },
    style: {
      backgroundColor: '#EEEEEE',
      padding: 0,
    },
  }
};

export const TabBar = TabNavigator(routeConfiguration, tabBarConfiguration);
