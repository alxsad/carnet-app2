// React
import React, {Component} from 'react'
import {StatusBar, Platform} from 'react-native';

// Navigation
import { addNavigationHelpers } from 'react-navigation'
import { TabBar } from './navigationConfiguration'
import { BackHandler } from '../../../utils/helpers';

//Redux
import { connect } from 'react-redux'

import Modal from '../../../components/Modal'

class TabBarNavigation extends Component {
  //handle android hardware back button
  handleBackPress = () => {
    const { dispatch, navigationState } = this.props;
    // const navigation = addNavigationHelpers({
    //   dispatch,
    //   state: navigationState,
    // });
    dispatch({ type: 'Navigation/BACK' });
    return true;
  };
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress',this.handleBackPress);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }
  render(){
    const { dispatch, navigationState, tabIndex, modal } = this.props;
    const statusBarStyle= (Platform.OS === 'ios' && (tabIndex === 0 && modal.routes.length === 1)) === true ?
        "dark-content": "light-content";
    return (
        <React.Fragment>
          <StatusBar barStyle={statusBarStyle}/>
          <TabBar
            navigation={
              addNavigationHelpers({
                dispatch: dispatch,
                state: navigationState,
              })
            }
          />
          {tabIndex === 0 && <Modal navigation={this.props.navigation}/>}
        </React.Fragment>
    )
  }
}

const mapStateToProps = state => {
  return {
    navigationState: state.tabBar,
    tabIndex: state.tabBar.index, //TODO: rework setting StatusBar style later
    modal: state.root
  }
};

export default connect(mapStateToProps)(TabBarNavigation)
