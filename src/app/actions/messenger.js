import * as types from './types';
import {Messenger} from '../../services';
// import Sound from 'react-native-sound';

export function deleteChat(chatId) {
  return {
    type: types.MESSENGER_DELETE_CONVERSATION,
    payload: {
      chatId,
    }
  };
}

export function sendMessage(chatId, message, fail) {
  return async (dispatch, getState) => {
    dispatch({
      type: types.MESSENGER_SEND_MESSAGE,
      payload: {chatId, message},
    });
    // const sound = new Sound('message_sent.m4a', Sound.MAIN_BUNDLE, () => {
    //   sound.play();
    // });
    const result = await Messenger.sendMessage(
      message._id,
      chatId,
      getState().messenger[chatId].user_id,
      message.text,
      getState().auth.accessToken,
    );
    dispatch({
      type: types.MESSENGER_SET_MESSAGE_SENT,
      payload: {chatId, message},
      sent: result
    });
    if (!result) {
      fail();
    }
  }
}
export function resendMessage(chatId, message, fail) {
  return async (dispatch, getState) => {
    const result = await Messenger.sendMessage(
      message._id,
      chatId,
      getState().messenger[chatId].user_id,
      message.text,
      getState().auth.accessToken,
    );
    dispatch({
      type: types.MESSENGER_SET_MESSAGE_SENT,
      payload: {chatId, message},
      sent: result
    });
    if (!result) {
      fail();
    }
  }
}
export function deleteMessage(chatId, message) {
  return async (dispatch, getState) => {
    dispatch({
      type: types.MESSENGER_REMOVE_MESSAGE,
      payload: {chatId, message},
    });
  }
}

export function receiveMessage(message) {
  return async (dispatch, getState) => {
    dispatch({
      type: types.MESSENGER_RECEIVE_MESSAGE,
      payload: {
        message,
      }
    });
    // const sound = new Sound('message_received.m4a', Sound.MAIN_BUNDLE, () => {
    //   sound.play();
    // });
    await Messenger.markMessageAsRead(message.id, getState().auth.accessToken);
  }
}

export function receiveUnreadMessages() {
  return async (dispatch, getState) => {
    const unreadMessages = await Messenger.receiveUnreadMessages(getState().auth.accessToken);
    if (unreadMessages && unreadMessages.length) {
      dispatch({
        type: types.MESSENGER_RECEIVE_UNREAD_MESSAGES,
        payload: {
          unreadMessages,
        }
      });
      await Messenger.markMessagesAsRead(getState().auth.accessToken);
    }
  }
}

export function createChat(carNumber, profile, chatId) {
  return  {
    type: types.MESSENGER_CREATE_CHAT,
    payload: {carNumber, profile, chatId: chatId}
  };
}

export function removeChatBadge(chatId) {
  const getChatIndicate = (state, id) => state.messenger[id].has_unread_messages;
  return (dispatch, getState) => {
    if (!getChatIndicate(getState(), chatId)) return;
    dispatch({
      type: types.MESSENGER_REMOVE_CHAT_BADGE,
      payload: {chatId}
    });
  };
}
