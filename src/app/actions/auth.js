import * as types from './types';
import {Auth, Profile} from '../../services';
import {RESEND_CODE_MESSAGE, INCORRECT_PHONE_MESSAGE, INCORRECT_CODE_MESSAGE} from '../../domain/MessagesList';
import DeviceInfo from 'react-native-device-info';

export function updateDeviceToken(token) {
  return async (dispatch, getState) => {
    dispatch({
      type: types.AUTH_UPDATE_DEVICE_TOKEN,
      result: await Auth.updateDeviceToken(DeviceInfo.getUniqueID(), token, getState().auth.accessToken),
      token,
    });
  }
}

export function sendPhone(phone, success, fail) {
  return async dispatch => {
    dispatch({
      type: types.AUTH_SAVE_PHONE,
      phoneNumber: phone.phoneNumber,
      countryCode: phone.countryCode
    });
    const response = await Auth.sendPhone(phone.countryCode + phone.phoneNumber);
    if (false === response) {
      return fail(INCORRECT_PHONE_MESSAGE);
    }
    if (null === response) {
      return fail();
    }
    dispatch({
      type: types.AUTH_OTP_WAS_SENT,
      uuid: DeviceInfo.getUniqueID(),
    });
    success();
  }
}

export function verifyOTP(params, fail) {
  return async (dispatch, getState) => {
    const { code, platform } = params;
    const { countryCode, phoneNumber, uuid } = getState().auth;
    const phone = countryCode + phoneNumber;
    const response = await Auth.verifyOTP(phone, code, uuid, platform);
    if (false === response) {
      return fail(INCORRECT_CODE_MESSAGE);
    }
    if (null === response) {
      return fail();
    }
    const profile = await Profile.getProfileByUserId(response.user_id, response.token);
    if (!profile) {
      return fail();
    }
    dispatch({
      type: types.PROFILE_INITIAL_SAVE,
      profile,
    });
    dispatch({
      type: types.AUTH_LOG_IN,
      accessToken: response.token,
      wsTimestamp: response.ws_timestamp,
      wsToken: response.ws_token,
      userId: response.user_id,
    });
  }
}

export function resendOTP(success, fail) {
  return async (dispatch, getState) => {
    const { OTPResendAttempt, countryCode, phoneNumber } = getState().auth;
    const resendCurrentAttempt = OTPResendAttempt + 1;
    const response = await Auth.sendPhone(countryCode + phoneNumber);
    if (false === response) {
      return fail(INCORRECT_PHONE_MESSAGE);
    }
    if (null === response) {
      return fail();
    }
    const date = getOTPNextAvailableDate(resendCurrentAttempt);
    dispatch({
      type: types.AUTH_OTP_RESEND,
      OTPResendAttempt: resendCurrentAttempt,
      OTPNextAvailableDate: date,
    });
    success(RESEND_CODE_MESSAGE);
  }
}

function getOTPNextAvailableDate(resendCurrentAttempt) {
  switch (resendCurrentAttempt) {
    case 1:
      return new Date().setMinutes(new Date().getMinutes() + 10);
    case 2:
      return new Date().setHours(new Date().getHours() + 5);
    case 3:
    default:
      return new Date().setHours(new Date().getHours() + 24);
  }
}
