import * as types from './types';
import {Profile, Media} from '../../services';

export function saveProfile(profile, success, fail) {
  return async (dispatch, getState) => {
    const response = await Profile.saveProfile(profile, getState().auth.accessToken);
    if (!response) {
      fail();
      return;
    }
    dispatch({
      type: types.PROFILE_SAVE,
      profile,
    });
    success();
  }
}

export function uploadAvatar(base64, success, fail) {
  return async (dispatch, getState) => {
    const url = await Media.uploadImage(base64, getState().auth.accessToken, 'avatars');
    if (!url) {
      fail();
      return;
    }
    success(url);
  }
}


export function setInitialState() {
  return dispatch => {
    dispatch({
      type: types.SET_INITIAL_STATE,
    });
  }
}
