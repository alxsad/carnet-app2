export const SET_INITIAL_STATE                                = 'SET_INITIAL_STATE';

export const PROFILE_SAVE                                     = 'PROFILE_SAVE';
export const PROFILE_INITIAL_SAVE                             = 'PROFILE_INITIAL_SAVE';

export const AUTH_CREATE_ACCESS_TOKEN                         = 'AUTH_CREATE_ACCESS_TOKEN';

export const OFFENSE_ADD_NEW                                  = 'OFFENSE_ADD_NEW';
export const OFFENSE_GET_LIST                                 = 'OFFENSE_GET_LIST';

export const MESSENGER_DELETE_CONVERSATION                    = 'MESSENGER_DELETE_CONVERSATION';
export const MESSENGER_SEND_MESSAGE                           = 'MESSENGER_SEND_MESSAGE';
export const MESSENGER_RECEIVE_MESSAGE                        = 'MESSENGER_RECEIVE_MESSAGE';
export const MESSENGER_RECEIVE_UNREAD_MESSAGES                = 'MESSENGER_RECEIVE_UNREAD_MESSAGES';
export const MESSENGER_CREATE_CHAT                            = 'MESSENGER_CREATE_CHAT';
export const MESSENGER_REMOVE_CHAT_BADGE                      = 'MESSENGER_REMOVE_CHAT_BADGE';
export const MESSENGER_SET_MESSAGE_SENT                       = 'MESSENGER_SET_MESSAGE_SENT';
export const MESSENGER_REMOVE_MESSAGE                         = 'MESSENGER_REMOVE_MESSAGE';

export const NEWS_GET_BY_GAI                                  = 'NEWS_GET_BY_GAI';
export const NEWS_GET_BY_INTERESTS                            = 'NEWS_GET_BY_INTERESTS';

export const AUTH_LOG_IN                                      = 'AUTH_LOG_IN';
export const AUTH_OTP_WAS_SENT                                = 'OTP_WAS_SENT';
export const AUTH_OTP_RESEND                                  = 'AUTH_OTP_RESEND';
export const AUTH_SAVE_PHONE                                  = 'AUTH_SAVE_PHONE';
export const AUTH_UPDATE_DEVICE_TOKEN                         = 'AUTH_UPDATE_DEVICE_TOKEN';

export const DOMAIN_SET_INTERESTS_LIST                        = 'DOMAIN_SET_INTERESTS_LIST';
export const DOMAIN_PERSISTED_SET_ANDROID_NOTIFICATION_ALERT_IS_SHOWN =
  'DOMAIN_PERSISTED_SET_ANDROID_NOTIFICATION_ALERT_IS_SHOWN';

export const FILTER_MAP_SWITCH_PERSIST                        = 'FILTER_MAP_SWITCH_PERSIST';
export const MAP_GET_MARKERS                                  = 'MAP_GET_MARKERS';
export const MAP_ADD_MARKER                                   = 'MAP_ADD_MARKER';
export const MAP_SET_SELECTED                                 = 'MAP_SET_SELECTED';
export const MAP_DELETE_MARKER                                = 'MAP_DELETE_MARKER';
export const MAP_CHANGE_MARKER_STATE                          = 'MAP_CHANGE_MARKER_STATE';
