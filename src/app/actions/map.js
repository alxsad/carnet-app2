import * as types from './types';
import {Map} from '../../services';
import MapboxGL from "@mapbox/react-native-mapbox-gl";
import {MapConf} from "../../domain";

export function getMarkers(finishCb, errorCb) {
  return async (dispatch, getState) => {
    const response = await Map.getMarkers(getState().auth.accessToken);
    if (response) {
      const featureCollection = mapToFeatureCollection(response);
      dispatch({
        type: types.MAP_GET_MARKERS,
        featureCollection
      });
      finishCb && finishCb();
      return;
    }
    finishCb && finishCb();
    errorCb && errorCb();
  }
}

export function addMarker(marker, finishCb, errorCb) {
  return async (dispatch, getState) => {
    if (marker.image) {
      const url = await Map.uploadMarkerImage(marker.image.data, getState().auth.accessToken);
      if (url) {
        marker = {...marker, photos: [{url: url}]}
      } else {
        errorCb();
        return;
      }
    }
    if (!marker.title) {
      marker = {...marker, title: MapConf.TypesDescription[marker.type]}
    }

    const response = await Map.saveMarker(marker, getState().auth.accessToken);
    if (response) {
      const feature = mapToFeatures(response);
      feature.properties.recentlyAdded = true;

      dispatch({
        type: types.MAP_ADD_MARKER,
        feature
      });
      finishCb(feature);
      return;
    }
    errorCb();
  }
}

export function deleteMarker(markerId, finishCb, errorCb) {
  return async (dispatch, getState) => {
    const response = await Map.deleteMarker(markerId, getState().auth.accessToken);
    if (response) {
      dispatch({
        type: types.MAP_DELETE_MARKER,
        markerId: markerId
      });
      finishCb && finishCb();
    } else {
      errorCb && errorCb();
    }
  }
}

export function setSelectedMarker(feature) {
  let selected = null;

  if (feature) {
    const {geometry, properties, ...other} = feature;
    selected = {...other, ...geometry, properties: {...properties, selected: true}};
  }

  return (dispatch, getState) => {
    dispatch({
      type: types.MAP_SET_SELECTED,
      feature: selected
    });
  }
}

export function deleteMarkerFromMap(markerId) {
  return async (dispatch, getState) => {
    dispatch({
      type: types.MAP_DELETE_MARKER,
      markerId,
    });
  }
}

export function voteMarker(markerId, vote) {
  return async (dispatch, getState) => {
    const response = await Map.voteMarker(markerId, {status: vote}, getState().auth.accessToken);
    if (response) {
      const feature = mapToFeatures(response);
      setSelectedMarker(feature)(dispatch, getState);
      dispatch({
        type: types.MAP_CHANGE_MARKER_STATE,
        feature
      });
    }
    return response;
  }
}

function mapToFeatures(m) {
  return {
    type: 'Feature',
    id: m.id,
    properties: {
      icon: m.type,
      id: m.id,
      title: m.title,
      created: m.created_date ?? m.created_date,
      markedAsUnknownDate: m.marked_as_unknown_date ?? m.marked_as_unknown_date,
      userId: m.user_id,
      status: m.status
    },
    geometry: {
      type: 'Point',
      coordinates: [m.location.longitude, m.location.latitude],
    },
  }
}


function mapToFeatureCollection(markers = []) {
  const featureCollection = MapboxGL.geoUtils.makeFeatureCollection();
  const features = markers.map((m, i) => {
    return {
      type: 'Feature',
      id: m.id,
      properties: {
        icon: m.type,
        id: m.id,
        title: m.title,
        created: m.created_date ?? m.created_date,
        markedAsUnknownDate: m.marked_as_unknown_date ?? m.marked_as_unknown_date,
        userId: m.user_id,
        status: m.status
      },
      geometry: {
        type: 'Point',
        coordinates: [m.location.longitude, m.location.latitude],
      },
    }
  });

  return {...featureCollection, features};
}
