import * as types from './types';
import { News } from '../../services';

export function getNewsByGai(params, finish) {
  return async (dispatch, getState) => {
    const response = await News.getNewsByGai(params, getState().auth.accessToken);
    if (response) {
      dispatch({
        type: types.NEWS_GET_BY_GAI,
        payload: {
          news: response,
          isRefreshing: params.isRefreshing,
        },
      });
    }
    finish();
  }
}

export function getNewsByInterests(params, finish) {
  return async (dispatch, getState) => {
    const response = await News.getNewsByInterests(params, getState().auth.accessToken);
    if (response) {
      dispatch({
        type: types.NEWS_GET_BY_INTERESTS,
        payload: {
          news: response,
          isRefreshing: params.isRefreshing,
        },
      });
    }
    finish();
  }
}
