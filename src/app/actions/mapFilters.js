import * as types from './types';

export function switchFilterState(filter) {
  return dispatch => {
    dispatch({
      type: types.FILTER_MAP_SWITCH_PERSIST,
      filter
    });
  }
}

export function setInitialState() {
  return dispatch => {
    dispatch({
      type: types.SET_INITIAL_STATE,
    });
  }
}
