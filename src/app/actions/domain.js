import * as types from './types';
import {Domain} from '../../services';

export function getInterestsList(fail) {
  return async (dispatch, getState) => {
    const response = await Domain.getInterestsList(getState().auth.accessToken);
    if (!response) {
      fail();
      return;
    }
    dispatch({
      type: types.DOMAIN_SET_INTERESTS_LIST,
      interestsList: response,
    });
  }
}

export function setAndroidNotificationAlertIsShown() {
  return (dispatch) => {
    dispatch({
      type: types.DOMAIN_PERSISTED_SET_ANDROID_NOTIFICATION_ALERT_IS_SHOWN,
    });
  }
}
