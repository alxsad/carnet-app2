import * as types from './types';
import { Media, Offense } from '../../services';

export function createOffense(offense, images, success, fail) {
  return async (dispatch, getState) => {
    const uploadedImages = await Media.bulkUploadImage(images, getState().auth.accessToken, 'offenses');
    if (!uploadedImages) {
      return fail();
    }
    const newOffense = await Offense.createOffense(
      { ...offense, ...{ photos: uploadedImages, offense_date: offense.offense_date } },
      getState().auth.accessToken
    );
    if (!newOffense) {
      return fail();
    }
    dispatch({
      type: types.OFFENSE_ADD_NEW,
      offense: newOffense
    });
    success();
  }
}

export function getOffenses(success, fail) {
  return async (dispatch, getState) => {
    const offenses = await Offense.getOffenses(getState().profile.id, getState().auth.accessToken);
    if (!offenses) {
      return fail();
    }
    dispatch({
      type: types.OFFENSE_GET_LIST,
      list: offenses,
    });
    success();
  }
}
