/**
 * Created by aliaksei on 5/29/17.
 */
import React, {Component} from 'react';
import AppInit from './navConfig/root'
import {configureStore}  from './Store';
import {PersistGate} from 'redux-persist/es/integration/react';

const {persistor} = configureStore();

import moment from 'moment';
import 'moment/locale/ru';

export default class Carnet extends Component {
  constructor(props) {
    super(props);
    moment.locale('ru');//set locale for moment.js globally
    moment.relativeTimeThreshold('m', 59);
    moment.relativeTimeThreshold('h', 23);
  }

  render() {
    return (
      <PersistGate
        //loading={<ActivityIndicator />}
        //onBeforeLift={onBeforeLift}
        persistor={persistor}>
      <AppInit/>
      </PersistGate>
    );
  }
}
