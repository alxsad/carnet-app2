'use strict';
import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import reducer from './reducers';
import {persistStore, persistCombineReducers} from 'redux-persist'
import storage from 'redux-persist/es/storage'; // default: localStorage if web, AsyncStorage if react-native

const middlewares = [thunk];
const enhancers = [applyMiddleware(...middlewares)];

if ('development' === process.env.NODE_ENV) {
  middlewares.push(logger);
}

if((process.env.NODE_ENV || '').toLowerCase() === 'production'){
  // disable console. log in production
  console.log = function () {};
  console.info = function () {};
  console.warn = function () {};
  console.error = function () {};
  console.debug = function () {};
}

const config = {
  key: 'root',
  storage,
  //debug: true //to get useful logging
  whitelist: ['profile', 'auth', 'messenger', 'domainPersisted', 'filtersPersisted'],
};
const persistConfig = {enhancers};
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const reducers = persistCombineReducers(config, reducer);

const Store = createStore(reducers, {}, composeEnhancers(...enhancers));
const persistor = persistStore(Store, persistConfig, () => {
  console.log('state rehydrated');
});

export const configureStore = () => {
  return {persistor};
};

export default Store;
