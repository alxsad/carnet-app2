'use strict';

import React, {Component} from 'react';
import {Provider} from 'react-redux';
import Carnet from './src/app/Carnet';
import Store from './src/app/Store';
import { withNetworkConnectivity } from 'react-native-offline';

let App = () => (
    <Carnet/>
);

App = withNetworkConnectivity({
    withRedux: true,
    withExtraHeadRequest: false,
})(App);

class Container extends Component {
    render() {
        return (
            <Provider store={Store}>
                <App/>
            </Provider>
        );
    }
}

export default Container;
